﻿using ShareFile.Api.Client;
using System;
using System.Collections.Generic;

namespace ThinClient.Verification.Common
{
    class Constants
    {
        #region Constant Variables 
        public static string Version_NUMBER = @"20190801";
        public static string BUILD_NUMBER = @"Ver. " + Version_NUMBER; 
        public static string LOG_FILE_NAME = @"ThinClientLog.txt";

        //HDX Policies links
        public static string HDXReadyPolicyLink = @"https://citrix.sharefile.com/d-s1b8f4559b624e84a";
        public static string HDXPremiumPolicyLink = @"https://citrix.sharefile.com/d-s5217e91605049e8b";
        public static string HDX3DProPolicyLink = @"https://citrix.sharefile.com/d-s5aa220bd6fe45428";

        //After every Test Case, wait for X milliseconds
        public static int TC_WAIT_TIME_IN_MS = 3000;

        //Registry Location to search for Installed Programs 
        public static string REG_UNINSTALL_LOCALMACHINE = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
        public static string REG_UNINSTALL_LOCALMACHINE_64 = @"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall";

        //Application Executable Names 
        public static string APP_EXECUTABLE_POWERPOINT = "POWERPNT.EXE";
        public static string APP_EXECUTABLE_COMMANDPROMPT = "CMD.EXE";
        public static string APP_EXECUTABLE_ACROBAT = "ACRORD32.EXE";
        public static string APP_EXECUTABLE_VLCPLAYER64BIT = @"C:\Program Files\VideoLAN\VLC\VLC.EXE";
        public static string APP_EXECUTABLE_VLCPLAYER = @"C:\Program Files (x86)\VIDEOLAN\VLC\VLC.EXE";
        public static string APP_EXECUTABLE_SOUNDRECORDER_PRE_WIN10 = @"C:\WINDOWS\SYSTEM32\SOUNDRECORDER.EXE";
        public static string APP_EXECUTABLE_SOUNDRECORDER_PRE_WIN10_64BIT = @"C:\WINDOWS\SYSNATIVE\SOUNDRECORDER.EXE";
        public static string APP_EXECUTABLE_INTERNETEXPLORER = @"IEXPLORE.EXE";
        public static string APP_EXECUTABLE_WINDOWSMEDIAPLAYER = @"WMPLAYER.EXE";
        public static string APP_EXECUTABLE_NOTEPAD = @"NOTEPAD.EXE";
        public static string APP_EXECUTABLE_AUDACITY = @"C:\PROGRAM FILES (X86)\AUDACITY\AUDACITY.EXE";
        public static string APP_PROCESS_INTERNETEXPLORER = @"IEXPLORE";
        public static string APP_EXECUTABLE_GOOGLEEARTH = @"C:\Program Files (x86)\Google\Google Earth\client\googleearth.exe";
        public static string APP_EXECUTABLE_ITKSNAP = @"C:\Program Files\ITK-SNAP 3.8\bin\ITK-SNAP.exe";
        public static string APP_EXECUTABLE_UNIGINE = @"C:\Program Files (x86)\Unigine\Valley Benchmark 1.0\valley.bat";
        public static string APP_EXECUTABLE_NEHE = @"Lesson30\Lesson30.exe";
        public static string APP_EXECUTABLE_3DXML = @"C:\Program Files\Dassault Systemes\3D XML Player\win_b64\code\bin\3DXMLPlayer.exe";
        public static string APP_EXECUTABLE_SMARTCARD = @"CERTUTIL ";
        public static string APP_SERVICE_SMARTCARD = @"SCardSvr";
        public static string APP_SERVICE_SMARTCARD_DEVICE_ENUM = @"ScDeviceEnum";

        public static string APP_EXECUTABLE_SKYPE = @"NOTEPAD.EXE";



        //Sample files used by different Applications 
        public static string SAMPLE_FILE_NAME_POWERPOINT = "PowerPoint_Test.ppsx";
        public static string SAMPLE_FILE_NAME_ACROBAT = "Scrolling_Test.pdf";
        public static string SAMPLE_FILE_NAME_MOVIE_WMV = "FARM_v1_720P25_NoAudio.wmv";
        public static string SAMPLE_FILE_NAME_YOUTUBE = "https://www.youtube.com/watch?v=EGCJgDb7koY?rel=0&vq=hd720";
        public static string SAMPLE_FILE_NAME_MOVIE_MP4 = "What-is-XenDesktop_H264_AAC.mp4";
        public static string SAMPLE_FILE_NAME_PRINT_DOC = "TestDoc1.doc";
        public static string SAMPLE_FILE_NAME_PRINT_PDF = "TestDoc2.pdf";
        public static string SAMPLE_FILE_NAME_PRINT_XLS = "TestDoc3.xls";
        public static string SAMPLE_FILE_NAME_PRINT_PPT = "TestDoc4.ppt";
        public static string SAMPLE_FILE_NAME_PRINT_RTF = "TestDoc5.rtf";
        public static string SAMPLE_FILE_NAME_FLASH_URL = "http://themaninblue.com/experiment/AnimationBenchmark/flash/";
        public static string SAMPLE_FILE_NAME_YOUTUBE_URL_TRAILER = "http://www.youtube.com/watch?v=JA2lKhMvMGU?rel=0&vq=hd720";
        public static string SAMPLE_FILE_NAME_HTML5 = "https://www.citrix.com/virtualization/hdx/html5-redirect.html";
        public static string SAMPLE_FILE_NAME_GOTOMEETING = "https://www.gotomeet.me/manishkumarj";
        public static string SAMPLE_FILE_NAME_3D_XML = "tractor.3dxml";
        public static string SAMPLE_FILE_NAME_ITKSNAP_IMAGE = "MRIcrop-orig.gipl";

        public static int SAMPLE_FILE_RUNTIME_POWERPOINT = 90000; //in Milliseconds - 1000 = 1sec
        public static int SAMPLE_FILE_RUNTIME_COMMANDPROMPT = 30000; //in Milliseconds - 1000 = 1sec
        public static int SAMPLE_FILE_RUNTIME_ACROBAT_SCROLL = 60000; //in Milliseconds - 1000 = 1sec
        public static int SAMPLE_FILE_RUNTIME_ACROBAT = 60000; //in Milliseconds - 1000 = 1sec
        public static int SAMPLE_FILE_RUNTIME_AUDACITY = 50000; //in milliseconds
        public static int SAMPLE_FILE_RUNTIME_MOVIE_WMV = 120000; //in Milliseconds - 1000 = 1sec
        public static int SAMPLE_FILE_RUNTIME_AUDIO_RECORDING = 45000; //in Milliseconds - 1000 = 1sec
        public static int SAMPLE_FILE_RUNTIME_YOUTUBE = 120000; //in Milliseconds - 1000 = 1sec // Full Video 235000
        public static int SAMPLE_FILE_RUNTIME_HTML5 = 150000;
        public static int SAMPLE_FILE_RUNTIME_MP4 = 100000; //in Milliseconds - 1000 = 1sec
        public static int SAMPLE_FILE_RUNTIME_NOTEPAD = 1000; //in Milliseconds - 1000 = 1sec
        public static int SAMPLE_FILE_RUNTIME_NOTEPADFPS = 30000;
        public static int SAMPLE_FILE_RUNTIME_MULTIMEDIA_VARIOUS = 20000; //in Milliseconds - 1000 = 1sec
        public static int SAMPLE_FILE_RUNTIME_3DPRO_TESTS = 30000; //in Milliseconds - 1000 = 1sec
        public static int SAMPLE_FILE_RUNTIME_GOOGLEEARTH = 30000; //in Milliseconds - 1000 = 1sec
        public static int SAMPLE_FILE_RUNTIME_3DXML = 90000; //in Milliseconds - 1000 = 1sec
        public static int SAMPLE_FILE_RUNTIME_UNIGINE = 120000; //in Milliseconds - 1000 = 1sec
        public static int SAMPLE_FILE_RUNTIME_ITKSNAP = 120000; //in Milliseconds - 1000 = 1sec
        public static int SAMPLE_FILE_RUNTIME_3DMOUSE = 90000; //in Milliseconds - 1000 = 1sec
        public static int SAMPLE_FILE_RUNTIME_NEHE = 90000;
        public static int SAMPLE_FILE_RUNTIME_MULTIWINDOW = 45000;
        public static int DUMMY_RUNTIME = 2000; //in Milliseconds - 1000 = 1sec
        public static int WaitRunTime = 5000;
        public static int FPS_CALCULATION_RUNTIME = 5000;

        public static int FPSRUNTIMEFORTC10 = 000;
        public static int eventFlag;

        public static string SAMPLE_FILE_NAME_1 = "What-is-XenDesktop_H264_AAC.mp4";
        public static string SAMPLE_FILE_NAME_2 = "What-is-XenDesktop_H264_AAC_AVC.mp4";
        public static string SAMPLE_FILE_NAME_3 = "What-is-XenDesktop_MPEG2_AC3.mpg";
        public static string SAMPLE_FILE_NAME_4 = "What-is-XenDesktop_std_MPEG4_AAC.mp4";
        public static string SAMPLE_FILE_NAME_5 = "What-is-XenDesktop_wmv9_wma9.wmv";
        public static string SAMPLE_FILE_NAME_6 = "What-is-XenDesktop.mp3";

        public static string SOFTWARE_NAME_MICROSOFT_WORD = "Microsoft Word";
        public static string SOFTWARE_NAME_MICROSOFT_POWERPOINT = "Microsoft PowerPoint";
        public static string SOFTWARE_NAME_WINDOWS_MEDIA_PLAYER = "Windows Media Player";
        public static string SOFTWARE_NAME_ADOBE_FLASH_PLUGIN = "Adobe Flash Plugin";
        public static string SOFTWARE_NAME_ADOBE_READER = "Adobe Reader";
        public static string SOFTWARE_NAME_VLC_MEDIA_PLAYER = "VLC Media Player";
        public static string SOFTWARE_NAME_HDX_MONITOR = "Citrix HDX Monitor";
        public static string SOFTWARE_NAME_SKYPE_FOR_BUSINESS2 = "Skype for Business";
        public static string SOFTWARE_NAME_EXPRESSION_ENCODER = @"Expression Encoder 4";
        public static string SOFTWARE_NAME_AUDACITY = "Audacity";
        public static string SOFTWARE_NAME_ADOBE_FLASH_PLAYER = "Adobe Flash Plugin";
        public static string SOFTWARE_NAME_GOTOMEETING = "GoToMeeting";
        public static string SOFTWARE_NAME_SKYPE_FOR_BUSINESS = "Skype for Business";
        public static string SOFTWARE_NAME_LYNC = "lync.exe";
        public static string SOFTWARE_NAME_NVIDIA_DRIVER = "NVIDIA";
        public static string SOFTWARE_NAME_GPUZ = "TechPowerUp GPU-Z";
        public static string SOFTWARE_NAME_ITK_SNAP = "ITK-SNAP";
        public static string SOFTWARE_NAME_GOOGLE_EARTH = "Google Earth";
        public static string SOFTWARE_NAME_3D_XML_PLAYER = "3D XML PLAYER";
        public static string SOFTWARE_NAME_3D_CONNEXION = "Connexion";
        public static string SOFTWARE_NAME_UNIGINE = "Unigine Valley";
        public static string SOFTWARE_NAME_VDA = "Virtual Delivery Agent";



        public static byte SOFTWARE_NUM1_MICROSOFT_WORD = 1;
        public static byte SOFTWARE_NUM2_MICROSOFT_POWERPOINT = 2;
        public static byte SOFTWARE_NUM3_WINDOWS_MEDIA_PLAYER = 3;
        public static byte SOFTWARE_NUM4_ADOBE_READER = 4;
        public static byte SOFTWARE_NUM5_VLC_MEDIA_PLAYER = 5;
        public static byte SOFTWARE_NUM6_ADOBE_FLASH_PLUGIN = 6;
        public static byte SOFTWARE_NUM7_HDX_MONITOR = 7;
        public static byte SOFTWARE_NUM11_SKYPE_FOR_BUSINESS2 = 8;
        public static byte SOFTWARE_NUM9_EXPRESSION_ENCODER = 9;
        public static byte SOFTWARE_NUM17_AUDACITY = 17;
        public static byte SOFTWARE_NUM9_ADOBE_FLASH_PLAYER = 11;
        public static byte SOFTWARE_NUM11_SKYPE_FOR_BUSINESS = 12;
        public static byte SOFTWARE_NUM9_EXPRESSION_ENCODER2 = 13;
        public static byte SOFTWARE_NUM10_NVIDIA_DRIVER = 10;
        public static byte SOFTWARE_NUM11_GPUZ = 11;
        public static byte SOFTWARE_NUM12_ITK_SNAP = 12;
        public static byte SOFTWARE_NUM13_GOOGLE_EARTH = 13;
        public static byte SOFTWARE_NUM14_3D_XML_PLAYER = 14;
        public static byte SOFTWARE_NUM15_3D_CONNEXION = 15;
        public static byte SOFTWARE_NUM16_UNIGINE = 16;
        public static byte SOFTWARE_NUM9_EXPRESSION_ENCODER3 = 21;

        public static string HTML_COLOR_GREEN = "#b6d333";
        public static string HTML_COLOR_RED = "#e74c3c";
        public static string HTML_COLOR_NORMAL = "#63666a";
        public static string HTML_COLOR_BUTTON_ENABLED = "#1979b8";
        public static string HTML_COLOR_BUTTON_DISABLED = "#d0d0ce";

        public static string REPORT_EXPORT_PASSWORD = "c1Tr1xR3@d4";

        public static string MSGBOX_CAPTION = "Citrix Ready - Thin Client Verification";

        public static string FPS_CLASS_NAME = "Citrix_VirtualChannel_Thinwire_Enum";
        public static string FPS_PARAMETER_NAME = "Component_fps";
        public static int FPS_INTERVAL = 2;
        public static int FPS_INTERVAL_FOR_3D_XML_PLAYER = 2;
        public static int FPS_TIMEOUT = 30;
        public static int FPS_SLEEP = 2000;
        public static bool FPS_DEBUG_FLAG = true;
        public static bool isLynPresent;
        public static byte CATEGORY_ID_HDX_READY = 1;
        public static string CATEGORY_NAME_HDX_READY = "HDX Ready";
        public static byte CATEGORY_ID_HDX_PREMIUM = 3;
        public static string CATEGORY_NAME_HDX_PREMIUM = "HDX Premium";
        public static byte CATEGORY_ID_HDX_3DPRO = 2;
        public static string CATEGORY_NAME_HDX_3DPRO = "HDX 3D Pro";

        public static int TC_HDX_MONITOR_TOOL = 7;
        //public static int SAMPLE_FILES_LOCATION = 15;
        public static int COMMANDPROMPT_MOVEMENT_SPEED_HORIZONTAL = 1;
        public static int COMMANDPROMPT_MOVEMENT_SPEED_DIAGONAL = 1;

        public static int Form_Width = 800;
        public static int Form_Height = 600;

        public static string PREREQUISITE_STATUS_NOT_CHECKED = "Not Checked";
        public static string PREREQUISITE_STATUS_INSTALLED = "Installed";
        public static string PREREQUISITE_STATUS_NOT_INSTALLED = "Not Installed";

        public static string VERIFICATION_STATUS_NOT_CHECKED = "Not Checked";
        public static string VERIFICATION_STATUS_IN_PROGRESS = "In Progress";
        public static string VERIFICATION_STATUS_COMPLETED = "Completed";
        public static string VERIFICATION_STATUS_SKIPPED = "Skipped";
        public static string VERIFICATION_STATUS_ERROR = "Error";

        public static string TESTCASE01_NAME = "2D Graphics - Server Rendered Virtual Apps";
        public static string TESTCASE02_NAME = "Secure Web and SaaS Apps";
        public static string TESTCASE03_NAME = "Audio Recording and Playback Support";
        public static string TESTCASE04_NAME = "Video Playback – Server Rendered Flash";
        public static string TESTCASE05_NAME = "Video Playback – Server Rendered Windows Media";
        public static string TESTCASE06_NAME = "Client Printer Test Using Citrix Universal Printer Driver";
        public static string TESTCASE07_NAME = "Common HDX Plug n Play Devices Redirection";
        public static string TESTCASE08_NAME = "HDX Multi Monitor Support";
       
        public static string TESTCASE09_NAME = "Speciality Devices Redirection - 3D Mouse Support";
        public static string TESTCASE10_NAME = "HDX Multi Monitor Support - 3D Apps";
        public static string TESTCASE11_NAME = "HDX 3D Pro Rich Graphics Apps Support";
        public static string TESTCASE12_NAME = "Pixel Perfect Lossless Compression";
        public static string TESTCASE13_NAME = "HDX P2P Video Conferencing on Skype for Business with HDX RealTime Optimization Pack";
        public static string TESTCASE14_NAME = "Citrix SD-WAN HDX Optimization";
        public static string TESTCASE15_NAME = "Citrix Gateway and Citrix ADM (formerly NetScaler MAS) verification";

        public static string TESTCASE16_NAME = "H265 decoding for graphics";
        public static string TESTCASE17_NAME = "HDX Real-Time Audio (VoIP)";
        public static string TESTCASE18_NAME = "HDX Media Stream Flash Redirection";
        public static string TESTCASE19_NAME = "HTML5 Video Redirection";
        public static string TESTCASE20_NAME = "HDX Media Stream Windows Media Redirection";
        public static string TESTCASE21_NAME = "HDX RealTime Webcam Compression";
        public static string TESTCASE22_NAME = "Browser Content Redirection";
        public static string TESTCASE23_NAME = "HDX PIV Smartcard On-Premises Support";

        public static byte TESTCASE_NUM1_HDX_READY_2D_GRAPHICS = 1;
        public static byte TESTCASE_NUM2_HDX_READY_SECURE_WEB_SAAS_APPS = 2;
        public static byte TESTCASE_NUM3_HDX_READY_AUDIO_RECORDING_PLAYBACK = 3;
        public static byte TESTCASE_NUM4_HDX_READY_VIDEO_PLAYBACK_SERVER_RENDERED_FLASH = 4;
        public static byte TESTCASE_NUM5_HDX_READY_VIDEO_PLAYBACK_SERVER_RENDERED_WINDOWS_MEDIA = 5;
        public static byte TESTCASE_NUM6_HDX_READY_CLIENT_PRINTER = 6;
        public static byte TESTCASE_NUM7_HDX_READY_PLUG_PLAY_DEVICE_REDIRECTION = 7;
        public static byte TESTCASE_NUM8_HDX_READY_MULTI_MONITOR_SUPPORT = 8;

        public static byte TESTCASE_NUM9_HDX_READY_3D_MOUSE = 9;
        public static byte TESTCASE_NUM10_HDX_READY_MULTI_MONITOR_SUPPORT_3D_APPS = 10;
        public static byte TESTCASE_NUM11_HDX_READY_3DPRO_RICH_GRAPHICS = 11;
        public static byte TESTCASE_NUM12_HDX_READY_PIXEL_PERFECT_LOSSLESS_COMPRESSION = 12;
        public static byte TESTCASE_NUM13_HDX_READY_P2P_VIDEO_CONFERENCE = 13;
        public static byte TESTCASE_NUM14_HDX_READY_SD_WAN_HDX_OPTIMIZATION = 14;
        public static byte TESTCASE_NUM15_HDX_READY_CITRIX_GATEWAY_ADM = 15;

        public static byte TESTCASE_NUM16_HDX_PREMIUM_H265_DECODING_GRAPHICS = 16;
        public static byte TESTCASE_NUM17_HDX_PREMIUM_REALTIME_AUDIO_VOIP = 17;
        public static byte TESTCASE_NUM18_HDX_PREMIUM_MEDIA_STREAM_FLASH_REDIRECTION = 18;
        public static byte TESTCASE_NUM19_HDX_PREMIUM_HTML5_VIDEO_REDIRECTION = 19;
        public static byte TESTCASE_NUM20_HDX_PREMIUM_MEDIA_STREAM_WINDOWS_REDIRECTION = 20;
        public static byte TESTCASE_NUM21_HDX_PREMIUM_REALTIME_WEBCAM_COMPRESSION = 21;
        public static byte TESTCASE_NUM22_HDX_PREMIUM_BROWSER_CONTENT_REDIRECTION = 22;
        public static byte TESTCASE_NUM23_HDX_PREMIUM_PIV_SMARTCARD_ONPRIMISES_SUPPORT = 23;

        #endregion
        public static bool isRecorderOpen = false;

        public static string FolderToSaveVideo = "";


        public static string JSONfileName1 = @"QSN_RTOP.xml";
        public static string JSONfileName = @"QSN_VOIP.xml";
        public static string XMLPath1 = Environment.CurrentDirectory + @"\QSN_RTOP.xml";
        public static string XMLPath2 = Environment.CurrentDirectory + @"\QSN_VOIP.xml";

        public static string GptFilePath1 = Environment.CurrentDirectory + @"\Standard.gpt";
        public static string GptFilePath2 = Environment.CurrentDirectory + @"\Premium.gpt";
        public static string GptFilePath3 = Environment.CurrentDirectory + @"\Standard contd.gpt";

        //API user credentials
        public static string Username = "cr_verification_api_user@citrix.com";
        public static string Password = "c1Tr1xR3@d4";
        public static string Subdomain = "citrixready";
        public static string ControlPlane = "sharefile.com";
        public static string clientId = "DAKy07MlTIPyVM6VvrrT8J4PhgWM9v4E";
        public static string clientSecret = "aPEGQGLg1Mc3sFRGYyITXDTBgBR2AdOm7GmYbjH4x5j6BwBm";
        public static ShareFileClient sfClient;

        public static List<Uri> XMLitemsUri = new List<Uri>();
        public static Uri XMLFolderUri = new Uri("https://citrixready.sf-api.com/sf/v3/Items(foe451d1-34ba-4769-903d-93b534cad104)");
        //public static Uri folderToUploadUri = new Uri("https://citrixready.sf-api.com/sf/v3/Items(fo32c9e6-1c64-420c-bfa1-e4116a262259)");
        public static Uri folderToUploadUri = new Uri("https://citrixready.sf-api.com/sf/v3/Items(fo6a157c-5f7b-4463-a721-4585b7ec345c)");
        public static string shareXMLFolderID = "sf69c7ab45de47289";
        public static Uri XMLfileUri = new Uri("https://citrixready.sf-api.com/sf/v3/Items(d-s60252aced4b4dda9)");


        public static bool isfileDownloaded = false;
        public static bool errorInDownlaod = false;
        public static bool errorWithDownload = false;

        public static bool IsGuidelines = false;
        public static bool IsEncoder = false;
        public static bool IsNewVideo = false;
        public static string VideoToUpload = "";

        public static string VideoFileName = "";
        public static string VideoDownloadLink = "";
        public static string CitrixStudioDomainName = "";
        public static string CitrixStudioIP = "";
        public static string CitrixStudioUserName = "";
        public static string CitrixStudioPassword = "";

        public static string policyname = "";
        public static string templatename = "";

        public static bool HDXReadyPolicyStatus = false;
        public static bool HDXPremiumPolicyStatus = false;
        public static bool HDX3DPolicyStatus = false;

        public static string PolicyExistResponse = "";

        public static bool HDXReadyTab = false;
        public static bool HDXPremiumTab = false;
        public static bool HDX3DTab = false;


        //public static string tempHDXPremium = "";
        //public static string tempHDXReady = "";
        //public static string tempHDX3D = "";
        internal static string tempLoadingFlag = "";

        public static string ValidationMsg = "";
        public static bool CurrentPolicyStatusChanged = false;

        public static string VDAversion = "";
        public static string VDAdisplayName = "";
    }
}
