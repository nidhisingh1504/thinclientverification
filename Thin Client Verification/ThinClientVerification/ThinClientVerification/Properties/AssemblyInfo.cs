﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("CitrixReady Thin Client Verification")]
[assembly: AssemblyDescription("CitrixReady Thin Client Verification (HDX Ready, HDX Premium, HDX 3D Pro)")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Citrix Systems, Inc.")]
[assembly: AssemblyProduct("CitrixReady Thin Client Verification")]
[assembly: AssemblyCopyright("Copyright 1999-2016 Citrix Systems, Inc. All Rights Reserved.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("026d4096-47e0-4304-a2c3-6df836e3b29b")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
