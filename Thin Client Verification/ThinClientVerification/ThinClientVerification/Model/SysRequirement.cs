﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThinClient.Verification.Model
{
    public class SysRequirement
    {
        public byte ID { get; set; }
        public string DisplayName { get; set; }
        public string[] InternalNames { get; set; }
        public string DownloadLinkText { get; set; }
        public string DownloadLinkURL { get; set; }
        public bool IsLicensed { get; set; }
        public bool IsInstalled { get; set; }
        public string Status { get; set; }
        public string Category { get; set; }
        public string Comments { get; set; }
    }
}
