﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThinClient.Verification.Model
{
    public class TestCase
    {
        public byte ID { get; set; }

        public string Category { get; set; }
        public string ScenarioTitle { get; set; }
        public string Status { get; set; }
        public string Result { get; set; }
        public string Comments { get; set; }
        public string DownloadLink { get; set; }

    }
}
