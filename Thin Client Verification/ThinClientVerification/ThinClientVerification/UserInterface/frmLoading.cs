﻿using ThinClient.Verification.Common;
using ThinClient.Verification.Model;
using System;
using System.Windows.Forms;

namespace ThinClient.Verification.UserInterface
{
    public partial class frmLoading : Form
    {
        public frmLoading()
        {
            InitializeComponent();
        }
        Timer tmr;
        private void frmLoading_Load(object sender, EventArgs e)
        {
            tmr = new Timer();
            tmr.Interval = 1000;
            tmr.Start();
            tmr.Tick += tmr_Tick;
        }
        private void frmLoading_Shown(object sender, EventArgs e)
        {

        }

        void tmr_Tick(object sender, EventArgs e)
        {
            if (Constants.tempLoadingFlag == "success")
            {
                Constants.tempLoadingFlag = "";
                tmr.Stop();
                this.Close();
            }
        }

    }
}
