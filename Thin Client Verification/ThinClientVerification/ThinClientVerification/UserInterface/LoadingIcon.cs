﻿using ThinClient.Verification.Common;
using ThinClient.Verification.Model;
using System;
using System.Windows.Forms;

namespace ThinClient.Verification.UserInterface
{
    public partial class LoadingIcon : Form
    {
        public LoadingIcon()
        {
            InitializeComponent();
        }

        private async void LoadingIcon_Load(object sender, EventArgs e)
        {
            string msgUser = "";
            msgUser = await Utilities.DownloadJson();
            while (!Constants.isfileDownloaded)
            {
                if (msgUser != "Success")
                {
                    if (MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error) == DialogResult.OK)
                    { Application.Exit(); }
                    break;
                }
            }
            if (Constants.errorWithDownload) { Application.Exit(); }
            if (msgUser != "Success") { this.Close(); Constants.isfileDownloaded = false; }
            this.Close();
        }
    }
}
