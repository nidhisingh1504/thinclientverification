﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ThinClient.Verification.Common;
using ThinClient.Verification.Model;
using ThinClient.Verification.UserInterface;

namespace ThinClientVerification.UserInterface
{
    public partial class frmFloatingWindow : Form
    {
        Form _frm;
        int _testcaseid;
        Process _ps;
        //bool isAudacityTaskCompleted = false;
        public frmFloatingWindow(frmVerification frmVerify, int testcaseId, Process ps)
        {
            _frm = frmVerify;
            _frm.Hide();
            _testcaseid = testcaseId;
            _ps = ps;
            Globals.isAudacityTestCaseCompleted = false;
            InitializeComponent();

            #region-commented
            /*if (testcaseId == 9)
            {
                timerDecrement = 45;
                Task taskForAudacity = Task.Factory.StartNew(() => isAudacityTaskCompleted = Utilities.ProcessMonitor(_ps, Constants.SAMPLE_FILE_RUNTIME_AUDACITY))
                                                    .ContinueWith(_ =>
                                                    {
                                                        this.Close();
                                                    }, CancellationToken.None
                                                    , TaskContinuationOptions.None
                                                    , TaskScheduler.FromCurrentSynchronizationContext())
                                                    .ContinueWith(_ =>
                                                    {
                                                        _frm.Show();                                                        
                                                    }, CancellationToken.None
                                                    , TaskContinuationOptions.None
                                                    , TaskScheduler.FromCurrentSynchronizationContext());
            }*/
            #endregion
        }

        static int counter = 0;
        int mywidth;
        int myheight;

        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        private void frmFloatingWindow_Load(object sender, EventArgs e)
        {
            this.BackColor = Color.Aqua;
            this.TransparencyKey = Color.Aqua;

            Screen myScreen = Screen.PrimaryScreen;
            mywidth = (myScreen.WorkingArea.Width);
            myheight = (myScreen.WorkingArea.Height);

            this.Location = new Point(mywidth - 200, 20);
            //if (_testcaseid == 9)
            //{
            //    btnCapture.Text = "Capture Screen";

            //    timer.Interval = 1000;
            //    timer.Tick += new EventHandler(Timer_Tick);
            //    timer.Start();
            //}
        }

        /*private void Timer_Tick(object sender, EventArgs e)
        {
            btnCapture.Text = "Capture Screen (" + timerDecrement.ToString() + ")";
            if (timerDecrement == 0)
            {
                timer.Stop();
                Globals.isAudacityTestCaseCompleted = true;
            }
            timerDecrement--;
        }*/

        private void btnCapture_Click(object sender, EventArgs e)
        {
            Globals.isAudacityTestCaseCompleted = true;
            btnCapture.Cursor = Cursors.WaitCursor;
            counter++;
            int screenLeft = SystemInformation.VirtualScreen.Left;
            int screenTop = SystemInformation.VirtualScreen.Top;
            int screenWidth = SystemInformation.VirtualScreen.Width;
            int screenHeight = SystemInformation.VirtualScreen.Height;
            Bitmap bmpScreenshot = new Bitmap(screenWidth, screenHeight, PixelFormat.Format32bppArgb);
            var gfxScreenshot = Graphics.FromImage(bmpScreenshot);
            gfxScreenshot.CopyFromScreen(screenLeft, screenTop, 0, 0, bmpScreenshot.Size, CopyPixelOperation.SourceCopy);
            string currentDirectory = System.IO.Directory.GetCurrentDirectory();
            CreateFolderIfNotAvailable(currentDirectory + "\\Screenshots");

            Globals.screenshotName = System.IO.Directory.GetCurrentDirectory() + "\\Screenshots\\Screenshot_BtnCaptured_" + DateTime.Now.ToString("yyyyMMddTHHmmssfff") + ".jpeg";
            bmpScreenshot.Save(Globals.screenshotName, ImageFormat.Jpeg);
            btnCapture.Cursor = Cursors.Default;

            frmSplashScreen splash = new frmSplashScreen();
            splash.Width = mywidth - 100;
            splash.Height = myheight - 50;
            splash.ShowDialog();

            Control[] cl = _frm.Controls.Find("txtResult", true);
            TextBox textBox1 = (TextBox)cl[0];
            textBox1.AppendText("Screenshot saved successfully and the path is " + Globals.screenshotName + "\n");

            if (_testcaseid == 9)
            {
                _ps.Kill();
            }
            this.Hide();
            _frm.Show();
        }
        private void CreateFolderIfNotAvailable(string path)
        {
            bool folderExists = Directory.Exists(path);
            if (!folderExists)
            {
                Directory.CreateDirectory(path);
            }
        }
    }
}
