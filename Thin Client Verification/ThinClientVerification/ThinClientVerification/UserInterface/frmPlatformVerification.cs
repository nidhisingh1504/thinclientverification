﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using System.Security.Principal;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using ThinClient.Verification.Common;
using ThinClient.Verification.UserInterface;

namespace ThinClientVerification
{

    public partial class frmPlatformVerification : Form
    {
        public frmPlatformVerification()
        {
            InitializeComponent();
            selectDefaultSetup();           
            lblSeperatorLine1.Visible = false;       

            this.AcceptButton = btnSubmit;
        }

        private void selectDefaultSetup()
        {
            radCxCloud.Select();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            string newFileName = System.IO.Path.GetRandomFileName();
            if(radCxCloud.Checked)
                this.DialogResult = DialogResult.Yes;
            else
                this.DialogResult = DialogResult.No;  
        }
    }
}
