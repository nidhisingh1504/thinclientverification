﻿using ThinClient.Verification.Common;
using ThinClient.Verification.Model;
using System;
using System.Windows.Forms;

namespace ThinClient.Verification.UserInterface
{
    public partial class UploadLoadingIcon : Form
    {
        public UploadLoadingIcon()
        {
            InitializeComponent();
        }

        private async  void LoadingIcon_Load(object sender, EventArgs e)
        {
            try
            {
                string msgUser = "";
                msgUser = await frmVerification.UploadVideo();

                if (Globals.isUpload == true)
                {
                    this.Close();
                    Globals.isUpload = false;
                }
            }
            catch(Exception ex)
            {
                string msg = "Unable to establish a connection with Citrix Ready ShareFile server." + "\n" + "\n" +
                                      "This could be caused by a loss of network connectivity or inadequate permission to connect to the Citrix Ready ShareFile." + "\n" + "\n" +
                                      "If the problem persists, please reach out to us at CitrixReady@citrix.com";
                MessageBox.Show(msg, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
         
            

        }
    }
}
