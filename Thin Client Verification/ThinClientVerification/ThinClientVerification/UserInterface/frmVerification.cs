﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Management;
using ThinClient.Verification.Common;
using ThinClient.Verification.Model;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading.Tasks;
using ThinClientVerification.UserInterface;
using System.Security.Permissions;
using Microsoft.Win32;
using ShareFile.Api.Client.Transfers;
using ShareFile.Api.Client;
using ShareFile.Api.Models;
using ShareFile.Api.Client.Logging;
using ShareFile.Api.Client.Security.Authentication.OAuth2;
using ShareFile.Api.Client.FileSystem;
using ShareFile.Api.Client.Extensions;
using System.Collections.Generic;
using ThinClientVerification.Model;
using System.ComponentModel;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Security;
using System.Text.RegularExpressions;

namespace ThinClient.Verification.UserInterface
{
    public partial class frmVerification : Form
    {
        public struct SampleUser
        {
            public string Username { get; set; }
            public string Password { get; set; }
            public string Subdomain { get; set; }
            public string ControlPlane { get; set; }
        }

        #region - Mouse click read
        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
        public class MouseClickFilter : IMessageFilter
        {
            public bool PreFilterMessage(ref Message m)
            {
                // Blocks all the messages relating to the left mouse button.
                if (m.Msg == 0x201 || m.Msg == 0x202 || m.Msg == 0x203) { return true; }
                if (m.Msg == 0x204 || m.Msg == 0x205 || m.Msg == 0x206) { return true; }
                return false;
            }
        }
        #endregion

        public string msgUser = "";

        public static bool flag = false;

        [DllImport("user32.dll", SetLastError = true)]
        internal static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);

        [DllImport("user32.dll", SetLastError = true)]
        static extern void keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll")]
        static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

        public static int SW_FORCEMINIMIZE = 11;

        public const int KEYEVENTF_KEYUP = 0x02;
        public const byte VK_RIGHT = 0x27;
        public const byte VK_DOWN = 0x28;
        public const byte VK_UP = 0x26;
        public const byte VK_ADD = 0x6B;

        const UInt32 SWP_NOSIZE = 0x0001;
        const UInt32 SWP_NOMOVE = 0x0002;
        const UInt32 SWP_SHOWWINDOW = 0x0040;

        public static frmTimer timerFrm;

        public static CancellationTokenSource tokenSource;
        public static CancellationToken token;

        static System.Timers.Timer scrnCaptureTimer = new System.Timers.Timer();

        static int validateClicklCount = 0;
        //static frmFloatingWindow captureFrm;
        public static frmVideoRecording frmVideo;
        public static frmQuestionBanner frmQsnBanner;
        static System.Diagnostics.Stopwatch watcher = new System.Diagnostics.Stopwatch();
        public static long bytesTansferd;
        public static long bytesofOriginalFile;
        public static ShareFileClient sfClient;
        public static Folder folderWithProductName;
        public static string Status = "";
        public static Dictionary<Uri, Item> itemData;
        private static BindingList<FileDetails> fileDetail = new BindingList<FileDetails>();



        #region - Windows Forms - Inbuilt Events
        public frmVerification()
        {
            InitializeComponent();
            this.FormClosing += frmVerification_FormClosing;
            //pictureBox1.Visible = false;
            lblprocessing.Visible = false;
            lblprocessing.Text = "Please wait while the policies are being configured in Citrix Studio....";
            
        }

        private async void frmVerification_Load(object sender, EventArgs e)
        {


            lblBuildNumber.Text = Constants.BUILD_NUMBER;
            lblSampleFileLocationValue.Text = Globals.SampleFilesLocation;

            if (Globals.Selected_HDX_Category == Convert.ToByte(Globals.HDX_Category.HDX_Ready))
            {
                tabTestCases.SelectedTab = tabHDXReady;
            }
            if (Globals.Selected_HDX_Category == Convert.ToByte(Globals.HDX_Category.HDX_Premium))
            {
                tabTestCases.SelectedTab = tabHDXPremium;
            }
            if (Globals.Selected_HDX_Category == Convert.ToByte(Globals.HDX_Category.HDX_3DPro))
            {
                tabTestCases.SelectedTab = tabHDX3DPro;
            }

            LoadToolTips();

            txtResult.Clear();
            txtResult.AppendText(Globals.VerificationScreenResult.ToString());


            var user = new SampleUser
            {
                ControlPlane = Constants.ControlPlane,
                Username = Constants.Username,
                Password = Constants.Password,
                Subdomain = Constants.Subdomain
            };

            string clientId = Constants.clientId;
            string clientSecret = Constants.clientSecret;
            try
            {
                // Authenticate with username/password
                sfClient = await PasswordAuthentication(user, clientId, clientSecret);

                // Create a Session
                await StartSession(sfClient);
            }
            catch (Exception ex)
            {
                string msgUser = "Unable to establish a connection with Citrix Ready ShareFile server." + "\n" + "\n" +
                               "This could be caused by a loss of network connectivity or inadequate permission to connect to the Citrix Ready ShareFile." + "\n" + "\n" +
                               "If the problem persists, please reach out to us at CitrixReady@citrix.com";
                MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
                //this.Close();
            }

            Constants.VideoDownloadLink = "";



        }

        private void frmVerification_Shown(object sender, EventArgs e)
        {
            ShowVerificationCheckStatus();
            InitForm();
        }

        private void frmVerification_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                string msgUser = "Before you exit the application, do save the reports by clicking on the ‘Generate Report’ to view the results of the test scenarios. If you wish to continue verifying your product with other HDX levels, please click on the appropriate tab and continue the verification process." + "\n" + "\n" +
               "Click on 'Save Report' to save a local copy of the results as a PDF format for your reference." + "\n" + "\n" +
               "Click on ‘Export Report’ to save the results in a password protected PDF. This PDF file needs to be to uploaded on the Citrix Ready Verification Platform to validate the results and approve the product." + "\n" + "\n" +
               "Click OK to exit the application or Cancel to abort.";
                DialogResult result = MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                if (result == DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
                else
                {
                    Application.Exit();
                    //string screensDirectory = System.IO.Directory.GetCurrentDirectory() + "\\Screenshots";
                    //DirectoryInfo screensdir = new DirectoryInfo(screensDirectory);
                    //screensdir.Delete(true);
                    Constants.CitrixStudioIP = "";
                    Constants.CitrixStudioDomainName = "";
                    Constants.CitrixStudioUserName = "";
                    Constants.CitrixStudioPassword = "";
                }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            string msgUser = "Before you exit the application, do save the reports by clicking on the ‘Generate Report’ to view the results of the test scenarios. If you wish to continue verifying your product with other HDX levels, please click on the appropriate tab and continue the verification process." + "\n" + "\n" +
               "Click on 'Save Report' to save a local copy of the results as a PDF format for your reference." + "\n" + "\n" +
               "Click on ‘Export Report’ to save the results in a password protected PDF. This PDF file needs to be to uploaded on the Citrix Ready Verification Platform to validate the results and approve the product." + "\n" + "\n" +
               "Click OK to exit the application or Cancel to abort.";
            DialogResult result = MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (result == DialogResult.OK)
            {
                Application.Exit();
                //string screensDirectory = System.IO.Directory.GetCurrentDirectory() + "\\Screenshots";
                //DirectoryInfo screensdir = new DirectoryInfo(screensDirectory);
                //screensdir.Delete(true);

            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            BackButton();
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            var frmRpt = new frmReport(txtResult.Text);
            frmRpt.ShowDialog();
        }

        private void btnSampleFilesLocation_Click(object sender, EventArgs e)
        {
            folderBrowserDialogSampleFileLocation.ShowDialog();
            if (!string.IsNullOrWhiteSpace(folderBrowserDialogSampleFileLocation.SelectedPath))
            {
                lblSampleFileLocationValue.Text = folderBrowserDialogSampleFileLocation.SelectedPath;
                if (lblSampleFileLocation.Text.EndsWith(@"\"))
                {
                    Globals.SampleFilesLocation = lblSampleFileLocationValue.Text;
                }
                else
                {
                    Globals.SampleFilesLocation = lblSampleFileLocationValue.Text + @"\";
                }
            }
        }

        private void chkTestCaseHDXReady_CheckedChanged(object sender, EventArgs e)
        {
            chkTestCase1.Checked = chkTestCaseHDXReady.Checked;
            chkTestCase2.Checked = chkTestCaseHDXReady.Checked;
            chkTestCase3.Checked = chkTestCaseHDXReady.Checked;
            chkTestCase4.Checked = chkTestCaseHDXReady.Checked;
            chkTestCase5.Checked = chkTestCaseHDXReady.Checked;
            chkTestCase6.Checked = chkTestCaseHDXReady.Checked;
            chkTestCase8.Checked = chkTestCaseHDXReady.Checked;
            
            chkTestCase7.Checked = chkTestCaseHDXReady.Checked;
        }

        private void chkTestCaseHDXPremium_CheckedChanged(object sender, EventArgs e)
        {
            chkTestCase16.Checked = chkTestCaseHDXPremium.Checked;
            chkTestCase17.Checked = chkTestCaseHDXPremium.Checked;
            chkTestCase18.Checked = chkTestCaseHDXPremium.Checked;
            chkTestCase19.Checked = chkTestCaseHDXPremium.Checked;
            chkTestCase20.Checked = chkTestCaseHDXPremium.Checked;
            chkTestCase21.Checked = chkTestCaseHDXPremium.Checked;
            chkTestCase22.Checked = chkTestCaseHDXPremium.Checked;
            chkTestCase23.Checked = chkTestCaseHDXPremium.Checked;
        }

        private void chkTestCaseHDX3DPro_CheckedChanged(object sender, EventArgs e)
        {
            chkTestCase9.Checked = chkTestCaseHDX3DPro.Checked;
           
            chkTestCase10.Checked = chkTestCaseHDX3DPro.Checked;
            chkTestCase11.Checked = chkTestCaseHDX3DPro.Checked;
            chkTestCase12.Checked = chkTestCaseHDX3DPro.Checked;
            chkTestCase13.Checked = chkTestCaseHDX3DPro.Checked;
            chkTestCase14.Checked = chkTestCaseHDX3DPro.Checked;
            chkTestCase15.Checked = chkTestCaseHDX3DPro.Checked;
        }
        // PolicyStatusChsnged =true 

        private async void btnValidate_Click(object sender, EventArgs e)
        {
            // take clicked tab value , then enable as per value 
            string msgUser = "";
            string returnedStatus = "";
            byte TestCaseNumber = 0;
            #region -VDA TEST
            //String value = "";
            //String output = "";
            //ManagementObjectSearcher ctxFPS = new ManagementObjectSearcher(@"root\Citrix\hdx", @"SELECT * FROM Citrix_XenDesktop ");
            //if (ctxFPS.Get().Count > 0)
            //{
            //    foreach (ManagementObject mo in ctxFPS.Get())
            //    {
            //        if (mo["Version"] != null)
            //        {
            //            value = (mo["Version"]).ToString() + "";
            //            ///fpsCurrent = Convert.ToInt32(mo[Constants.FPS_PARAMETER_NAME]);
            //            //CurrentFPSInfo = "FPS recording for " + Scenario + " at " + DateTime.Now.ToString() + ": " + Convert.ToString(fpsCurrent);
            //            //fpsValueSum = fpsValueSum + fpsCurrent;

            //            break;
            //        }
            //    }
            //    MessageBox.Show(value);
            //}

            #endregion
            if (Globals.Selected_Setup_Choice == Convert.ToByte(Globals.Setup_Choice.ON_Prim))
            {
              
                //MessageBox.Show("On Prim", Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
                if (tabTestCases.SelectedTab == tabHDXReady)
                {
                    if (chkTestCase1.Checked || chkTestCase2.Checked || chkTestCase3.Checked || chkTestCase4.Checked || chkTestCase5.Checked || chkTestCase6.Checked || chkTestCase8.Checked ||  chkTestCase7.Checked)
                    {

                        if (Constants.CurrentPolicyStatusChanged == false)
                        {
                            //ctureBox1.Visible = true;
                            lblprocessing.Visible = true;
                            DisableItem();
                            await Task.Factory.StartNew(() => Constants.tempLoadingFlag = HDXReady());
                            Constants.HDXReadyTab = true;
                            // pictureBox1.Visible = false;
                            lblprocessing.Visible = false;
                            EnableItem();
                            Constants.CurrentPolicyStatusChanged = true;

                        }
                    }
                }


                if (tabTestCases.SelectedTab == tabHDX3DPro)
                {
                    if (chkTestCase9.Checked || chkTestCase10.Checked || chkTestCase11.Checked || chkTestCase12.Checked || chkTestCase13.Checked || chkTestCase14.Checked || chkTestCase15.Checked)
                    {
                        if (Constants.CurrentPolicyStatusChanged == false)
                        {
                            //ctureBox1.Visible = true;
                            lblprocessing.Visible = true;
                            DisableItem();
                            await Task.Factory.StartNew(() => Constants.tempLoadingFlag = HDX3d());
                            Constants.HDX3DTab = true;
                            //ictureBox1.Visible = false;
                            lblprocessing.Visible = false;
                            EnableItem();
                            Constants.CurrentPolicyStatusChanged = true;

                        }
                    }
                }
                if (tabTestCases.SelectedTab == tabHDXPremium)
                {

                    if (Globals.IsSystemRequirementsCheckCompletedForHDXPremium)
                    {

                        if (chkTestCase16.Checked || chkTestCase17.Checked || chkTestCase18.Checked || chkTestCase19.Checked || chkTestCase20.Checked || chkTestCase21.Checked || chkTestCase22.Checked || chkTestCase23.Checked)
                        {
                            if (Constants.CurrentPolicyStatusChanged == false)
                            {
                                //ictureBox1.Visible = true;
                                lblprocessing.Visible = true;
                                DisableItem();
                                await Task.Factory.StartNew(() => Constants.tempLoadingFlag = HDXPremium());
                                Constants.HDXPremiumTab = true;
                                //ictureBox1.Visible = false;
                                lblprocessing.Visible = false;
                                EnableItem();
                                Constants.CurrentPolicyStatusChanged = true;

                            }
                        }

                    }
                    else
                    {
                        msgUser = "HDX Premium Preqrequiste Check has not been completed!";
                        MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        BackButton();
                        return;
                    }
                    
                }



            }
            else if (Globals.Selected_Setup_Choice == Convert.ToByte(Globals.Setup_Choice.Cloud))
            {
                //MessageBox.Show("Cloud", Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
           

            MouseClickFilter filter = new MouseClickFilter();
            //if(!ChkHDXRTOP.Checked && !chkTestCase12.Checked && !chkTestCase9.Checked)
            //{
            if (!Directory.Exists(lblSampleFileLocationValue.Text))
            {
                msgUser = "Please select an appropriate Sample Files Location." + Globals.SampleFilesLocation + "\n" + "\n";
                MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);

                return;
            }
            //}

            if (validateClicklCount == 0)
            {
                msgUser = "All other applications need to be closed be before starting test cases!" + "\n" +
                              "Click OK to close the applications";
                MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                validateClicklCount = 1;
            }

            btnValidate.Enabled = false;
            btnExit.Enabled = false;

            #region - HDXREADY
            if (tabTestCases.SelectedTab == tabHDXReady)
            {
                // 

                if (chkTestCase1.Checked || chkTestCase2.Checked || chkTestCase3.Checked || chkTestCase4.Checked || chkTestCase5.Checked || chkTestCase6.Checked || chkTestCase8.Checked  || chkTestCase7.Checked)
                {

                    Globals.policytitle = "1";
                    string[] processList = new string[] { "acrord32", "vlc", "powerpnt", "Winword", "wmplayer", "iexplore" };
                    if (Utilities.CheckOpenedApplications(processList))
                    {
                        foreach (string processName in processList)
                        {
                            this.Refresh();
                            Utilities.CloseAllOpenedApplications(processName);
                        }
                    }
                    if (Globals.IsSystemRequirementsCheckCompletedForHDXReady)
                    {

                        #region - TC1
                        TestCaseNumber = Constants.TESTCASE_NUM1_HDX_READY_2D_GRAPHICS;
                        if (chkTestCase1.Checked)
                        {
                            lblStatus1.Text = Constants.VERIFICATION_STATUS_IN_PROGRESS; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            returnedStatus = TC1_2DGraphics(TestCaseNumber);
                            lblStatus1.Text = returnedStatus; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);
                        }
                        else
                        {
                            if ((TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_COMPLETED) && (TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_ERROR))
                            {
                                TestCases.oTestCases[TestCaseNumber - 1].Status = Constants.VERIFICATION_STATUS_SKIPPED;
                                lblStatus1.Text = Constants.VERIFICATION_STATUS_SKIPPED; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            }
                        }

                        this.Refresh();
                        #endregion
                        
                        #region - TC2
                        TestCaseNumber = Constants.TESTCASE_NUM2_HDX_READY_SECURE_WEB_SAAS_APPS;
                        if (chkTestCase2.Checked)
                        {
                            lblStatus2.Text = Constants.VERIFICATION_STATUS_IN_PROGRESS; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            returnedStatus = TC2_SecureWebSAASApps(TestCaseNumber);
                            lblStatus2.Text = returnedStatus; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);
                        }
                        else
                        {
                            if ((TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_COMPLETED) && (TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_ERROR))
                            {
                                TestCases.oTestCases[TestCaseNumber - 1].Status = Constants.VERIFICATION_STATUS_SKIPPED;
                                lblStatus2.Text = Constants.VERIFICATION_STATUS_SKIPPED; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            }
                        }

                        this.Refresh();
                        #endregion

                        #region - TC3

                        TestCaseNumber = Constants.TESTCASE_NUM3_HDX_READY_AUDIO_RECORDING_PLAYBACK;
                        if (chkTestCase3.Checked)
                        {
                            lblStatus3.Text = Constants.VERIFICATION_STATUS_IN_PROGRESS; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            returnedStatus = TC3_AudioRec(TestCaseNumber);
                            lblStatus3.Text = returnedStatus; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);
                        }
                        else
                        {
                            if ((TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_COMPLETED) && (TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_ERROR))
                            {
                                TestCases.oTestCases[TestCaseNumber - 1].Status = Constants.VERIFICATION_STATUS_SKIPPED;
                                lblStatus3.Text = Constants.VERIFICATION_STATUS_SKIPPED; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            }
                        }

                        this.Refresh();
                        #endregion

                        #region - TC4
                        TestCaseNumber = Constants.TESTCASE_NUM4_HDX_READY_VIDEO_PLAYBACK_SERVER_RENDERED_FLASH;
                        if (chkTestCase4.Checked)
                        {
                            lblStatus4.Text = Constants.VERIFICATION_STATUS_IN_PROGRESS; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            returnedStatus = TC4_VidPlaybackFlash(TestCaseNumber);
                            lblStatus4.Text = returnedStatus; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);
                        }
                        else
                        {
                            if ((TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_COMPLETED) && (TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_ERROR))
                            {
                                TestCases.oTestCases[TestCaseNumber - 1].Status = Constants.VERIFICATION_STATUS_SKIPPED;
                                lblStatus4.Text = Constants.VERIFICATION_STATUS_SKIPPED; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            }
                        }

                        this.Refresh();
                        #endregion

                        #region - TC5
                        TestCaseNumber = Constants.TESTCASE_NUM5_HDX_READY_VIDEO_PLAYBACK_SERVER_RENDERED_WINDOWS_MEDIA;
                        if (chkTestCase5.Checked)
                        {
                            lblStatus5.Text = Constants.VERIFICATION_STATUS_IN_PROGRESS; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            returnedStatus = TC5_VidPlaybackPlayer(TestCaseNumber);
                            lblStatus5.Text = returnedStatus; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);
                        }
                        else
                        {
                            if ((TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_COMPLETED) && (TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_ERROR))
                            {
                                TestCases.oTestCases[TestCaseNumber - 1].Status = Constants.VERIFICATION_STATUS_SKIPPED;
                                lblStatus5.Text = Constants.VERIFICATION_STATUS_SKIPPED; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            }
                        }

                        this.Refresh();
                        #endregion

                        #region - TC6
                        TestCaseNumber = Constants.TESTCASE_NUM6_HDX_READY_CLIENT_PRINTER;
                        if (chkTestCase6.Checked)
                        {
                            lblStatus6.Text = Constants.VERIFICATION_STATUS_IN_PROGRESS; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            returnedStatus = TC6_ClientPrintTest(TestCaseNumber);
                            lblStatus6.Text = returnedStatus; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);
                        }
                        else
                        {
                            if ((TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_COMPLETED) && (TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_ERROR))
                            {
                                TestCases.oTestCases[TestCaseNumber - 1].Status = Constants.VERIFICATION_STATUS_SKIPPED;
                                lblStatus6.Text = Constants.VERIFICATION_STATUS_SKIPPED; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            }
                        }

                        this.Refresh();

                        #endregion

                        #region - TC7
                        TestCaseNumber = Constants.TESTCASE_NUM7_HDX_READY_PLUG_PLAY_DEVICE_REDIRECTION;
                        if (chkTestCase7.Checked)
                        {
                            lblStatus7.Text = Constants.VERIFICATION_STATUS_IN_PROGRESS; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            returnedStatus = TC7_PlugPlayDeviceRedirection(TestCaseNumber);
                            lblStatus7.Text = returnedStatus; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);
                        }
                        else
                        {
                            if ((TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_COMPLETED) && (TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_ERROR))
                            {
                                TestCases.oTestCases[TestCaseNumber - 1].Status = Constants.VERIFICATION_STATUS_SKIPPED;
                                lblStatus7.Text = Constants.VERIFICATION_STATUS_SKIPPED; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            }
                        }

                        this.Refresh();
                        #endregion

                        #region - TC8
                        TestCaseNumber = Constants.TESTCASE_NUM8_HDX_READY_MULTI_MONITOR_SUPPORT;
                        if (chkTestCase8.Checked)
                        {
                            Globals.isPremiumTestcasesValidatedFirstTime = false;
                            lblStatus8.Text = Constants.VERIFICATION_STATUS_IN_PROGRESS; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            returnedStatus = TC8_HDXMultiMonitor(TestCaseNumber);
                            lblStatus8.Text = returnedStatus; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);
                        }
                        else
                        {
                            if ((TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_COMPLETED) && (TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_ERROR))
                            {

                                TestCases.oTestCases[TestCaseNumber - 1].Status = Constants.VERIFICATION_STATUS_SKIPPED;
                                lblStatus8.Text = Constants.VERIFICATION_STATUS_SKIPPED; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            }
                        }

                        this.Refresh();
                        #endregion
                       

                        // new test case
                        


                    }
                    else
                    {
                        msgUser = "HDX Standard Preqrequiste Check has not been completed!";
                        MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        BackButton();
                    }
                }
                else
                {
                    msgUser = "Please select at least one testcase to proceed further";
                    MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            #endregion-HDXREADY

            #region - HDX3D
            if (tabTestCases.SelectedTab == tabHDX3DPro)
            {
                if (chkTestCase9.Checked || chkTestCase10.Checked || chkTestCase11.Checked || chkTestCase12.Checked || chkTestCase13.Checked || chkTestCase14.Checked || chkTestCase15.Checked)
                {
                    Constants.policyname = "HDX_3D_Pro";
                    Constants.templatename = "HDX 3D Pro";
                    tabTestCases.SelectedTab = tabHDX3DPro;
                    this.Refresh();
                    if (Globals.IsSystemRequirementsCheckCompletedForHDXReady)
                    {

                        #region -TC9
                        TestCaseNumber = Constants.TESTCASE_NUM9_HDX_READY_3D_MOUSE;
                        if (chkTestCase9.Checked)
                        {
                            lblStatus9.Text = Constants.VERIFICATION_STATUS_IN_PROGRESS; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            returnedStatus = TC9_3DMouseSupport(TestCaseNumber);
                            lblStatus9.Text = returnedStatus; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);
                        }
                        else
                        {
                            if ((TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_COMPLETED) && (TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_ERROR))
                            {
                                TestCases.oTestCases[TestCaseNumber - 1].Status = Constants.VERIFICATION_STATUS_SKIPPED;
                                lblStatus9.Text = Constants.VERIFICATION_STATUS_SKIPPED; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            }
                        }
                        this.Refresh();
                        #endregion

                        #region -TC10
                        TestCaseNumber = Constants.TESTCASE_NUM10_HDX_READY_MULTI_MONITOR_SUPPORT_3D_APPS;
                        if (chkTestCase10.Checked)
                        {
                            lblStatus10.Text = Constants.VERIFICATION_STATUS_IN_PROGRESS; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            returnedStatus = TC10_HDXMultiMonitorSupport(TestCaseNumber);
                            lblStatus10.Text = returnedStatus; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);
                        }
                        else
                        {
                            if ((TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_COMPLETED) && (TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_ERROR))
                            {
                                TestCases.oTestCases[TestCaseNumber - 1].Status = Constants.VERIFICATION_STATUS_SKIPPED;
                                lblStatus10.Text = Constants.VERIFICATION_STATUS_SKIPPED; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            }
                        }

                        this.Refresh();
                        #endregion

                        #region -TC11
                        TestCaseNumber = Constants.TESTCASE_NUM11_HDX_READY_3DPRO_RICH_GRAPHICS;
                        if (chkTestCase11.Checked)
                        {
                            lblStatus11.Text = Constants.VERIFICATION_STATUS_IN_PROGRESS; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            returnedStatus = TC11_HDX3DProRichGraphicsAppsSupport(TestCaseNumber);
                            lblStatus11.Text = returnedStatus; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);
                        }
                        else
                        {
                            if ((TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_COMPLETED) && (TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_ERROR))
                            {
                                TestCases.oTestCases[TestCaseNumber - 1].Status = Constants.VERIFICATION_STATUS_SKIPPED;
                                lblStatus11.Text = Constants.VERIFICATION_STATUS_SKIPPED; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            }
                        }

                        this.Refresh();
                        #endregion

                        #region -TC12
                        TestCaseNumber = Constants.TESTCASE_NUM12_HDX_READY_PIXEL_PERFECT_LOSSLESS_COMPRESSION;
                        if (chkTestCase12.Checked)
                        {
                            lblStatus12.Text = Constants.VERIFICATION_STATUS_IN_PROGRESS; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            returnedStatus = TC12_PixelPerfectLosslessCompression(TestCaseNumber);
                            lblStatus12.Text = returnedStatus; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);
                        }
                        else
                        {
                            if ((TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_COMPLETED) && (TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_ERROR))
                            {
                                TestCases.oTestCases[TestCaseNumber - 1].Status = Constants.VERIFICATION_STATUS_SKIPPED;
                                lblStatus12.Text = Constants.VERIFICATION_STATUS_SKIPPED; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            }
                        }

                        this.Refresh();
                        #endregion

                        #region -TC13
                        TestCaseNumber = Constants.TESTCASE_NUM13_HDX_READY_P2P_VIDEO_CONFERENCE;
                        if (chkTestCase13.Checked)
                        {

                            msgUser = "Select the location where you'd like your media file(s) to be saved.";
                            DialogResult msgResult = MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);

                            if (msgResult == DialogResult.OK)
                            {
                                var result = browseFolder.ShowDialog();
                                if (result == DialogResult.OK)
                                {
                                    flag = true;
                                    Constants.FolderToSaveVideo = browseFolder.SelectedPath;
                                }
                                else
                                {
                                    msgUser = "Please select the folder to save the recording.";
                                    MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    btnValidate.Enabled = true;
                                    btnExit.Enabled = true;
                                    return;
                                }
                            }


                            Globals.isPremiumTestcasesValidatedFirstTime = false;
                            lblStatus13.Text = Constants.VERIFICATION_STATUS_IN_PROGRESS; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            returnedStatus = TC13_HDXP2PVideoConferencingOnSkypeForBusiness(TestCaseNumber);
                            lblStatus13.Text = returnedStatus;
                            UpdateStatusColor(TestCaseNumber); this.Refresh();

                            Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);

                        }
                        else
                        {
                            if ((TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_COMPLETED) && (TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_ERROR))
                            {
                                TestCases.oTestCases[TestCaseNumber - 1].Status = Constants.VERIFICATION_STATUS_SKIPPED;
                                lblStatus13.Text = Constants.VERIFICATION_STATUS_SKIPPED; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            }
                        }
                        this.Refresh();
                        #endregion

                        #region -TC14
                        TestCaseNumber = Constants.TESTCASE_NUM14_HDX_READY_SD_WAN_HDX_OPTIMIZATION;
                        if (chkTestCase14.Checked)
                        {
                            lblStatus14.Text = Constants.VERIFICATION_STATUS_IN_PROGRESS; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            returnedStatus = TC14_CloudBridge(TestCaseNumber);
                            lblStatus14.Text = returnedStatus; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);
                        }
                        else
                        {
                            if ((TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_COMPLETED) && (TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_ERROR))
                            {

                                TestCases.oTestCases[TestCaseNumber - 1].Status = Constants.VERIFICATION_STATUS_SKIPPED;
                                lblStatus14.Text = Constants.VERIFICATION_STATUS_SKIPPED; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            }
                        }

                        this.Refresh();



                        #endregion

                        #region -TC15
                        TestCaseNumber = Constants.TESTCASE_NUM15_HDX_READY_CITRIX_GATEWAY_ADM;
                        if (chkTestCase15.Checked)
                        {
                            lblStatus15.Text = Constants.VERIFICATION_STATUS_IN_PROGRESS; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            returnedStatus = TC15_NetScaler(TestCaseNumber);
                            lblStatus15.Text = returnedStatus; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);
                        }
                        else
                        {
                            if ((TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_COMPLETED) && (TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_ERROR))
                            {

                                TestCases.oTestCases[TestCaseNumber - 1].Status = Constants.VERIFICATION_STATUS_SKIPPED;
                                lblStatus15.Text = Constants.VERIFICATION_STATUS_SKIPPED; UpdateStatusColor(TestCaseNumber); this.Refresh();
                            }
                        }

                        this.Refresh();
                        #endregion

                       

                    }
                    else
                    {
                        msgUser = "HDX Standard Preqrequiste Check has not been completed!";
                        MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        BackButton();
                    }
                }
                else
                {
                    msgUser = "Please select at least one testcase to proceed further";
                    MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
            #endregion - HDX3DPRO


            #region - HDXPREMIUM
            if (tabTestCases.SelectedTab == tabHDXPremium)
            {
                if (chkTestCase16.Checked || chkTestCase17.Checked || chkTestCase18.Checked || chkTestCase19.Checked || chkTestCase20.Checked || chkTestCase21.Checked || chkTestCase22.Checked || chkTestCase23.Checked)
                {
                    // "2. Disable the previous policy template and enable HDX Premium Policies in Citrix Studio" + "\n" 
                    if (Globals.isPremiumTestcasesValidatedFirstTime == true )
                    {
                       
                        if(Globals.Selected_Setup_Choice == Convert.ToByte(Globals.Setup_Choice.ON_Prim))
                        {
                            msgUser = "1. Please save the previous test results and then logoff from VDA." + "\n" +
                            "2. Then log back in to the VDA for the HDX Premium policies to take effect";
                            MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                        }
                        string[] processList = new string[] { "notepad", "vlc", "powerpnt", "Winword", "wmplayer", "iexplore" };
                        if (Utilities.CheckOpenedApplications(processList))
                        {
                            foreach (string processName in processList)
                            {
                                this.Refresh();
                                Utilities.CloseAllOpenedApplications(processName);
                            }
                        }
                        Globals.isPremiumTestcasesValidatedFirstTime = false;
                    }
                    
                        tabTestCases.SelectedTab = tabHDXPremium;
                        this.Refresh();

                        if (Globals.IsSystemRequirementsCheckCompletedForHDXPremium)
                        {
                            #region -TC16
                            TestCaseNumber = Constants.TESTCASE_NUM16_HDX_PREMIUM_H265_DECODING_GRAPHICS;
                            if (chkTestCase16.Checked)
                            {
                                Globals.isPremiumTestcasesValidatedFirstTime = false;
                                lblStatus16.Text = Constants.VERIFICATION_STATUS_IN_PROGRESS; UpdateStatusColor(TestCaseNumber); this.Refresh();
                                returnedStatus = TC16_H265DecodingForGraphics(TestCaseNumber);
                                lblStatus16.Text = returnedStatus; UpdateStatusColor(TestCaseNumber); this.Refresh();
                                Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);
                            }
                            else
                            {
                                if ((TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_COMPLETED) && (TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_ERROR))
                                {
                                    TestCases.oTestCases[TestCaseNumber - 1].Status = Constants.VERIFICATION_STATUS_SKIPPED;
                                    lblStatus16.Text = Constants.VERIFICATION_STATUS_SKIPPED; UpdateStatusColor(TestCaseNumber); this.Refresh();
                                }
                            }

                            this.Refresh();
                            #endregion

                            #region -TC17
                            TestCaseNumber = Constants.TESTCASE_NUM17_HDX_PREMIUM_REALTIME_AUDIO_VOIP;
                            if (chkTestCase17.Checked)
                            {
                                msgUser = "Select the location where you'd like your media file(s) to be saved.";
                                DialogResult msgResult = MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);

                                if (msgResult == DialogResult.OK)
                                {
                                    var result = browseFolder.ShowDialog();
                                    if (result == DialogResult.OK)
                                    {
                                        flag = true;
                                        Constants.FolderToSaveVideo = browseFolder.SelectedPath;
                                    }
                                    else
                                    {
                                        msgUser = "Please select the folder to save the recording.";
                                        MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        btnValidate.Enabled = true;
                                        btnExit.Enabled = true;
                                        return;
                                    }
                                }

                                Globals.isPremiumTestcasesValidatedFirstTime = false;
                                lblStatus17.Text = Constants.VERIFICATION_STATUS_IN_PROGRESS; UpdateStatusColor(TestCaseNumber); this.Refresh();
                                returnedStatus = TC17_HDXRealTimeAudioVoIP(TestCaseNumber);
                                lblStatus17.Text = returnedStatus; UpdateStatusColor(TestCaseNumber); this.Refresh();
                                Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);
                            }
                            else
                            {
                                if ((TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_COMPLETED) && (TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_ERROR))
                                {

                                    TestCases.oTestCases[TestCaseNumber - 1].Status = Constants.VERIFICATION_STATUS_SKIPPED;
                                    lblStatus17.Text = Constants.VERIFICATION_STATUS_SKIPPED; UpdateStatusColor(TestCaseNumber); this.Refresh();
                                }
                            }

                            this.Refresh();

                            #endregion

                            #region -TC18
                            TestCaseNumber = Constants.TESTCASE_NUM18_HDX_PREMIUM_MEDIA_STREAM_FLASH_REDIRECTION;
                            if (chkTestCase18.Checked)
                            {
                                Globals.isPremiumTestcasesValidatedFirstTime = false;
                                lblStatus18.Text = Constants.VERIFICATION_STATUS_IN_PROGRESS; UpdateStatusColor(TestCaseNumber); this.Refresh();
                                returnedStatus = TC18_HDXMediaStreamFlashRedirection(TestCaseNumber);
                                lblStatus18.Text = returnedStatus; UpdateStatusColor(TestCaseNumber); this.Refresh();
                                Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);
                            }
                            else
                            {
                                if ((TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_COMPLETED) && (TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_ERROR))
                                {
                                    TestCases.oTestCases[TestCaseNumber - 1].Status = Constants.VERIFICATION_STATUS_SKIPPED;
                                    lblStatus18.Text = Constants.VERIFICATION_STATUS_SKIPPED; UpdateStatusColor(TestCaseNumber); this.Refresh();
                                }
                            }

                            this.Refresh();
                            #endregion

                            #region -TC19
                            TestCaseNumber = Constants.TESTCASE_NUM19_HDX_PREMIUM_HTML5_VIDEO_REDIRECTION;
                            if (chkTestCase19.Checked)
                            {
                                Globals.isPremiumTestcasesValidatedFirstTime = false;
                                lblStatus19.Text = Constants.VERIFICATION_STATUS_IN_PROGRESS; UpdateStatusColor(TestCaseNumber); this.Refresh();
                                returnedStatus = TC19_HTML5VideoRedirection(TestCaseNumber);
                                lblStatus19.Text = returnedStatus; UpdateStatusColor(TestCaseNumber); this.Refresh();
                                Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);
                            }
                            else
                            {
                                if ((TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_COMPLETED) && (TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_ERROR))
                                {
                                    TestCases.oTestCases[TestCaseNumber - 1].Status = Constants.VERIFICATION_STATUS_SKIPPED;
                                    lblStatus19.Text = Constants.VERIFICATION_STATUS_SKIPPED; UpdateStatusColor(TestCaseNumber); this.Refresh();
                                }
                            }

                            this.Refresh();
                            #endregion

                            #region -TC20
                            TestCaseNumber = Constants.TESTCASE_NUM20_HDX_PREMIUM_MEDIA_STREAM_WINDOWS_REDIRECTION;
                            if (chkTestCase20.Checked)
                            {
                                Globals.isPremiumTestcasesValidatedFirstTime = false;
                                lblStatus20.Text = Constants.VERIFICATION_STATUS_IN_PROGRESS; UpdateStatusColor(TestCaseNumber); this.Refresh();
                                returnedStatus = TC20_HDXMediaStreamWindowsMediaRedirection(TestCaseNumber);
                                lblStatus20.Text = returnedStatus; UpdateStatusColor(TestCaseNumber); this.Refresh();
                                Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);
                            }
                            else
                            {
                                if ((TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_COMPLETED) && (TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_ERROR))
                                {
                                    TestCases.oTestCases[TestCaseNumber - 1].Status = Constants.VERIFICATION_STATUS_SKIPPED;
                                    lblStatus20.Text = Constants.VERIFICATION_STATUS_SKIPPED; UpdateStatusColor(TestCaseNumber); this.Refresh();
                                }
                            }

                            this.Refresh();
                            #endregion

                            #region -TC21
                            TestCaseNumber = Constants.TESTCASE_NUM21_HDX_PREMIUM_REALTIME_WEBCAM_COMPRESSION;
                            if (chkTestCase21.Checked)
                            {
                                Globals.isPremiumTestcasesValidatedFirstTime = false;
                                lblStatus21.Text = Constants.VERIFICATION_STATUS_IN_PROGRESS; UpdateStatusColor(TestCaseNumber); this.Refresh();
                                returnedStatus = TC21_HDXRealTimeWebcamCompression(TestCaseNumber);
                                lblStatus21.Text = returnedStatus; UpdateStatusColor(TestCaseNumber); this.Refresh();
                                Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);
                            }
                            else
                            {
                                if ((TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_COMPLETED) && (TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_ERROR))
                                {
                                    TestCases.oTestCases[TestCaseNumber - 1].Status = Constants.VERIFICATION_STATUS_SKIPPED;
                                    lblStatus21.Text = Constants.VERIFICATION_STATUS_SKIPPED; UpdateStatusColor(TestCaseNumber); this.Refresh();
                                }
                            }

                            this.Refresh();
                            #endregion

                            #region -TC22
                            TestCaseNumber = Constants.TESTCASE_NUM22_HDX_PREMIUM_BROWSER_CONTENT_REDIRECTION;
                            if (chkTestCase22.Checked)
                            {
                                Globals.isPremiumTestcasesValidatedFirstTime = false;
                                lblStatus22.Text = Constants.VERIFICATION_STATUS_IN_PROGRESS; UpdateStatusColor(TestCaseNumber); this.Refresh();
                                returnedStatus = TC22_BrowserContentRedirection(TestCaseNumber);
                                lblStatus22.Text = returnedStatus; UpdateStatusColor(TestCaseNumber); this.Refresh();
                                Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);
                            }
                            else
                            {
                                if ((TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_COMPLETED) && (TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_ERROR))
                                {
                                    TestCases.oTestCases[TestCaseNumber - 1].Status = Constants.VERIFICATION_STATUS_SKIPPED;
                                    lblStatus22.Text = Constants.VERIFICATION_STATUS_SKIPPED; UpdateStatusColor(TestCaseNumber); this.Refresh();
                                }
                            }

                            this.Refresh();
                            #endregion

                            #region -TC23

                            TestCaseNumber = Constants.TESTCASE_NUM23_HDX_PREMIUM_PIV_SMARTCARD_ONPRIMISES_SUPPORT;
                            if (chkTestCase23.Checked)
                            {
                                Globals.isPremiumTestcasesValidatedFirstTime = false;
                                lblStatus23.Text = Constants.VERIFICATION_STATUS_IN_PROGRESS; UpdateStatusColor(TestCaseNumber); this.Refresh();
                                returnedStatus = TC23_HDXPIVSmartcardSupport(TestCaseNumber);
                                lblStatus23.Text = returnedStatus; UpdateStatusColor(TestCaseNumber); this.Refresh();
                                Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);
                            }
                            else
                            {
                                if ((TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_COMPLETED) && (TestCases.oTestCases[TestCaseNumber - 1].Status != Constants.VERIFICATION_STATUS_ERROR))
                                {
                                    TestCases.oTestCases[TestCaseNumber - 1].Status = Constants.VERIFICATION_STATUS_SKIPPED;
                                    lblStatus23.Text = Constants.VERIFICATION_STATUS_SKIPPED; UpdateStatusColor(TestCaseNumber); this.Refresh();
                                }
                            }
                            this.Refresh();
                            #endregion
                                                      

                        }
                        else
                        {
                            msgUser = "HDX Premium Preqrequiste Check has not been completed!";
                            MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            BackButton();
                        }
                    
                }
                else
                {
                    msgUser = "Please select at least one testcase to proceed further";
                    MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
            #endregion - HDXPREMIUM

           

            btnValidate.Enabled = true;
            btnExit.Enabled = true;
        }
        #region
        private string HDXP2PVideoConferencingOnSkypeForBusiness(byte testCaseNumber)
        {
            string RTOPTestcaseStatus = "";
            StringBuilder sbResult = new StringBuilder("");
            txtResult.AppendText("\n");
            txtResult.AppendText("Running " + Constants.TESTCASE07_NAME + " Test Case - " + DateTime.Now.ToString() + "\n");
            this.Enabled = false;
            if (Constants.isLynPresent)
            {
                if (Constants.IsEncoder == true)
                {
                    Globals.RTOP = "RTOP";
                    ProcessStartInfo psStartInfo = new ProcessStartInfo();
                    psStartInfo.FileName = @"" + "\"" + Constants.SOFTWARE_NAME_LYNC + "\"";
                    Process ps = Process.Start(psStartInfo);
                    Thread.Sleep(Constants.DUMMY_RUNTIME * 2);
                    //SendKeys.Send("%");
                    //SendKeys.Send("E");
                    //SendKeys.Send("F");
                    //SendKeys.Send("R");
                    //SendKeys.Send("{TAB}"); SendKeys.Send("{TAB}"); SendKeys.Send("{TAB}"); SendKeys.Send("{TAB}");
                    //SendKeys.Send("0");
                    //SendKeys.Send("~");
                    frmQsnBanner = new frmQuestionBanner(this, 7, ps);
                    frmQsnBanner.ShowDialog();
                    try
                    {
                        if (Globals.IsVideoRecorded == true)
                        {
                            UploadLoadingIcon icon = new UploadLoadingIcon();
                            icon.ShowDialog();
                        }
                    }
                    catch (Exception ex)
                    {
                        string msgUser = "Unable to establish a connection with Citrix Ready ShareFile server." + "\n" + "\n" +
                                       "This could be caused by a loss of network connectivity or inadequate permission to connect to the Citrix Ready ShareFile." + "\n" + "\n" +
                                       "If the problem persists, please reach out to us at CitrixReady@citrix.com";
                        MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    if (Globals.isRTOPCompleted1 == true)
                    {

                        sbResult.AppendLine("HDX RTOP with Skype For Business completed");
                        txtResult.AppendText("HDX P2P Video Conferencing on Skype for Business With HDX RealTime Optimization Pack completed" + "\n");
                        TestCases.oTestCases[testCaseNumber - 1].Status = Constants.VERIFICATION_STATUS_COMPLETED;
                        TestCases.oTestCases[testCaseNumber - 1].Result = sbResult.ToString();
                        TestCases.oTestCases[testCaseNumber - 1].DownloadLink = Constants.VideoDownloadLink;
                        txtResult.AppendText(Constants.VideoDownloadLink);

                        StreamWriter fileLogHDXRTOP = new StreamWriter(Constants.LOG_FILE_NAME, true);
                        fileLogHDXRTOP.WriteLine("URL - " + Constants.VideoDownloadLink);
                        fileLogHDXRTOP.Close();

                        this.Enabled = true;
                        this.Refresh();
                        RTOPTestcaseStatus = Constants.VERIFICATION_STATUS_COMPLETED;

                    }
                    else
                    {
                        sbResult.AppendLine("HDX RTOP with Skype For Business closed  abruptly.");
                        txtResult.AppendText("HDX RTOP with Skype For Business closed  abruptly. \n");
                        this.Enabled = true;
                        this.Refresh();
                        TestCases.oTestCases[testCaseNumber - 1].Status = Constants.VERIFICATION_STATUS_ERROR;
                        TestCases.oTestCases[testCaseNumber - 1].Result = sbResult.ToString();
                        RTOPTestcaseStatus = Constants.VERIFICATION_STATUS_ERROR;
                    }

                }
                else
                {
                    MessageBox.Show("This testcase cannot be executed because all the required software(s) are not installed. Please install all the mandatory software(s) mentioned in the prerequisite.", Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    sbResult.AppendLine("ERROR: Required software(s) are not installed on this VDA");
                    txtResult.AppendText("ERROR: Required software(s) are not installed on this VDA" + "\n");
                    this.Enabled = true;
                    this.Refresh();
                    RTOPTestcaseStatus = Constants.VERIFICATION_STATUS_ERROR;
                }
                return RTOPTestcaseStatus;

            }
            else
            {
                TestCases.oTestCases[testCaseNumber - 1].Status = Constants.VERIFICATION_STATUS_ERROR;
                TestCases.oTestCases[testCaseNumber - 1].Result = sbResult.ToString();
                sbResult.AppendLine("HDX P2P Video Conferencing on Skype for Business With HDX RealTime Optimization Pack Error: Skype for Business not available");
                txtResult.AppendText("HDX P2P Video Conferencing on Skype for Business With HDX RealTime Optimization Pack Error: Skype for Business not available" + "\n");
                this.Enabled = true;
                this.Refresh();
                return Constants.VERIFICATION_STATUS_ERROR;
            }
        }
        #endregion

        #endregion

        #region - Custom Functions

        public void InitForm()
        {
            //frmVerification.ActiveForm.Height = Constants.Form_Height;
            //frmVerification.ActiveForm.Width = Constants.Form_Width;

            //Fix for the Help Icon alignment
            //As the design time position is not maintained correctly, 
            //the below code will fix the position

            byte offset = 0;
            for (byte iCounter = 1; iCounter <= 18; iCounter++)
            {
                //TODO: To minimize the below code, try Object By Reference 
            }
            picboxTestCase1.Left = (lblTestCase1.Left + lblTestCase1.Width) + offset;
            picboxTestCase2.Left = (lblTestCase2.Left + lblTestCase2.Width) + offset;
            picboxTestCase3.Left = (lblTestCase3.Left + lblTestCase3.Width) + offset;
            picboxTestCase4.Left = (lblTestCase4.Left + lblTestCase4.Width) + offset;
            picboxTestCase5.Left = (lblTestCase5.Left + lblTestCase5.Width) + offset;
            picboxTestCase6.Left = (lblTestCase6.Left + lblTestCase6.Width) + offset;
            picboxTestCase8.Left = (lblTestCase8.Left + lblTestCase8.Width) + offset;
            picboxTestCase16.Left = (lblTestCase16.Left + lblTestCase16.Width) + offset;
            picboxTestCase17.Left = (lblTestCase17.Left + lblTestCase17.Width) + offset;
            picboxTestCase18.Left = (lblTestCase18.Left + lblTestCase18.Width) + offset;
            picboxTestCase19.Left = (lblTestCase19.Left + lblTestCase19.Width) + offset;
            picboxTestCase20.Left = (lblTestCase20.Left + lblTestCase20.Width) + offset;
            picboxTestCase21.Left = (lblTestCase21.Left + lblTestCase21.Width) + offset;
            picboxTestCase22.Left = (lblTestCase22.Left + lblTestCase22.Width) + offset;
            picboxTestCase9.Left = (lblTestCase9.Left + lblTestCase9.Width) + offset;
            picboxTestCase10.Left = (lblTestCase10.Left + lblTestCase10.Width) + offset;
            picboxTestCase11.Left = (lblTestCase11.Left + lblTestCase11.Width) + offset;
            picboxTestCase12.Left = (lblTestCase12.Left + lblTestCase12.Width) + offset;
        }

        public void WorkInProgress()
        {
            MessageBox.Show("This feature is under construction.", Constants.MSGBOX_CAPTION,
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        #region Back Button Functionality

        public void BackButton()
        {
            this.Hide();

            if (tabTestCases.SelectedTab == tabHDXReady)
            {
                Globals.Selected_HDX_Category = Convert.ToByte(Globals.HDX_Category.HDX_Ready);
            }
            if (tabTestCases.SelectedTab == tabHDXPremium)
            {
                Globals.Selected_HDX_Category = Convert.ToByte(Globals.HDX_Category.HDX_Premium);
            }
            if (tabTestCases.SelectedTab == tabHDX3DPro)
            {
                Globals.Selected_HDX_Category = Convert.ToByte(Globals.HDX_Category.HDX_Ready);
            }

            Globals.VerificationScreenResult.Clear();
            for (int iCounter = 0; iCounter < txtResult.Lines.Count(); iCounter++)
            {
                Globals.VerificationScreenResult.Append(txtResult.Lines[iCounter]);
                Globals.VerificationScreenResult.AppendLine();
            }

            var frmPreReq = new frmPrerequisites();
            frmPreReq.Show();

        }

        #endregion

        #region Load Tool Tips

        public void LoadToolTips()
        {
            string msgToolTip = "";

            #region - Tooltips for HDX Ready

            msgToolTip = @"This test case will check the performance of your endpoint at 1080p display" +
                Environment.NewLine + " resolution, when the load is rendered on a virtual desktop." +
                Environment.NewLine +
                Environment.NewLine + "The following tests will be run for this scenario:" +
                Environment.NewLine + "1) PowerPoint Presentation" +
                Environment.NewLine + "2) Command Window Movement" +
                Environment.NewLine + "3) PDF File Scrolling" +
                Environment.NewLine + "4) Movie playing using VLC Player";
            ttHelp.SetToolTip(pnlTestCase1, msgToolTip);
            ttHelp.SetToolTip(lblTestCase1, msgToolTip);
            ttHelp.SetToolTip(picboxTestCase1, msgToolTip);

            msgToolTip = @"Secure Web and SaaS Apps ";
            ttHelp.SetToolTip(pnlTestCase2, msgToolTip);
            ttHelp.SetToolTip(lblTestCase2, msgToolTip);
            ttHelp.SetToolTip(picboxTestCase2, msgToolTip);

            msgToolTip = @"This test case will verify your endpoint’s support for audio recording" +
                Environment.NewLine + " from a virtual desktop";
            ttHelp.SetToolTip(pnlTestCase3, msgToolTip);
            ttHelp.SetToolTip(lblTestCase3, msgToolTip);
            ttHelp.SetToolTip(picboxTestCase3, msgToolTip);

            msgToolTip = @"This test case will verify that your endpoint supports server rendered flash video " +
               Environment.NewLine + " with high definition (720p) playback over LAN, on the virtual desktop.";
            ttHelp.SetToolTip(pnlTestCase4, msgToolTip);
            ttHelp.SetToolTip(lblTestCase4, msgToolTip);
            ttHelp.SetToolTip(picboxTestCase4, msgToolTip);



            msgToolTip = @"This test case will verify that your endpoint supports server rendered Windows Media video " +
                Environment.NewLine + "with high definition (720p) playback over LAN, on the virtual desktop.";
            ttHelp.SetToolTip(pnlTestCase5, msgToolTip);
            ttHelp.SetToolTip(lblTestCase5, msgToolTip);
            ttHelp.SetToolTip(picboxTestCase5, msgToolTip);

            msgToolTip = @"This test will verify that the printers connected directly to the endpoint device are created as " +
                Environment.NewLine + "valid client printers in a virtual desktop session, when using the Universal Print Driver (UPD).";
            ttHelp.SetToolTip(pnlTestCase6, msgToolTip);
            ttHelp.SetToolTip(lblTestCase6, msgToolTip);
            ttHelp.SetToolTip(picboxTestCase6, msgToolTip);

            msgToolTip = @"This test case will help establish that your endpoint redirects for USB 2.0 and " +
                Environment.NewLine + @"3.0 devices via generic USB redirection, when connected with any USB flash drive.";
            ttHelp.SetToolTip(pnlTestCase7, msgToolTip);
            ttHelp.SetToolTip(lblTestCase7, msgToolTip);
            ttHelp.SetToolTip(picboxTestCase7, msgToolTip);


            msgToolTip = @"This test case will verify that the endpoint supports at least two monitors with 1080p (1920x1080) resolution. " +
                Environment.NewLine + "This test case will also establish if a virtual desktop can span across monitors in rectangular and " +
                Environment.NewLine + "non-rectangular formations and with varied screen resolutions.";
            ttHelp.SetToolTip(pnlTestCase8, msgToolTip);
            ttHelp.SetToolTip(lblTestCase8, msgToolTip);
            ttHelp.SetToolTip(picboxTestCase8, msgToolTip);

           

            #endregion

            #region - Tooltips for HDX Ready Contd.

            msgToolTip = @"This test case will verify that your endpoint supports functionality and interactivity of 3D applications using " +
                Environment.NewLine + "a Space Mouse. This test validates 3D mice support using Generic USB redirection.";
            ttHelp.SetToolTip(pnlTestCase9, msgToolTip);
            ttHelp.SetToolTip(lblTestCase9, msgToolTip);
            ttHelp.SetToolTip(picboxTestCase9, msgToolTip);

            msgToolTip = @"This test case will verify that your endpoint is able to support at least two 1080p HD resolution monitors running " +
                Environment.NewLine + "multiple 3D applications. In addition, this test evaluates the functionality and performance of using " +
                Environment.NewLine + "3D applications on an endpoint in multi-monitor mode.";
            ttHelp.SetToolTip(pnlTestCase10, msgToolTip);
            ttHelp.SetToolTip(lblTestCase10, msgToolTip);
            ttHelp.SetToolTip(picboxTestCase10, msgToolTip);

            msgToolTip = @"This test case will verify that your endpoint supports Citrix HDX Standard 3D  technologies and runs " +
                Environment.NewLine + "non-interactive and interactive rich graphical app loads with native-like user experience and performance. " +
                Environment.NewLine + "This test measures FPS, CPU consumption, and ICA session validation.";
            ttHelp.SetToolTip(pnlTestCase11, msgToolTip);
            ttHelp.SetToolTip(lblTestCase11, msgToolTip);
            ttHelp.SetToolTip(picboxTestCase11, msgToolTip);

            msgToolTip = @"This test case will verify that your endpoint supports the Lossless mode i.e. no compression of the ICA stream occurs, " +
                Environment.NewLine + "allowing for pixel-perfect images to be delivered. This option is available for delivering medical " +
                Environment.NewLine + "imaging software that cannot have degraded image quality. ";
            ttHelp.SetToolTip(pnlTestCase12, msgToolTip);
            ttHelp.SetToolTip(lblTestCase12, msgToolTip);
            ttHelp.SetToolTip(picboxTestCase12, msgToolTip);

            msgToolTip = @"This test case will verify that your endpoint can support peer to peer (P2P) video conferencing using Microsoft Skype" +
             Environment.NewLine + "for Business (MS SfB) and HDX RealTime Optimization Pack (RTOP).";
            ttHelp.SetToolTip(pnlTestCase13, msgToolTip);
            ttHelp.SetToolTip(lblTestCase13, msgToolTip);
            ttHelp.SetToolTip(picboxTestCase13, msgToolTip);

            msgToolTip = @"This test case will verify that your endpoint supports accelerated traffic attained by Citrix SD-WAN.";
            ttHelp.SetToolTip(pnlTestCase14, msgToolTip);
            ttHelp.SetToolTip(lblTestCase14, msgToolTip);
            ttHelp.SetToolTip(picboxTestCase14, msgToolTip);

            msgToolTip = @"This test case allows you to demonstrate interoperability of Thin-clients with Citrix Gateway" +
                Environment.NewLine + "and be able to capture metrics on Citrix ADM.";
            ttHelp.SetToolTip(pnlTestCase15, msgToolTip);
            ttHelp.SetToolTip(lblTestCase15, msgToolTip);
            ttHelp.SetToolTip(picboxTestCase15, msgToolTip);

            #endregion

            #region - Tooltips for HDX Premium


            msgToolTip = @"H265 decoding for graphics";
               ttHelp.SetToolTip(pnlTestCase16, msgToolTip);
            ttHelp.SetToolTip(lblTestCase16, msgToolTip);
            ttHelp.SetToolTip(picboxTestCase16, msgToolTip);

            msgToolTip = @"This test case will verify that your endpoint supports HDX RealTime Audio (VoIP)." +
                Environment.NewLine + "Enterprise VoIP readiness in a Citrix environment includes the following three elements:" +
                Environment.NewLine + "1) Latency" +
                Environment.NewLine + "2) Audio quality" +
                Environment.NewLine + "3) Support for audio mixing";
            ttHelp.SetToolTip(pnlTestCase17, msgToolTip);
            ttHelp.SetToolTip(lblTestCase17, msgToolTip);
            ttHelp.SetToolTip(picboxTestCase17, msgToolTip);

            msgToolTip = @"This test case will verify the local rendering of Adobe Flash to the extent that these formats are" +
                Environment.NewLine + "supported via HDX Media Stream Flash Redirection. " +
                Environment.NewLine + "Perform these tests at 720p resolution.";
            ttHelp.SetToolTip(pnlTestCase18, msgToolTip);
            ttHelp.SetToolTip(lblTestCase18, msgToolTip);
            ttHelp.SetToolTip(picboxTestCase18, msgToolTip);


            msgToolTip = @"This test case will verify the local rendering of HTML5 Media video formats via HTML5 Video Redirection." +
                Environment.NewLine + "Perform these tests at 720p resolution.";
            ttHelp.SetToolTip(pnlTestCase19, msgToolTip);
            ttHelp.SetToolTip(lblTestCase19, msgToolTip);
            ttHelp.SetToolTip(picboxTestCase19, msgToolTip);

            msgToolTip = @"This test case will verify that your endpoint supports local rendering of Windows Media video formats via " +
                Environment.NewLine + "HDX Media Stream Multimedia Redirection (i.e. Windows Media Redirection).";
            ttHelp.SetToolTip(pnlTestCase20, msgToolTip);
            ttHelp.SetToolTip(lblTestCase20, msgToolTip);
            ttHelp.SetToolTip(picboxTestCase20, msgToolTip);

      

            msgToolTip = @"This test case will verify that your endpoint supports HDX RealTime Webcam Compression. " +
               Environment.NewLine + "This allows the endpoint to capture video data, compress it and then send it to the " +
               Environment.NewLine + "XenDesktop session. Sending the compressed video stream to the virtual desktop reduces " +
               Environment.NewLine + "the bandwidth needed between the virtual desktop and the endpoint. In addition, it" +
               Environment.NewLine + "allows endpoint webcams to be used in a XenDesktop session even under WAN. However, " +
               Environment.NewLine + "the compression on the endpoint and decompression on the virtual desktop needs " +
               Environment.NewLine + "additional CPU resources. When a WebCam capable of performing hardware encoding is " +
               Environment.NewLine + "used, it reduces CPU usage on the endpoint.";
            ttHelp.SetToolTip(pnlTestCase21, msgToolTip);
            ttHelp.SetToolTip(lblTestCase21, msgToolTip);
            ttHelp.SetToolTip(picboxTestCase21, msgToolTip);

            msgToolTip = @"Browser Content Redirection";
            ttHelp.SetToolTip(pnlTestCase22, msgToolTip);
            ttHelp.SetToolTip(lblTestCase22, msgToolTip);
            ttHelp.SetToolTip(picboxTestCase22, msgToolTip);

            msgToolTip = @"This test case will verify that your endpoint functions with smartcards supporting the PIV standard for authentication.";
            ttHelp.SetToolTip(pnlTestCase23, msgToolTip);
            ttHelp.SetToolTip(lblTestCase23, msgToolTip);
            ttHelp.SetToolTip(picboxTestCase23, msgToolTip);

            #endregion

           

        }

        #endregion

        #region - Show Verification Check Status
        public void ShowVerificationCheckStatus()
        {
            foreach (var oTestCase in TestCases.oTestCases)
            {
                string strStatusLabelControlID = "lblStatus" + Convert.ToString(oTestCase.ID);

                Label lblStatusControl = this.Controls.Find(strStatusLabelControlID, true).FirstOrDefault() as Label;

                if (lblStatusControl != null)
                {
                    lblStatusControl.Text = oTestCase.Status.ToString();

                    lblStatusControl.ForeColor = System.Drawing.ColorTranslator.FromHtml(Constants.HTML_COLOR_NORMAL);

                    if (lblStatusControl.Text == Constants.VERIFICATION_STATUS_COMPLETED)
                    {
                        lblStatusControl.ForeColor = System.Drawing.ColorTranslator.FromHtml(Constants.HTML_COLOR_GREEN);
                    }
                    if (lblStatusControl.Text == Constants.VERIFICATION_STATUS_ERROR)
                    {
                        lblStatusControl.ForeColor = System.Drawing.ColorTranslator.FromHtml(Constants.HTML_COLOR_RED);
                    }

                }

            }
        }
        #endregion

        #region - Screen Capturing Event Handler
        private static void CaptureScreen(object sender, EventArgs e)
        {
            scrnCaptureTimer.Stop();

            int screenLeft = SystemInformation.VirtualScreen.Left;
            int screenTop = SystemInformation.VirtualScreen.Top;
            int screenWidth = SystemInformation.VirtualScreen.Width;
            int screenHeight = SystemInformation.VirtualScreen.Height;

            Bitmap bmpScreenshot = new Bitmap(screenWidth, screenHeight, PixelFormat.Format32bppArgb);
            var gfxScreenshot = Graphics.FromImage(bmpScreenshot);


            gfxScreenshot.CopyFromScreen(screenLeft, screenTop, 0, 0, bmpScreenshot.Size, CopyPixelOperation.SourceCopy);
            string currentDirectory = System.IO.Directory.GetCurrentDirectory();
            CreateFolderIfNotAvailable(currentDirectory + "\\Screenshots");

            Globals.screenshotName = System.IO.Directory.GetCurrentDirectory() + "\\Screenshots\\Screenshot_" + DateTime.Now.ToString("yyyyMMddTHHmmss") + ".jpeg";
            bmpScreenshot.Save(Globals.screenshotName, ImageFormat.Jpeg);
        }
        private static void CreateFolderIfNotAvailable(string path)
        {
            bool folderExists = Directory.Exists(path);
            if (!folderExists)
            {
                Directory.CreateDirectory(path);
            }
        }

        #endregion


        #endregion - Custom Functions

        #region - All Test Case Functions

        #region - HDX Ready Test Cases

        #region - Test Case # 1 - 2D Graphics
        public string TC1_2DGraphics(byte TestCaseID)
        {
            string returnValue = Constants.VERIFICATION_STATUS_ERROR;
            Boolean resultPPT = false; Boolean resultCmdPrompt = false; Boolean resultPDF = false;
            Boolean resultVLC = false;

            StringBuilder sbResult = new StringBuilder("");
            string sampleFileFullPath = "";

            txtResult.AppendText("\n");
            txtResult.AppendText("Running " + Constants.TESTCASE01_NAME + " Test Case - " + DateTime.Now.ToString() + "\n");

            #region - Check screen resolution

            int iHeight = 0; int iWidth = 0;
            ManagementObjectSearcher displays = new ManagementObjectSearcher("SELECT * FROM Win32_VideoController");
            foreach (ManagementObject mo in displays.Get())
            {
                if (mo["CurrentVerticalResolution"] != null) iHeight = Convert.ToInt32(mo["CurrentVerticalResolution"]);
                if (mo["CurrentHorizontalResolution"] != null) iWidth = Convert.ToInt32(mo["CurrentHorizontalResolution"]);
            }
            sbResult.AppendLine("Current Display Resolution: " + iWidth.ToString() + " x " + iHeight.ToString());
            txtResult.AppendText("Current Display Resolution: " + iWidth.ToString() + " x " + iHeight.ToString() + "\n");

            #endregion

            #region - PowerPoint Slide Deck
            StreamWriter fileFPSLogPowerPointStart = new StreamWriter(Constants.LOG_FILE_NAME, true);
            fileFPSLogPowerPointStart.WriteLine("START - PowerPoint Presentation - " + DateTime.Now.ToString());
            fileFPSLogPowerPointStart.Close();

            try
            {
                string fpsTaskDataPowerPoint = "";
                tokenSource = new CancellationTokenSource();
                token = tokenSource.Token;
                sampleFileFullPath = Globals.SampleFilesLocation;
                sampleFileFullPath = sampleFileFullPath + Constants.SAMPLE_FILE_NAME_POWERPOINT;
                if (!System.IO.File.Exists(sampleFileFullPath))
                {
                    sbResult.AppendLine("PowerPoint Sample File is missing: " + sampleFileFullPath);
                    txtResult.AppendText("PowerPoint Sample File is missing: " + sampleFileFullPath + "\n");
                }
                else
                {
                    if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes")
                    {
                        Task fpsTaskForPowerPoint = Task.Factory.StartNew(() =>
                      fpsTaskDataPowerPoint = Utilities.CalculateFPS(TestCaseID, "PowerPoint Presentation", Constants.FPS_INTERVAL, (Constants.FPS_CALCULATION_RUNTIME) / 1000, token), token);
                    }

                    ProcessStartInfo psStartInfo = new ProcessStartInfo();
                    psStartInfo.FileName = @"" + "\"" + Constants.APP_EXECUTABLE_POWERPOINT + "\"";
                    psStartInfo.Arguments = @"/S " + "\"" + sampleFileFullPath + "\"";
                    Process ps = Process.Start(psStartInfo);

                    bool flag = Utilities.ProcessMonitor(ps, Constants.SAMPLE_FILE_RUNTIME_POWERPOINT + 3000);

                    if (!ps.HasExited)
                    {
                        ps.Kill();
                    }

                    if (!flag)
                    {
                        tokenSource.Cancel();
                        resultPPT = false;
                        sbResult.AppendLine("Powerpoint is terminated by User.");
                        txtResult.AppendText("Powerpoint is terminated by User." + "\n");
                    }

                    if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes" && flag)
                    {
                        sbResult.AppendLine(fpsTaskDataPowerPoint);
                        txtResult.AppendText(fpsTaskDataPowerPoint);
                        sbResult.AppendLine("PowerPoint Sample File ran successfully from: " + sampleFileFullPath + "\n \n");
                        txtResult.AppendText("PowerPoint Sample File ran successfully from: " + sampleFileFullPath + "\n \n");
                        resultPPT = true;
                    }
                    else
                    {
                        sbResult.AppendLine("FPS is not available as the Scenario failed.");
                        txtResult.AppendText("FPS is not available as the Scenario failed." + "\n");
                    }
                    StreamWriter fileFPSLogPowerPointEnd = new StreamWriter(Constants.LOG_FILE_NAME, true);
                    fileFPSLogPowerPointEnd.WriteLine("END - PowerPoint Presentation - " + DateTime.Now.ToString() + "\n\n");
                    fileFPSLogPowerPointEnd.Close();
                }

                this.Refresh();
            }

            catch (Exception ex)
            {
                resultPPT = false;
                sbResult.AppendLine("ERROR: Runtime Error in TC1_2DGraphics - PowerPoint Presentation Failed - " + ex.Message);
                txtResult.AppendText("ERROR: Runtime Error in TC1_2DGraphics - PowerPoint Presentation Failed - " + ex.Message + "\n");
            }
            this.Refresh();
            Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);

            #endregion - PowerPoint File Testing

            #region - Command Prompt Movement

            StreamWriter fileFPSCommandPromptStart = new StreamWriter(Constants.LOG_FILE_NAME, true);
            fileFPSCommandPromptStart.WriteLine("START - Command Prompt Movement - " + DateTime.Now.ToString());
            fileFPSCommandPromptStart.Close();

            try
            {
                string fpsTaskDataCommandPrompt = "";
                tokenSource = new CancellationTokenSource();
                token = tokenSource.Token;
                if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes")
                {
                    Task fpsTaskForCommandPrompt = Task.Factory.StartNew(() =>
                    fpsTaskDataCommandPrompt = Utilities.CalculateFPS(TestCaseID, "Command Prompt Movement", Constants.FPS_INTERVAL, Constants.SAMPLE_FILE_RUNTIME_COMMANDPROMPT / 1000, token), token);
                }

                Process ps = new Process();
                ps.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
                ps.StartInfo.FileName = @"" + "\"" + Constants.APP_EXECUTABLE_COMMANDPROMPT + "\"";
                ps.Start();

                var stopWatchCommandPrompt = new System.Diagnostics.Stopwatch();
                stopWatchCommandPrompt.Start();

                IntPtr id = ps.MainWindowHandle;

                int screenWidth = Screen.PrimaryScreen.WorkingArea.Width;
                int screenHeight = Screen.PrimaryScreen.WorkingArea.Height;
                int dimensionCmdPrompt = screenHeight / 3;
                double coordinateShift = Math.Abs(((double)screenWidth / (double)screenHeight) - 1);

                MoveWindow(ps.MainWindowHandle, 1, 1, dimensionCmdPrompt, dimensionCmdPrompt, true);

                for (int iRepeat = 0; iRepeat < 40; iRepeat++)
                {
                    for (int xCoordinate = 1; xCoordinate < screenWidth - dimensionCmdPrompt; xCoordinate++)
                    {
                        MoveWindow(ps.MainWindowHandle, xCoordinate, 1, dimensionCmdPrompt, dimensionCmdPrompt, true);
                        //Thread.Sleep(Constants.COMMANDPROMPT_MOVEMENT_SPEED_HORIZONTAL);
                    }

                    for (double xCoordinate = screenWidth - dimensionCmdPrompt, yCoordinate = 1; yCoordinate < screenHeight - dimensionCmdPrompt || xCoordinate > 0; xCoordinate--, yCoordinate = yCoordinate + coordinateShift)
                    {
                        MoveWindow(ps.MainWindowHandle, Convert.ToInt32(xCoordinate--), Convert.ToInt32(yCoordinate), dimensionCmdPrompt, dimensionCmdPrompt, true);
                        //Thread.Sleep(Constants.COMMANDPROMPT_MOVEMENT_SPEED_DIAGONAL);
                    }

                    for (int xCoordinate = 1; xCoordinate < screenWidth - dimensionCmdPrompt; xCoordinate++)
                    {
                        MoveWindow(ps.MainWindowHandle, xCoordinate, Convert.ToInt32(screenHeight) - dimensionCmdPrompt, dimensionCmdPrompt, dimensionCmdPrompt, true);
                        //Thread.Sleep(Constants.COMMANDPROMPT_MOVEMENT_SPEED_HORIZONTAL);
                    }

                    for (double xCoordinate = screenWidth - dimensionCmdPrompt, yCoordinate = screenHeight - dimensionCmdPrompt; yCoordinate > 0 || xCoordinate > 0; xCoordinate--, yCoordinate = yCoordinate - coordinateShift)
                    {
                        MoveWindow(ps.MainWindowHandle, Convert.ToInt32(xCoordinate--), Convert.ToInt32(yCoordinate), dimensionCmdPrompt, dimensionCmdPrompt, true);
                        //Thread.Sleep(Constants.COMMANDPROMPT_MOVEMENT_SPEED_DIAGONAL);
                    }

                    Thread.Sleep(1000);
                }

                ps.CloseMainWindow();
                ps.Close();

                stopWatchCommandPrompt.Stop();
                txtResult.AppendText("Time taken to run Command Prompt: " +
                    Convert.ToString(stopWatchCommandPrompt.ElapsedMilliseconds) + " ms (" +
                    Convert.ToString(stopWatchCommandPrompt.ElapsedMilliseconds / (decimal)1000) + " sec)" + "\n");

                if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes")
                {
                    sbResult.AppendLine(fpsTaskDataCommandPrompt);
                    txtResult.AppendText(fpsTaskDataCommandPrompt);
                    resultCmdPrompt = true;
                }
                else
                {
                    sbResult.AppendLine("Unable to calculate FPS using HDX Monitor.");
                    txtResult.AppendText("Unable to calculate FPS using HDX Monitor." + "\n");
                }

                sbResult.AppendLine("Moving of Command Prompt Window on screen completed" + "\n\n");
                txtResult.AppendText("Moving of Command Prompt Window on screen completed" + "\n\n");
            }

            catch (Exception ex)
            {
                resultCmdPrompt = false;
                sbResult.AppendLine("ERROR: Runtime Error in TC1_2DGraphics - Moving Command Prompt Failed - " + ex.Message);
                txtResult.AppendText("ERROR: Runtime Error in TC1_2DGraphics - Moving Command Prompt Failed - " + ex.Message + "\n");
            }
            this.Refresh();

            Thread.Sleep(5000);

            #endregion - Move Command Prompt

            #region - PDF File Scrolling Testing

            StreamWriter fileFPSAcrobatPDFStart = new StreamWriter(Constants.LOG_FILE_NAME, true);
            fileFPSAcrobatPDFStart.WriteLine("START - Acrobat Reader PDF Scrolling - " + DateTime.Now.ToString());
            fileFPSAcrobatPDFStart.Close();

            try
            {
                string fpsTaskDataAcrobatPDF = "";
                bool result = true;
                tokenSource = new CancellationTokenSource();
                token = tokenSource.Token;
                sampleFileFullPath = Globals.SampleFilesLocation;
                sampleFileFullPath = sampleFileFullPath + Constants.SAMPLE_FILE_NAME_ACROBAT;
                if (!System.IO.File.Exists(sampleFileFullPath))
                {
                    sbResult.AppendLine("Acrobat Reader PDF Sample File is missing: " + sampleFileFullPath);
                    txtResult.AppendText("Acrobat Reader PDF Sample File is missing: " + sampleFileFullPath + "\n");
                }
                else
                {
                    if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes")
                    {
                        Task fpsTaskForAcrobatPDF = Task.Factory.StartNew(() =>
                        fpsTaskDataAcrobatPDF = Utilities.CalculateFPS(TestCaseID, "Acrobat Reader PDF Scrolling", Constants.FPS_INTERVAL, Constants.SAMPLE_FILE_RUNTIME_ACROBAT / 1000, token), token);
                    }

                    ProcessStartInfo psStartInfo = new ProcessStartInfo();
                    psStartInfo.FileName = @"" + "\"" + Constants.APP_EXECUTABLE_ACROBAT + "\"";
                    psStartInfo.Arguments = @" " + "\"" + sampleFileFullPath + "\"";
                    Process ps = Process.Start(psStartInfo);

                    Thread.Sleep(Constants.DUMMY_RUNTIME);
                    System.Diagnostics.Stopwatch watcher = System.Diagnostics.Stopwatch.StartNew();

                    //Simulate the scrolling in PDF file
                    while (watcher.ElapsedMilliseconds < Constants.SAMPLE_FILE_RUNTIME_ACROBAT_SCROLL + 20000)
                    {
                        if (!ps.HasExited)
                        {
                            for (int iCounterDown = 0; iCounterDown < 400; iCounterDown++)
                            {
                                SendKeys.Send("{Down}");
                            }
                            for (int iCounterUp = 0; iCounterUp < 300; iCounterUp++)
                            {
                                SendKeys.Send("{Up}");
                            }
                            Thread.Sleep(500);
                        }
                        else
                        {
                            result = false;
                            break;
                        }
                    }
                    ps.Kill();



                    if (result == false)
                    {
                        tokenSource.Cancel();
                        resultPDF = false;
                        sbResult.AppendLine("PDF scrolling aborted by User.");
                        txtResult.AppendText("PDF scrolling aborted by User." + "\n");
                    }

                    if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes" && result)
                    {
                        //fpsThreadForAcrobatPDF.Abort();
                        sbResult.AppendLine(fpsTaskDataAcrobatPDF);
                        txtResult.AppendText(fpsTaskDataAcrobatPDF);
                        sbResult.AppendLine("Acrobat Reader PDF Sample File ran successfully from: " + sampleFileFullPath + "\n\n");
                        txtResult.AppendText("Acrobat Reader PDF Sample File ran successfully from: " + sampleFileFullPath + "\n\n");
                        resultPDF = true;
                    }
                    else
                    {
                        sbResult.AppendLine("Unable to calculate FPS using HDX Monitor.");
                        txtResult.AppendText("Unable to calculate FPS using HDX Monitor." + "\n");
                    }
                }

                //TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_COMPLETED;

                StreamWriter fileFPSAcrobatPDFEnd = new StreamWriter(Constants.LOG_FILE_NAME, true);
                fileFPSAcrobatPDFEnd.WriteLine("END - Acrobat Reader PDF Scrolling - " + DateTime.Now.ToString() + "\n\n");
                fileFPSAcrobatPDFEnd.Close();
                this.Refresh();

            }

            catch (Exception ex)
            {
                resultPDF = false;
                sbResult.AppendLine("ERROR: Runtime Error in TC1_2DGraphics - Acrobat Reader PDF Test Failed - " + ex.Message);
                txtResult.AppendText("ERROR: Runtime Error in TC1_2DGraphics - Acrobat Reader PDF Test Failed - " + ex.Message + "\n");
                //TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;
            }


            this.Refresh();
            Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);

            #endregion - PDF File Testing

            #region - Video Clip Testing using VLC Player

            StreamWriter fileFPSLogVLCPlayerStart = new StreamWriter(Constants.LOG_FILE_NAME, true);
            fileFPSLogVLCPlayerStart.WriteLine("START - VLC Player Video Clip - " + DateTime.Now.ToString());
            fileFPSLogVLCPlayerStart.Close();

            try
            {
                string fpsTaskDataVLCPlayer = "";
                tokenSource = new CancellationTokenSource();
                token = tokenSource.Token;
                sampleFileFullPath = Globals.SampleFilesLocation;
                sampleFileFullPath = sampleFileFullPath + Constants.SAMPLE_FILE_NAME_MOVIE_WMV;
                if (!System.IO.File.Exists(sampleFileFullPath))
                {
                    sbResult.AppendLine("VLC Movie Sample File is missing: " + sampleFileFullPath);
                    txtResult.AppendText("VLC Movie Sample File is missing: " + sampleFileFullPath + "\n");
                }
                else
                {
                    if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes")
                    {
                        Task fpsTaskForVLCPlayer = Task.Factory.StartNew(() =>
                        fpsTaskDataVLCPlayer = Utilities.CalculateFPS(TestCaseID, "VLC Player Video Clip", Constants.FPS_INTERVAL, (Constants.FPS_CALCULATION_RUNTIME) / 1000, token), token);
                    }
                    ProcessStartInfo psStartInfo = new ProcessStartInfo();
                    if (System.IO.File.Exists(Constants.APP_EXECUTABLE_VLCPLAYER64BIT))
                    {
                        psStartInfo.FileName = @"" + "\"" + Constants.APP_EXECUTABLE_VLCPLAYER64BIT + "\"";
                    }
                    else
                    {
                        psStartInfo.FileName = @"" + "\"" + Constants.APP_EXECUTABLE_VLCPLAYER + "\"";
                    }
                    psStartInfo.Arguments = @" -f " + "\"" + sampleFileFullPath + "\"";
                    psStartInfo.WindowStyle = ProcessWindowStyle.Maximized;
                    Process ps = Process.Start(psStartInfo);
                    Thread.Sleep(Constants.DUMMY_RUNTIME);
                    System.Diagnostics.Stopwatch watcher = System.Diagnostics.Stopwatch.StartNew();

                    bool flag = Utilities.ProcessMonitor(ps, Constants.SAMPLE_FILE_RUNTIME_MOVIE_WMV + 10000);

                    if (flag == false)
                    {
                        tokenSource.Cancel();
                        resultVLC = false;
                        sbResult.AppendLine("VLC media player aborted by User.");
                        txtResult.AppendText("VLC media player aborted by User." + "\n");
                    }
                    if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes" && flag)
                    {
                        sbResult.AppendLine(fpsTaskDataVLCPlayer);
                        txtResult.AppendText(fpsTaskDataVLCPlayer);
                        sbResult.AppendLine("VLC Movie Sample File ran successfully from: " + sampleFileFullPath + "\n\n");
                        txtResult.AppendText("VLC Movie Sample File ran successfully from: " + sampleFileFullPath + "\n\n");
                        resultVLC = true;
                    }
                    else
                    {
                        sbResult.AppendLine("Unable to calculate FPS using HDX Monitor.");
                        txtResult.AppendText("Unable to calculate FPS using HDX Monitor." + "\n");
                    }


                }

                StreamWriter fileFPSLogVLCPlayerEnd = new StreamWriter(Constants.LOG_FILE_NAME, true);
                fileFPSLogVLCPlayerEnd.WriteLine("END - VLC Player Video Clip - " + DateTime.Now.ToString() + "\n\n");
                fileFPSLogVLCPlayerEnd.Close();
                this.Refresh();
            }

            catch (Exception ex)
            {
                resultVLC = false;
                sbResult.AppendLine("ERROR: Runtime Error in TC1_2DGraphics - VLC Movie Sample Test Failed - " + ex.Message);
                txtResult.AppendText("ERROR: Runtime Error in TC1_2DGraphics - VLC Movie Sample Test Failed - " + ex.Message + "\n");
                //TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;
            }

            this.Refresh();



            #endregion-Video Clip Testing

            TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
            if ((resultPPT && resultCmdPrompt) && (resultPDF && resultVLC)) { returnValue = Constants.VERIFICATION_STATUS_COMPLETED; }
            TestCases.oTestCases[TestCaseID - 1].Status = returnValue;
            return returnValue;

        }

        #endregion

        #region - Test Case # 2 - Secure Web and SAAS Apps
        public string TC2_SecureWebSAASApps(byte TestCaseID)
        {
            StringBuilder sbResult = new StringBuilder("");
            string tc16TestcaseStatus = "";
            txtResult.AppendText("\n");
            txtResult.AppendText("Running " + Constants.TESTCASE02_NAME + " Test Case - " + DateTime.Now.ToString() + "\n");
            sbResult.AppendLine("Secure Web and SAAS Apps");
            txtResult.AppendText("Secure Web and SAAS Apps completed" + "\n");

            TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_COMPLETED;
            TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
            tc16TestcaseStatus = Constants.VERIFICATION_STATUS_COMPLETED;
            return tc16TestcaseStatus;
        }

        #endregion

        #region - Test Case # 3 - Audio Recording and Playback
        public string TC3_AudioRec(byte TestCaseID)
        {
            StringBuilder sbResult = new StringBuilder("");
            string usbDeviceManufacturer = "";
            string msgUser = "";
            string soundRecorderApp = "";
            bool resultRec = false;

            bool isStoreApp = false;
            //For Windows 10, setting the isStoreApp to true 
            if (SysInformations.oSysInformations[0].ConfigValue.Contains("Windows 10")) { isStoreApp = true; }

            txtResult.AppendText("\n");
            txtResult.AppendText("Running " + Constants.TESTCASE03_NAME + " Test Case - " + DateTime.Now.ToString() + "\n");

            ManagementObjectSearcher usbDevices = new ManagementObjectSearcher("SELECT * FROM Win32_PnPEntity WHERE Service='usbaudio' AND Status='OK'");
            int connectedUsbCount = usbDevices.Get().Count;

            msgUser = "1) Ensure the USB Audio Headset is connected to the Endpoint" + "\n" +
                             "2) Navigate to ‘Devices’ on Citrix Receiver and click on ‘Switch to generic’ for the connected USB Headset to redirect the headset to the session" + "\n" + "\n" +
                             "Once the Windows Voice Recorder App is launched, record a audio clip for 30-40 seconds and save it to the " +
                             "virtual desktops.";
            MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE03_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);

            #region - comments
            /*if (connectedUsbCount < 1)
            {
                PlatformID platform = Environment.OSVersion.Platform;
                if (platform.Equals(PlatformID.MacOSX) || platform.Equals(PlatformID.Unix))
                {
                    msgUser = "Please ensure that the USB device is properly plugged in and is connected locally" + "\n";
                    MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE01_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (platform.Equals(PlatformID.Win32NT) || platform.Equals(PlatformID.Win32S) || platform.Equals(PlatformID.Win32Windows) || platform.Equals(PlatformID.WinCE))
                {
                    msgUser = "1) Ensure the USB Audio Headset is connected to the Endpoint" + "\n" +
                             "2) It is set as default Audio input and output device in VDA audio settings" + "\n" +
                             "3) Navigate to Citrix Toolbar -> Preferences, Click on “Switch to generic mode” for Audio Device" + "\n\n" +
                             "Once the Windows 10 Voice Recorder App is launched, record a audio clip for 30-40 seconds and save it to the " +
                             "virtual desktops.";
                    MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE03_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }*/
            #endregion

            try
            {
                if (usbDevices.Get().Count > 0)
                {
                    foreach (var mo in usbDevices.Get())
                    {
                        if (mo["Manufacturer"] != null) usbDeviceManufacturer = Convert.ToString(mo["Caption"]);
                    }
                    txtResult.AppendText("USB Audio Device: " + usbDeviceManufacturer + " " + "\n");
                    sbResult.AppendLine("USB Audio Device: " + usbDeviceManufacturer + " ");


                    // new Code

                    if (isStoreApp)
                    {
                        if (SysInformations.oSysInformations[1].ConfigValue.Contains("64"))
                        {
                            soundRecorderApp = @"" + Constants.APP_EXECUTABLE_SOUNDRECORDER_PRE_WIN10_64BIT;
                        }
                        else
                        {
                            soundRecorderApp = @"" + Constants.APP_EXECUTABLE_SOUNDRECORDER_PRE_WIN10;
                        }

                        #region - Windows 10 Store App Approach - USB Headset
                        var mgr = new ApplicationActivationManager();
                        uint processId;
                        var name = "Microsoft.WindowsSoundRecorder_8wekyb3d8bbwe!App";
                        mgr.ActivateApplication(name, null, ActivateOptions.None, out processId);
                        frmTimer formTimer1 = new frmTimer(45000, 3);
                        formTimer1.ShowDialog();

                        killSoundRec("SoundRec");

                        #endregion - Windows 10 Store App Approach - USB Headset

                        #region - Windows 10 Store App Approach - 3.5mm Jack
                        this.Enabled = false;

                        Thread.Sleep(Constants.DUMMY_RUNTIME);
                        msgUser = "Please save the recorded message and listen to the recording." + "\n" +
                            " Once the recording is audible correctly, disconnect the USB headset and connect a 3.5mm jack headset, record an audio clip for 30 seconds." + "\n" +
                            "Save the audio clip and listen to the recording using the connected 3.5mm headset" + "\n" + "\n";
                        MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE03_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);

                        mgr.ActivateApplication(name, null, ActivateOptions.None, out processId);
                        frmTimer formTimer2 = new frmTimer(45000, 3);
                        formTimer2.ShowDialog();
                        sbResult.AppendLine("Sound Recorder with 3.5mm Headset completed");
                        txtResult.AppendText("Sound Recorder with 3.5mm Headset completed" + "\n");
                        resultRec = true;
                        killSoundRec("SoundRec");
                        this.Enabled = true;
                        #endregion - Windows 10 Store App Approach - 3.5mm Jack

                    }
                    else
                    {
                        if (SysInformations.oSysInformations[1].ConfigValue.Contains("64"))
                        {
                            soundRecorderApp = @"" + Constants.APP_EXECUTABLE_SOUNDRECORDER_PRE_WIN10_64BIT;
                        }
                        else
                        {
                            soundRecorderApp = @"" + Constants.APP_EXECUTABLE_SOUNDRECORDER_PRE_WIN10;
                        }

                        #region - Windows regular Application Approach - USB Headset

                        ProcessStartInfo psStartInfoUSB = new ProcessStartInfo();
                        psStartInfoUSB.FileName = @"" + "\"" + soundRecorderApp + "\"";

                        Process psUSB = Process.Start(psStartInfoUSB);
                        frmTimer formTimer3 = new frmTimer(45000, 3);
                        formTimer3.ShowDialog();

                        //killSoundRec(psUSB.ProcessName);

                        //bool flag = Utilities.ProcessMonitor(psUSB, Constants.SAMPLE_FILE_RUNTIME_AUDIO_RECORDING);


                        sbResult.AppendLine("Sound Recorder with USB Headset completed");
                        txtResult.AppendText("Sound Recorder with USB Headset completed" + "\n");

                        killSoundRec(psUSB.ProcessName);
                        #endregion - Windows regular Application Approach - USB Headset

                        #region - Windows regular Application Approach - 3.5mm Jack

                        msgUser = "Please save the recorded message and listen to the recording." + "\n" +
                            " Once the recording is audible correctly, disconnect the USB headset and connect a 3.5mm jack headset, record an audio clip for 30 seconds." + "\n" +
                            "Save the audio clip and listen to the recording using the connected 3.5mm headset" + "\n" + "\n";
                        MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE03_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        resultRec = true;

                        if (psUSB.HasExited)
                        {
                            ProcessStartInfo psStartInfo35 = new ProcessStartInfo();
                            psStartInfo35.FileName = @"" + "\"" + soundRecorderApp + "\"";
                            Process ps35 = Process.Start(psStartInfo35);

                            frmTimer formTimer4 = new frmTimer(45000, 3);
                            formTimer4.ShowDialog();
                            sbResult.AppendLine("Sound Recorder with 3.5mm Headset completed");
                            txtResult.AppendText("Sound Recorder with 3.5mm Headset completed" + "\n");
                            killSoundRec("SoundRec");
                        }
                        else
                        {
                            frmTimer formTimer5 = new frmTimer(45000, 3);
                            formTimer5.ShowDialog();
                            sbResult.AppendLine("Sound Recorder with 3.5mm Headset completed");
                            txtResult.AppendText("Sound Recorder with 3.5mm Headset completed" + "\n");
                            killSoundRec(psUSB.ProcessName);
                        }
                        killSoundRec("SoundRec");
                        #endregion - Windows regular Application Approach - 3.5mm Jack

                    }

                    this.Refresh();
                    if (resultRec == true)
                    {
                        TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                        TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_COMPLETED;
                        return Constants.VERIFICATION_STATUS_COMPLETED;
                    }
                    else
                    {
                        TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                        TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;
                        return Constants.VERIFICATION_STATUS_ERROR;
                    }
                }
                else
                {
                    msgUser = "Please Plug in Usb Headset" + "\n" + "\n";
                    MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE03_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                    TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;
                    sbResult.AppendLine("Audio Recording and Playback Failed - No USB Headset is Detected");
                    txtResult.AppendText("Audio Recording and Playback Failed - No USB Headset is Detected" + "\n");
                    return Constants.VERIFICATION_STATUS_ERROR;
                }


            }

            catch (Exception ex)
            {
                sbResult.AppendLine("ERROR: Runtime Error in TC3_AudioRec - Sound Recording Test Failed - " + ex.Message + " (" + soundRecorderApp + ")");
                txtResult.AppendText("ERROR: Runtime Error in TC3_AudioRec - Sound Recording Test Failed - " + ex.Message + " (" + soundRecorderApp + ")" + "\n");
                TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;

                return Constants.VERIFICATION_STATUS_ERROR;
            }

        }

        public void killSoundRec(string proname)
        {
            try
            {
                Process[] soundRecApp = Process.GetProcessesByName("SoundRec");
                if (soundRecApp.Length > 0)
                {
                    foreach (Process pro in soundRecApp)
                    {
                        pro.Kill();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        #region - Voice Recorder Windows Store App related Functions

        public enum ActivateOptions
        {
            None = 0x00000000,  // No flags set
            DesignMode = 0x00000001,  // The application is being activated for design mode, and thus will not be able to
            // to create an immersive window. Window creation must be done by design tools which
            // load the necessary components by communicating with a designer-specified service on
            // the site chain established on the activation manager.  The splash screen normally
            // shown when an application is activated will also not appear.  Most activations
            // will not use this flag.
            NoErrorUI = 0x00000002,  // Do not show an error dialog if the app fails to activate.                                
            NoSplashScreen = 0x00000004,  // Do not show the splash screen when activating the app.
        }

        [ComImport, Guid("2e941141-7f97-4756-ba1d-9decde894a3d"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        interface IApplicationActivationManager
        {
            // Activates the specified immersive application for the "Launch" contract, passing the provided arguments
            // string into the application.  Callers can obtain the process Id of the application instance fulfilling this contract.
            IntPtr ActivateApplication([In] String appUserModelId, [In] String arguments, [In] ActivateOptions options, [Out] out UInt32 processId);
            IntPtr ActivateForFile([In] String appUserModelId, [In] [MarshalAs(UnmanagedType.Interface, IidParameterIndex = 2)] /*IShellItemArray* */ IShellItemArray itemArray, [In] String verb, [Out] out UInt32 processId);
            IntPtr ActivateForProtocol([In] String appUserModelId, [In] IntPtr /* IShellItemArray* */itemArray, [Out] out UInt32 processId);
        }

        [ComImport, Guid("45BA127D-10A8-46EA-8AB7-56EA9078943C")]//Application Activation Manager
        class ApplicationActivationManager : IApplicationActivationManager
        {
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)/*, PreserveSig*/]
            public extern IntPtr ActivateApplication([In] String appUserModelId, [In] String arguments, [In] ActivateOptions options, [Out] out UInt32 processId);
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            public extern IntPtr ActivateForFile([In] String appUserModelId, [In] [MarshalAs(UnmanagedType.Interface, IidParameterIndex = 2)]  /*IShellItemArray* */ IShellItemArray itemArray, [In] String verb, [Out] out UInt32 processId);
            [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
            public extern IntPtr ActivateForProtocol([In] String appUserModelId, [In] IntPtr /* IShellItemArray* */itemArray, [Out] out UInt32 processId);
        }

        [ComImport]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [Guid("43826d1e-e718-42ee-bc55-a1e261c37bfe")]
        interface IShellItem
        {
        }

        [ComImport]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [Guid("b63ea76d-1f85-456f-a19c-48159efa858b")]
        interface IShellItemArray
        {
        }

        #endregion

        #endregion

        #region - Test Case # 4 - Video Playback - Server Rendered flash
        public string TC4_VidPlaybackFlash(byte TestCaseID)
        {
            string msgUser = "";
            StringBuilder sbResult = new StringBuilder("");
            string result = Constants.VERIFICATION_STATUS_ERROR;

            tokenSource = new CancellationTokenSource();
            token = tokenSource.Token;

            bool isHDXMonitoringToolAvailable = SysRequirements.oSysRequirements[Constants.TC_HDX_MONITOR_TOOL - 1].IsInstalled;
            if (!isHDXMonitoringToolAvailable) sbResult.AppendLine("HDX Monitor Tool is not available");

            txtResult.AppendText("\n");
            txtResult.AppendText("Running " + Constants.TESTCASE04_NAME + " Test Case - " + DateTime.Now.ToString() + "\n");

            msgUser = @"Instruction for YouTube Test Scenario:" + "\n" + "\n" +
                      @"After YouTube video is launched in the Browser, from the YouTube Video Settings menu please " +
                      @"set the video resolution to 720p and switch the video to full screen.";
            MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE04_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);

            StreamWriter fileFPSLogIEYouTubeStart = new StreamWriter(Constants.LOG_FILE_NAME, true);
            fileFPSLogIEYouTubeStart.WriteLine("START - Internet Explorer YouTube Video Clip - " + DateTime.Now.ToString());
            fileFPSLogIEYouTubeStart.Close();
            try
            {
                string fpsTaskDataIEYouTube = "";
                Task fpsTaskForIEYouTube;
                if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes")
                {
                    fpsTaskForIEYouTube = Task.Factory.StartNew(() =>
                    fpsTaskDataIEYouTube = Utilities.CalculateFPS(TestCaseID, "Internet Explorer YouTube Video Clip", Constants.FPS_INTERVAL, (Constants.FPS_CALCULATION_RUNTIME) / 1000, token), token);
                }

                Process ps = Process.Start("\"" + Constants.APP_EXECUTABLE_INTERNETEXPLORER + "\"",
                    "\"" + Constants.SAMPLE_FILE_NAME_YOUTUBE + "\"");

                bool flag = Utilities.ProcessMonitor(ps, Constants.SAMPLE_FILE_RUNTIME_YOUTUBE);

                if (!ps.HasExited)
                {
                    ps.Kill();
                }

                if (flag == false)
                {
                    tokenSource.Cancel();
                    sbResult.AppendLine("Internet Explorer is terminated by User.");
                    txtResult.AppendText("Internet Explorer is terminated by User." + "\n");
                }

                if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes" && flag)
                {
                    sbResult.AppendLine(fpsTaskDataIEYouTube);
                    txtResult.AppendText(fpsTaskDataIEYouTube);
                    sbResult.AppendLine("Video Playback - Server Rendered Flash Plugin Test ran successfully from: " + Constants.SAMPLE_FILE_NAME_YOUTUBE + "\n");
                    txtResult.AppendText("Video Playback - Server Rendered Flash Plugin Test ran successfully from: " + Constants.SAMPLE_FILE_NAME_YOUTUBE + "\n");
                    result = Constants.VERIFICATION_STATUS_COMPLETED;
                }

                else
                {
                    sbResult.AppendLine("Unable to calculate FPS using HDX Monitor.");
                    txtResult.AppendText("Unable to calculate FPS using HDX Monitor." + "\n");
                }

                StreamWriter fileFPSLogIEYouTubeEnd = new StreamWriter(Constants.LOG_FILE_NAME, true);
                fileFPSLogIEYouTubeEnd.WriteLine("END - Internet Explorer YouTube Video Clip - " + DateTime.Now.ToString());
                fileFPSLogIEYouTubeEnd.Close();

                TestCases.oTestCases[TestCaseID - 1].Status = result;
                TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                return result;
            }

            catch (Exception ex)
            {
                sbResult.AppendLine("ERROR: Runtime Error in TC4_VidPlaybackFlash - Video Playback - Server Rendered Flash Plugin Test Failed - " + ex.Message);
                txtResult.AppendText("ERROR: Runtime Error in TC4_VidPlaybackFlash - Video Playback - Server Rendered Flash Plugin Test Failed - " + ex.Message + "\n");
                TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;

                return Constants.VERIFICATION_STATUS_ERROR;
            }
        }
        #endregion

        #region - Test Case # 5 - Video Playback - Server Rendered Windows Media
        public string TC5_VidPlaybackPlayer(byte TestCaseID)
        {
            StringBuilder sbResult = new StringBuilder("");
            string sampleFileFullPath = "";
            string resultVidPlayback = Constants.VERIFICATION_STATUS_ERROR;
            tokenSource = new CancellationTokenSource();
            token = tokenSource.Token;

            bool isHDXMonitoringToolAvailable = SysRequirements.oSysRequirements[Constants.TC_HDX_MONITOR_TOOL - 1].IsInstalled;
            if (!isHDXMonitoringToolAvailable) sbResult.AppendLine("HDX Monitor Tool is not available");

            txtResult.AppendText("\n");
            txtResult.AppendText("Running " + Constants.TESTCASE05_NAME + " Test Case - " + DateTime.Now.ToString() + "\n");

            Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);
            StreamWriter fileFPSLogWindowsMediaPlayer = new StreamWriter(Constants.LOG_FILE_NAME, true);
            fileFPSLogWindowsMediaPlayer.WriteLine("START - Windows Media Player Video Clip - " + DateTime.Now.ToString());
            fileFPSLogWindowsMediaPlayer.Close();

            try
            {
                string fpsTaskDataWindowsMediaPlayer = "";

                sampleFileFullPath = Globals.SampleFilesLocation;
                sampleFileFullPath = sampleFileFullPath + Constants.SAMPLE_FILE_NAME_MOVIE_MP4;
                if (!System.IO.File.Exists(sampleFileFullPath))
                {
                    sbResult.AppendLine("ERROR: Windows Media Player MP4 Movie Sample File is missing: " + sampleFileFullPath);
                    txtResult.AppendText("ERROR: Windows Media Player MP4 Movie Sample File is missing: " + sampleFileFullPath + "\n");
                }
                else
                {
                    if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes")
                    {
                        Task fpsTaskForWindowsMediaPlayer = Task.Factory.StartNew(() =>
                        fpsTaskDataWindowsMediaPlayer = Utilities.CalculateFPS(TestCaseID, "Windows Media Player Video Clip", Constants.FPS_INTERVAL, (Constants.FPS_CALCULATION_RUNTIME) / 1000, token), token);
                    }

                    ProcessStartInfo psStartInfo = new ProcessStartInfo();
                    psStartInfo.FileName = @"" + "\"" + Constants.APP_EXECUTABLE_WINDOWSMEDIAPLAYER + "\"";
                    psStartInfo.Arguments = @" " + "\"" + sampleFileFullPath + "\" /fullscreen";
                    psStartInfo.WindowStyle = ProcessWindowStyle.Maximized;
                    Process ps = Process.Start(psStartInfo);

                    bool flag = Utilities.ProcessMonitor(ps, Constants.SAMPLE_FILE_RUNTIME_MP4);

                    if (!ps.HasExited)
                    {
                        ps.Kill();
                    }
                    if (flag == false)
                    {
                        tokenSource.Cancel();
                        sbResult.AppendLine("Media Player terminated by User abruptly.");
                        txtResult.AppendText("Media Player terminated by User abruptly." + "\n");
                    }
                    if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes" && flag)
                    {
                        sbResult.AppendLine(fpsTaskDataWindowsMediaPlayer);
                        txtResult.AppendText(fpsTaskDataWindowsMediaPlayer);
                        sbResult.AppendLine("Windows Media Player Movie Sample File ran successfully from: " + sampleFileFullPath);
                        txtResult.AppendText("Windows Media Player Movie Sample File ran successfully from: " + sampleFileFullPath + "\n");
                        resultVidPlayback = Constants.VERIFICATION_STATUS_COMPLETED;
                    }
                    else
                    {
                        sbResult.AppendLine("Unable to calculate FPS using HDX Monitor.");
                        txtResult.AppendText("Unable to calculate FPS using HDX Monitor." + "\n");
                    }
                }

                StreamWriter fileFPSLogWindowsMediaPlayerEnd = new StreamWriter(Constants.LOG_FILE_NAME, true);
                fileFPSLogWindowsMediaPlayerEnd.WriteLine("END - Windows Media Player Video Clip - " + DateTime.Now.ToString());
                fileFPSLogWindowsMediaPlayerEnd.Close();

                TestCases.oTestCases[TestCaseID - 1].Status = resultVidPlayback;
                TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                return resultVidPlayback;
            }
            catch (Exception ex)
            {
                sbResult.AppendLine("ERROR: Runtime Error in TC5_VidPlaybackPlayer - Video Playback - Server Rendered Windows Media Test Failed - " + ex.Message);
                txtResult.AppendText("ERROR: Runtime Error in TC5_VidPlaybackPlayer - Video Playback - Server Rendered Windows Media Test Failed - " + ex.Message + "\n");
                TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;

                return Constants.VERIFICATION_STATUS_ERROR;
            }
        }
        #endregion

        #region - Test Case # 6 - Client Printer Test Using Citrix Universal Printer Driver
        public string TC6_ClientPrintTest(byte TestCaseID)
        {
            bool isSavePaper = true;
            bool isXPSPrint = true;
            StringBuilder sbResult = new StringBuilder("");
            string sampleFileFullPath = "";
            string defaultPrinterDeviceID = "";
            string msgUser = "";
            bool isctxDriver = false;
            txtResult.AppendText("\n");
            txtResult.AppendText("Running " + Constants.TESTCASE06_NAME + " Test Case - " + DateTime.Now.ToString() + "\n");
            Thread.Sleep(3000);

            #region - Check Printer Details
            try
            {
                using (ManagementObjectSearcher printers = new ManagementObjectSearcher("SELECT * FROM Win32_Printer WHERE Default=TRUE"))
                {
                    if (printers != null)
                    {
                        if (printers.Get() != null)
                        {
                            if (printers.Get().Count < 0 || printers.Get().Count != 0)
                            {
                                msgUser = "Please connect a printer to endpoint and set it as Default printer." + "\n" +
                                "Navigate to ‘Devices’ on Citrix Receiver and click on ‘Switch to generic’ for the connected printer to redirect the printer to the session." + "\n" + "\n" +
                                "Once you are done, click OK to continue.";
                                MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE06_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            if (printers.Get() != null && printers.Get().Count > 0)
                            {
                                foreach (ManagementObject mo in printers.Get())
                                {
                                    if (mo["DeviceID"] != null)
                                    {
                                        sbResult.AppendLine("Device Name: " + Convert.ToString(mo["DeviceID"]));
                                        txtResult.AppendText("Device Name: " + Convert.ToString(mo["DeviceID"]) + "\n");
                                        sbResult.AppendLine("Driver Name: " + Convert.ToString(mo["DriverName"]));
                                        txtResult.AppendText("Driver Name: " + Convert.ToString(mo["DriverName"]) + "\n");
                                        string driverName = Convert.ToString(mo["DriverName"]).ToUpper();
                                        string deviceName = Convert.ToString(mo["DeviceID"]).ToUpper();
                                        isctxDriver = driverName.Contains("UNIVERSAL PRINTER");
                                        isXPSPrint = deviceName.Contains("MICROSOFT");

                                        if (Globals.ClientPlatformId == "51")
                                        {
                                            isctxDriver = true;
                                        }
                                        else
                                        {
                                            if (isctxDriver)
                                            {
                                                sbResult.AppendLine("Your default printer is setup using Universal Printer driver");
                                                txtResult.AppendText("Your default printer is setup using Universal Printer driver" + "\n");
                                            }
                                            else
                                            {
                                                msgUser = "Please navigate to printer properties and set the driver to universal printer driver.";
                                                MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE06_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            }
                                        }

                                    }
                                }
                            }
                            else
                            {
                                sbResult.AppendLine("No Printers were found!");
                                txtResult.AppendText("No Printers were found!" + "\n");
                            }
                        }
                    }
                    else
                    {
                        sbResult.AppendLine("No Printers were found!");
                        txtResult.AppendText("No Printers were found!" + "\n");
                    }
                }
            }
            catch (Exception ex)
            {
                sbResult.AppendLine("No Printers were found!" + "\n" + ex.Message);
                txtResult.AppendText("No Printers were found!" + "\n");
                msgUser = "Cannot find printers or printer drivers are not installed properly. Please check the printer drivers and try again.";
                MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE06_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            #endregion

            try
            {
                bool status = false;
                #region - Virtual Printer check
                while (isXPSPrint)
                {
                    msgUser = "Please connect a printer, navigate to printer properties and set the driver to universal printer driver and set it as Default printer." + "\n" + "\n" +
                    "Once you are done, click OK to continue.";
                    DialogResult msgResult = MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE06_NAME, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                    if (msgResult == DialogResult.OK)
                    {
                        ManagementObjectSearcher printers2 = new ManagementObjectSearcher("SELECT * FROM Win32_Printer WHERE Default=TRUE");
                        if (printers2 != null)
                        {
                            if (printers2.Get() != null)
                            {
                                foreach (ManagementObject mo in printers2.Get())
                                {
                                    if (mo["DeviceID"] != null)
                                    {
                                        string deviceName = Convert.ToString(mo["DeviceID"]).ToUpper();
                                        isXPSPrint = deviceName.Contains("MICROSOFT");
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                #endregion

                #region - send print command
                if ((isctxDriver) && (isXPSPrint == false))
                {
                    this.Enabled = false;
                    this.WindowState = FormWindowState.Minimized;

                    #region - Print the Word Document

                    sampleFileFullPath = Globals.SampleFilesLocation;
                    sampleFileFullPath = sampleFileFullPath + Constants.SAMPLE_FILE_NAME_PRINT_DOC;
                    if (!System.IO.File.Exists(sampleFileFullPath))
                    {
                        status = false;
                        sbResult.AppendLine("ERROR: Printing Test Case - Sample Word Document File is missing: " + sampleFileFullPath);
                        txtResult.AppendText("ERROR: Printing Test Case - Sample Word Document File is missing: " + sampleFileFullPath + "\n");
                    }
                    else
                    {
                        ProcessStartInfo psStartInfo = new ProcessStartInfo();
                        psStartInfo.Arguments = @"" + "\"" + defaultPrinterDeviceID + "\"";
                        psStartInfo.FileName = @"" + "\"" + sampleFileFullPath + "\"";
                        psStartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                        psStartInfo.Verb = "print";
                        psStartInfo.CreateNoWindow = true;
                        psStartInfo.UseShellExecute = true;
                        Process ps = Process.Start(psStartInfo);

                        sbResult.AppendLine("Printing Test Case - Word Document Printed: " + sampleFileFullPath);
                        txtResult.AppendText("Printing Test Case - Word Document Printed: " + sampleFileFullPath + "\n");
                        status = true;
                    }

                    #endregion
                    isSavePaper = false;

                    #region - Print the PDF Document

                    sampleFileFullPath = Globals.SampleFilesLocation;
                    sampleFileFullPath = sampleFileFullPath + Constants.SAMPLE_FILE_NAME_PRINT_PDF;
                    if (!System.IO.File.Exists(sampleFileFullPath))
                    {
                        sbResult.AppendLine("ERROR: Printing Test Case - Sample PDF Document File is missing: " + sampleFileFullPath);
                        txtResult.AppendText("ERROR: Printing Test Case - Sample PDF Document File is missing: " + sampleFileFullPath + "\n");
                        status = false;
                    }
                    else
                    {
                        if (!isSavePaper)
                        {
                            ProcessStartInfo psStartInfo = new ProcessStartInfo();
                            psStartInfo.Arguments = @"" + "\"" + defaultPrinterDeviceID + "\"";
                            psStartInfo.FileName = @"" + "\"" + sampleFileFullPath + "\"";
                            psStartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                            psStartInfo.Verb = "print";
                            psStartInfo.CreateNoWindow = true;
                            psStartInfo.UseShellExecute = true;
                            Process ps = Process.Start(psStartInfo);
                        }

                        sbResult.AppendLine("Printing Test Case - PDF Document Printed: " + sampleFileFullPath);
                        txtResult.AppendText("Printing Test Case - PDF Document Printed: " + sampleFileFullPath + "\n");
                        status = true;
                    }

                    #endregion

                    #region - Print the Excel Document
                    sampleFileFullPath = Globals.SampleFilesLocation;
                    sampleFileFullPath = sampleFileFullPath + Constants.SAMPLE_FILE_NAME_PRINT_XLS;
                    if (!System.IO.File.Exists(sampleFileFullPath))
                    {
                        sbResult.AppendLine("ERROR: Printing Test Case - Sample Excel Document File is missing: " + sampleFileFullPath);
                        txtResult.AppendText("ERROR: Printing Test Case - Sample Excel Document File is missing: " + sampleFileFullPath + "\n");
                        status = false;
                    }
                    else
                    {
                        if (!isSavePaper)
                        {
                            ProcessStartInfo psStartInfo = new ProcessStartInfo();
                            psStartInfo.Arguments = @"" + "\"" + defaultPrinterDeviceID + "\"";
                            psStartInfo.FileName = @"" + "\"" + sampleFileFullPath + "\"";
                            psStartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                            psStartInfo.Verb = "print";
                            psStartInfo.CreateNoWindow = true;
                            psStartInfo.UseShellExecute = true;
                            Process ps = Process.Start(psStartInfo);
                        }

                        sbResult.AppendLine("Printing Test Case - Excel Document Printed: " + sampleFileFullPath);
                        txtResult.AppendText("Printing Test Case - Excel Document Printed: " + sampleFileFullPath + "\n");
                        status = true;
                    }
                    #endregion

                    #region - Print the PowerPoint Document
                    sampleFileFullPath = Globals.SampleFilesLocation;
                    sampleFileFullPath = sampleFileFullPath + Constants.SAMPLE_FILE_NAME_PRINT_PPT;
                    if (!System.IO.File.Exists(sampleFileFullPath))
                    {
                        sbResult.AppendLine("ERROR: Printing Test Case - Sample PowerPoint Document File is missing: " + sampleFileFullPath);
                        txtResult.AppendText("ERROR: Printing Test Case - Sample PowerPoint Document File is missing: " + sampleFileFullPath + "\n");
                        status = false;
                    }
                    else
                    {
                        if (!isSavePaper)
                        {
                            ProcessStartInfo psStartInfo = new ProcessStartInfo();
                            psStartInfo.Arguments = @"" + "\"" + defaultPrinterDeviceID + "\"";
                            psStartInfo.FileName = @"" + "\"" + sampleFileFullPath + "\"";
                            psStartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                            psStartInfo.Verb = "print";
                            psStartInfo.CreateNoWindow = true;
                            psStartInfo.UseShellExecute = true;
                            Process ps = Process.Start(psStartInfo);
                        }

                        sbResult.AppendLine("Printing Test Case - PowerPoint Document Printed: " + sampleFileFullPath);
                        txtResult.AppendText("Printing Test Case - PowerPoint Document Printed: " + sampleFileFullPath + "\n");
                        status = true;
                    }
                    #endregion

                    this.Refresh();
                }
                else
                {
                    status = false;
                }
                #endregion

                this.Enabled = true;
                this.WindowState = FormWindowState.Normal;

                if (status)
                {
                    TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_COMPLETED;
                    TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                    return Constants.VERIFICATION_STATUS_COMPLETED;
                }
                else
                {
                    TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;
                    TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                    return Constants.VERIFICATION_STATUS_ERROR;
                }
            }
            catch (Exception ex)
            {
                this.Enabled = true;
                this.WindowState = FormWindowState.Normal;
                sbResult.AppendLine("ERROR: Runtime Error in TC6_ClientPrintTest - Printing Test Case Failed - " + ex.Message);
                txtResult.AppendText("ERROR: Runtime Error in TC6_ClientPrintTest - Printing Test Case Failed - " + ex.Message + "\n");
                TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;

                return Constants.VERIFICATION_STATUS_ERROR;
            }
        }
        #endregion

        #region - Test Case # 7 - Common HDX Plug n Play Devices Redirection
        public string TC7_PlugPlayDeviceRedirection(byte TestCaseID)
        {



            string returnValue = Constants.VERIFICATION_STATUS_ERROR;
            string msgUser = "";
            StringBuilder sbResult = new StringBuilder("");

            ManagementObjectSearcher usbDevices = new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive WHERE InterfaceType='USB'");
            int connectedUsbCount = usbDevices.Get().Count;

            if (connectedUsbCount < 1)
            {
                PlatformID platform = Environment.OSVersion.Platform;
                if (platform.Equals(PlatformID.MacOSX) || platform.Equals(PlatformID.Unix))
                {
                    msgUser = "Please ensure that the USB device is properly plugged in and is connected locally" + "\n";
                    MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE07_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (platform.Equals(PlatformID.Win32NT) || platform.Equals(PlatformID.Win32S) || platform.Equals(PlatformID.Win32Windows) || platform.Equals(PlatformID.WinCE))
                {
                    msgUser = "1) Please connect a USB Drive to the Endpoint" + "\n" +
                          "2) Navigate to Citrix Toolbar -> Preferences" + "\n" +
                          "3) Click on “Switch to generic mode” for the connected USB drive" + "\n" +
                          "4) Click OK";
                    MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE01_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            try
            {
                txtResult.AppendText("\n");
                txtResult.AppendText("Running " + Constants.TESTCASE07_NAME + " Test Case - " + DateTime.Now.ToString() + "\n");

                ManagementObjectSearcher usbDevice = new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive WHERE InterfaceType='USB'");
                int connectedUsbCount2 = usbDevice.Get().Count;

                txtResult.AppendText("Total Number of USB Drives: " + Convert.ToString(connectedUsbCount2) + " " + "\n");
                sbResult.AppendLine("Total Number of USB Drives: " + Convert.ToString(connectedUsbCount2));


                if (Globals.ClientPlatformId == "51")
                {
                    returnValue = Constants.VERIFICATION_STATUS_COMPLETED;
                    TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_COMPLETED;
                    TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                }
                else
                {
                    if (connectedUsbCount2 > 0)
                    {
                        if (Globals.ClientPlatformId != "51")
                        {
                            foreach (ManagementObject mo in usbDevice.Get())
                            {
                                decimal dSize = Math.Round((Convert.ToDecimal(mo["Size"]) / 1073741824), 2); //HDD Size in Gb
                                txtResult.AppendText(mo["Model"] + " / " + Convert.ToString(dSize) + " GB (" + mo["InterfaceType"] + ")" + " " + "\n");
                                sbResult.AppendLine(mo["Model"] + " / " + Convert.ToString(dSize) + " GB (" + mo["InterfaceType"] + ")");
                            }
                        }

                        returnValue = Constants.VERIFICATION_STATUS_COMPLETED;
                        TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_COMPLETED;
                        TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                    }
                    else
                    {
                        returnValue = Constants.VERIFICATION_STATUS_ERROR;
                        TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;
                        TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                    }
                }

                return returnValue;
            }
            catch (Exception ex)
            {
                sbResult.AppendLine("ERROR in TC7_PlugPlayDeviceRedirection: " + ex.Message);
                txtResult.AppendText(sbResult + "\n");
                TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;

                return Constants.VERIFICATION_STATUS_ERROR;
            }
        }

        #endregion

        #region - Test Case #8 - HDX Multi Monitor Support
        public string TC8_HDXMultiMonitor(byte TestCaseID)
        {
            StringBuilder sbResult = new StringBuilder("");
            string sampleFileFullPathVLC = "";
            string sampleFileFullPathPPT = "";
            string msgUser = "";
            int cntScreen = 0;
            bool resultNP = false;
            bool resultMW = false;
            bool isHDXMonitoringToolAvailable = SysRequirements.oSysRequirements[Constants.TC_HDX_MONITOR_TOOL - 1].IsInstalled;
            if (!isHDXMonitoringToolAvailable) sbResult.AppendLine("HDX Monitor Tool is not available");

            txtResult.AppendText("\n");
            txtResult.AppendText("Running " + Constants.TESTCASE08_NAME + " Test Case - " + DateTime.Now.ToString() + "\n");

            Screen[] scr = Screen.AllScreens;
            cntScreen = scr.Count();
            if (!(cntScreen > 1))
            {
                msgUser = "Please ensure you have two monitors connected in Extended mode." + "\n" + "\n" +
                       "Once you are done, click OK to continue.";
                MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE08_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            if (cntScreen > 1)
            {


                string fpsTaskDataNotepad = "";

                #region - Moving Notepad

                try
                {


                    tokenSource = new CancellationTokenSource();
                    token = tokenSource.Token;
                    if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes")
                    {
                        Task fpsTaskForNotepad = Task.Factory.StartNew(() =>
                        fpsTaskDataNotepad = Utilities.CalculateFPS(TestCaseID, "Moving notepad across screens", Constants.FPS_INTERVAL, (Constants.FPS_CALCULATION_RUNTIME) / 1000, token), token);
                    }

                    ProcessStartInfo psStartInfo = new ProcessStartInfo();
                    psStartInfo.WindowStyle = ProcessWindowStyle.Normal;
                    psStartInfo.FileName = @"" + "\"" + Constants.APP_EXECUTABLE_NOTEPAD + "\"";
                    Process ps = Process.Start(psStartInfo);
                    Thread.Sleep(Constants.SAMPLE_FILE_RUNTIME_NOTEPAD);

                    IntPtr handle = ps.MainWindowHandle;
                    Screen[] screens = Screen.AllScreens;
                    int iCounter = 1;
                    int iScreenNumber = 0;
                    scrnCaptureTimer.Interval = 1000;
                    scrnCaptureTimer.AutoReset = false;
                    scrnCaptureTimer.Start();
                    scrnCaptureTimer.Elapsed += new System.Timers.ElapsedEventHandler(CaptureScreen);

                    while (iScreenNumber <= screens.Length)
                    {
                        if (iScreenNumber == 0)
                        {
                            MoveWindow(handle, screens[iScreenNumber].Bounds.Width / 2, 0, screens[iScreenNumber].Bounds.Width, screens[iScreenNumber].Bounds.Height, false);
                            this.Refresh();
                            Thread.Sleep(1000);
                            iScreenNumber++;
                            MoveWindow(handle, screens[iScreenNumber].Bounds.X, screens[iScreenNumber].Bounds.Y, screens[iScreenNumber].Bounds.Width, screens[iScreenNumber].Bounds.Height, false);
                            this.Refresh();
                            Thread.Sleep(1000);
                        }
                        else
                        {
                            MoveWindow(handle, screens[iScreenNumber].Bounds.Width / 2, 0, screens[iScreenNumber].Bounds.Width, screens[iScreenNumber].Bounds.Height, false);
                            this.Refresh();
                            Thread.Sleep(1000);
                            iScreenNumber--;
                            MoveWindow(handle, screens[iScreenNumber].Bounds.X, screens[iScreenNumber].Bounds.Y, screens[iScreenNumber].Bounds.Width, screens[iScreenNumber].Bounds.Height, false);
                            this.Refresh();
                            Thread.Sleep(1000);
                        }
                        iCounter++;
                        if (iCounter == 20)
                        {
                            break;
                        }
                    }
                    sbResult.AppendLine("Screenshot saved successfully and the path is " + Globals.screenshotName);
                    txtResult.AppendText("Screenshot saved successfully and the path is " + Globals.screenshotName + "\n");

                    if (!ps.HasExited)
                    {
                        ps.Kill();
                    }
                    if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes")
                    {
                        sbResult.AppendLine(fpsTaskDataNotepad);
                        txtResult.AppendText(fpsTaskDataNotepad);
                        sbResult.AppendLine("Scenario of Moving app across screens is completed.");
                        txtResult.AppendText("Scenario of Moving app across screens is completed." + "\n");
                        resultNP = true;
                    }
                    else
                    {
                        sbResult.AppendLine("HDX Monitor Tool has not been installed properly. FPS is not available.");
                        txtResult.AppendText("HDX Monitor Tool has not been installed properly. FPS is not available." + "\n");
                    }
                }
                catch(Exception ex)
                {

                    sbResult.AppendLine("ERROR: Runtime Error in TC8_HDXMultiMonitorSupport - " + ex.Message);
                    txtResult.AppendText("ERROR: Runtime Error in TC8_HDXMultiMonitorSupport -  " + ex.Message + "\n");

                }
                this.Refresh();

                #endregion

                #region - Opening multiple apps on different screens
                try
                {


                    sampleFileFullPathVLC = Globals.SampleFilesLocation;
                    sampleFileFullPathVLC = sampleFileFullPathVLC + Constants.SAMPLE_FILE_NAME_MOVIE_MP4;

                    sampleFileFullPathPPT = Globals.SampleFilesLocation;
                    sampleFileFullPathPPT = sampleFileFullPathPPT + Constants.SAMPLE_FILE_NAME_POWERPOINT;

                    if (!System.IO.File.Exists(sampleFileFullPathVLC) && !System.IO.File.Exists(sampleFileFullPathPPT))
                    {
                        sbResult.AppendLine("ERROR: Video (720p resolution) sample files is missing: " + sampleFileFullPathVLC + "\n" + sampleFileFullPathPPT);
                        txtResult.AppendText("ERROR: Video (720p resolution) sample files is missing: " + sampleFileFullPathVLC + "\n" + sampleFileFullPathPPT + "\n");
                    }
                    else
                    {
                        Screen[] sc = Screen.AllScreens;
                        string fpsTaskDataMultipleApp = "";
                        tokenSource = new CancellationTokenSource();
                        token = tokenSource.Token;
                        if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes")
                        {
                            Task fpsTaskForMultipleApp = Task.Factory.StartNew(() =>
                            fpsTaskDataMultipleApp = Utilities.CalculateFPS(TestCaseID, "Opening multiple apps across screens", Constants.FPS_INTERVAL, (Constants.FPS_CALCULATION_RUNTIME) / 1000, token), token);
                        }

                        ProcessStartInfo psStartInfoVLC = new ProcessStartInfo();
                        psStartInfoVLC.FileName = @"" + "\"" + Constants.APP_EXECUTABLE_VLCPLAYER + "\"";
                        psStartInfoVLC.Arguments = @" -f " + "\"" + sampleFileFullPathVLC + "\"";
                        psStartInfoVLC.WindowStyle = ProcessWindowStyle.Maximized;
                        Process psVLC = Process.Start(psStartInfoVLC);
                        Thread.Sleep(500);

                        ProcessStartInfo psStartInfoPPT = new ProcessStartInfo();
                        psStartInfoPPT.FileName = @"" + "\"" + Constants.APP_EXECUTABLE_POWERPOINT + "\"";
                        psStartInfoPPT.Arguments = @"/S " + "\"" + sampleFileFullPathPPT + "\"";
                        Process psPPT = Process.Start(psStartInfoPPT);
                        psPPT.WaitForInputIdle();

                        Thread.Sleep(Constants.SAMPLE_FILE_RUNTIME_NOTEPAD);

                        IntPtr handlePPT = psPPT.MainWindowHandle;
                        MoveWindow(handlePPT, sc[1].Bounds.X, sc[1].Bounds.Y, sc[1].Bounds.Width, sc[1].Bounds.Height, false);
                        scrnCaptureTimer.Interval = 15000;
                        scrnCaptureTimer.AutoReset = false;
                        scrnCaptureTimer.Start();
                        scrnCaptureTimer.Elapsed += new System.Timers.ElapsedEventHandler(CaptureScreen);

                        bool flag = Utilities.ProcessMonitor(psPPT, psVLC, Constants.SAMPLE_FILE_RUNTIME_MULTIWINDOW);

                        sbResult.AppendLine("Screenshot saved successfully and the path is " + Globals.screenshotName);
                        txtResult.AppendText("Screenshot saved successfully and the path is " + Globals.screenshotName + "\n");

                        sbResult.AppendLine("Video (720p resolution) sample file ran successfully on 1st screen: " + sampleFileFullPathVLC);
                        txtResult.AppendText("Video (720p resolution) sample file ran successfully on 1st screen: " + sampleFileFullPathVLC + "\n");

                        sbResult.AppendLine("PowerPoint sample file ran successfully on 2nd screen: " + sampleFileFullPathPPT);
                        txtResult.AppendText("PowerPoint sample file ran successfully on 2nd screen: " + sampleFileFullPathPPT + "\n");
                        if (flag == false)
                        {
                            sbResult.AppendLine("MultiMonitor Apps terminated by User.");
                            txtResult.AppendText("MultiMonitor Apps terminated by User." + "\n");
                        }
                        if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes" && flag)
                        {
                            sbResult.AppendLine(fpsTaskDataMultipleApp);
                            txtResult.AppendText(fpsTaskDataMultipleApp);
                            sbResult.AppendLine("HDX MultiMonitor Support Verification completed sucessfully");
                            txtResult.AppendText("HDX MultiMonitor Support Verification completed sucessfully" + "\n");
                            resultMW = true;
                        }
                        else
                        {
                            sbResult.AppendLine("HDX Monitor Tool has not been installed properly. FPS is not available.");
                            txtResult.AppendText("HDX Monitor Tool has not been installed properly. FPS is not available." + "\n");
                        }
                    }
                }
                catch(Exception ex)
                {
                    sbResult.AppendLine("ERROR: Runtime Error in TC8_HDXMultiMonitorSupport - " + ex.Message);
                    txtResult.AppendText("ERROR: Runtime Error in TC8_HDXMultiMonitorSupport -  " + ex.Message + "\n");

                }
                #endregion

                if (resultNP && resultMW)
                {
                    TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_COMPLETED;
                    TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                    this.Refresh();
                    return Constants.VERIFICATION_STATUS_COMPLETED;
                }
                else
                {
                    sbResult.AppendLine("ERROR: TC8_HDXMultiMonitor Failed and FPS Unavailable");
                    txtResult.AppendText("ERROR: TC8_HDXMultiMonitor Failed and FPS Unavailable");
                    TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;

                    return Constants.VERIFICATION_STATUS_ERROR;
                }
            }
            else
            {
                msgUser = "You don't seem to have multi-monitor connection.  This test case will be skipped.";
                MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);
                sbResult.AppendLine("INFO: You don't seem to have multi-monitor connection.  This test case will be skipped.");
                txtResult.AppendText("INFO: You don't seem to have multi-monitor connection.  This test case will be skipped." + "\n");
                TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;
                TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                return Constants.VERIFICATION_STATUS_ERROR;
            }
        }


        #endregion




       

      
        #endregion

        #region - HDX 3D Pro Test Cases

        #region - Test Case #9 - 3D Mouse Support
        public string TC9_3DMouseSupport(byte TestCaseID)
        {
            string returnValue = Constants.VERIFICATION_STATUS_ERROR;
            string msgUser = "";
            StringBuilder sbResult = new StringBuilder("");
            tokenSource = new CancellationTokenSource();
            token = tokenSource.Token;

            msgUser = "Please connect a 3D Mouse, navigate to ‘Devices’ on Citrix Receiver and click on ‘Switch to generic’ for the connected 3D Mouse to redirect it to the session" + "\n" +
                "Use it for 2 minutes on Google Earth application, once it is launched automatically.";
            MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE09_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);

            txtResult.AppendText("\n");
            txtResult.AppendText("Running " + Constants.TESTCASE09_NAME + " Test Case - " + DateTime.Now.ToString() + "\n");

            StreamWriter fileFPSGoogleEarthStart = new StreamWriter(Constants.LOG_FILE_NAME, true);
            fileFPSGoogleEarthStart.WriteLine("START - 3D Mouse - Google Earth rotation - " + DateTime.Now.ToString());
            fileFPSGoogleEarthStart.Close();

            try
            {
                #region - Google Earth App

                string fpsThreadDataGoogleEarth = "";

                if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes")
                {
                    Task fpsTaskForGoogleEarth = Task.Factory.StartNew(() =>
                    fpsThreadDataGoogleEarth = Utilities.CalculateFPS(TestCaseID, "3D Mouse - Google Earth", Constants.FPS_INTERVAL, (Constants.FPS_CALCULATION_RUNTIME) / 1000, token), token);
                }

                ProcessStartInfo info = new ProcessStartInfo(Constants.APP_EXECUTABLE_GOOGLEEARTH);
                info.WindowStyle = ProcessWindowStyle.Maximized;
                Process ps = Process.Start(info);
                this.Enabled = false;
                this.WindowState = FormWindowState.Minimized;

                Thread.Sleep(10000);

                msgUser = "Please navigate to Google Earth Tools -> Options -> Navigation tab and ensure the “Enable controller” checkbox is checked prior using the 3D mouse.";
                MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE09_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);

                Thread.Sleep(5000);

                //SendKeys.Send("%{t}");
                //SendKeys.Send("{UP}{ENTER}");
                //SendKeys.Send("{RIGHT}{RIGHT}{RIGHT}");
                //SendKeys.Send("{TAB}{TAB}{TAB}{TAB}{TAB}{TAB}{TAB}{TAB}{TAB}");
                //SendKeys.Send(" ");
                //SendKeys.Send("{ENTER}");
                //SendKeys.Send("{F11}");

                timerFrm = new frmTimer(Constants.SAMPLE_FILE_RUNTIME_GOOGLEEARTH * 4, 19);
                timerFrm.ShowDialog();

                if (!ps.HasExited)
                {
                    ps.Kill();
                }
                this.Enabled = true;
                tokenSource.Cancel();
                if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes")
                {
                    sbResult.AppendLine(fpsThreadDataGoogleEarth);
                    txtResult.AppendText(fpsThreadDataGoogleEarth);

                    sbResult.AppendLine("3D Mouse Support tested successfully " + "\n");
                    txtResult.AppendText("3D Mouse Support tested successfully " + "\n");

                    returnValue = Constants.VERIFICATION_STATUS_COMPLETED;
                }
                else
                {
                    sbResult.AppendLine("HDX Monitor Tool has not been installed properly. FPS is not available.");
                    txtResult.AppendText("HDX Monitor Tool has not been installed properly. FPS is not available." + "\n");
                }

                #endregion
            }

            catch (Exception ex)
            {
                returnValue = Constants.VERIFICATION_STATUS_ERROR;
                sbResult.AppendLine("ERROR in TC9_3DMouseSupport: " + ex.Message);
                txtResult.AppendText(sbResult + "\n");
                TestCases.oTestCases[TestCaseID - 1].Status = returnValue;
            }

            StreamWriter fileFPSGoogleEarthEnd = new StreamWriter(Constants.LOG_FILE_NAME, true);
            fileFPSGoogleEarthEnd.WriteLine("END - 3D Mouse - Google Earth rotation - " + DateTime.Now.ToString());
            fileFPSGoogleEarthEnd.Close();

            Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);

            TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
            TestCases.oTestCases[TestCaseID - 1].Status = returnValue;
            return returnValue;
        }
        #endregion

        #region - Test Case #10 - HDX Multi Monitor Support
        public string TC10_HDXMultiMonitorSupport(byte TestCaseID)
        {
            StringBuilder sbResult = new StringBuilder("");
            string sampleFileFullPath = "";
            string msgUser = "";

            txtResult.AppendText("\n");
            txtResult.AppendText("Running " + Constants.TESTCASE10_NAME + " Test Case - " + DateTime.Now.ToString() + "\n");

            Screen[] screens = Screen.AllScreens;

            if (!(screens.Count() > 1))
            {
                msgUser = "Please ensure you have two monitors connected in Extended mode." + "\n" + "\n" +
                          "Once you are done, click OK to continue.";
                MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE10_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            Screen[] screensNew = Screen.AllScreens;
            if (screensNew.Count() > 1)
            {
                #region - Open different windows on multiple screens
                try
                {
                    sampleFileFullPath = Globals.SampleFilesLocation;
                    sampleFileFullPath = sampleFileFullPath + Constants.APP_EXECUTABLE_NEHE;

                    if (!System.IO.File.Exists(sampleFileFullPath) && !System.IO.File.Exists(Constants.APP_EXECUTABLE_GOOGLEEARTH))
                    {
                        sbResult.AppendLine("ERROR: NEHE app is missing: " + sampleFileFullPath);
                        txtResult.AppendText("ERROR: NEHE app is missing: " + sampleFileFullPath + "\n");
                    }
                    else
                    {
                        Screen[] sc = Screen.AllScreens;
                        ProcessStartInfo processInfo = new ProcessStartInfo();
                        processInfo.FileName = sampleFileFullPath;
                        processInfo.ErrorDialog = true;
                        processInfo.UseShellExecute = false;
                        processInfo.RedirectStandardOutput = true;
                        processInfo.RedirectStandardError = true;
                        processInfo.WorkingDirectory = Path.GetDirectoryName(sampleFileFullPath);
                        Process psNEHE = Process.Start(processInfo);

                        this.WindowState = FormWindowState.Minimized;
                        Thread.Sleep(Constants.DUMMY_RUNTIME);
                        SendKeys.Send("N");
                        Thread.Sleep(Constants.DUMMY_RUNTIME);

                        IntPtr handleNEHE = psNEHE.MainWindowHandle;
                        MoveWindow(handleNEHE, 0, 0, sc[0].Bounds.Width, sc[0].Bounds.Height, false);

                        sbResult.AppendLine("NEHE ran successfully on 1st screen: " + sampleFileFullPath);
                        txtResult.AppendText("NEHE ran successfully on 1st screen: " + sampleFileFullPath + "\n");

                        sampleFileFullPath = Globals.SampleFilesLocation;
                        sampleFileFullPath = Constants.APP_EXECUTABLE_GOOGLEEARTH;

                        Screen[] scr = Screen.AllScreens;
                        ProcessStartInfo psInfoGoogleEarth = new ProcessStartInfo(sampleFileFullPath);
                        psInfoGoogleEarth.WindowStyle = ProcessWindowStyle.Maximized;
                        Process psGE = Process.Start(psInfoGoogleEarth);
                        psGE.WaitForInputIdle();
                        Thread.Sleep(Constants.WaitRunTime);
                        this.Enabled = false;

                        IntPtr handleGoogleEarth = psGE.MainWindowHandle;

                        Thread.Sleep(1000);
                        MoveWindow(handleGoogleEarth, scr[0].Bounds.Width / 2, 0, scr[0].Bounds.Width, scr[0].Bounds.Height, false);
                        Thread.Sleep(500);
                        MoveWindow(handleGoogleEarth, scr[1].Bounds.X, scr[1].Bounds.Y, scr[1].Bounds.Width, scr[1].Bounds.Height, false);

                        Thread.Sleep(Constants.WaitRunTime + Constants.DUMMY_RUNTIME);
                        SendKeys.Send("{F11}");
                        Thread.Sleep(Constants.SAMPLE_FILE_RUNTIME_3DPRO_TESTS / 2);

                        scrnCaptureTimer.Interval = 10000;
                        scrnCaptureTimer.AutoReset = false;
                        scrnCaptureTimer.Start();
                        scrnCaptureTimer.Elapsed += new System.Timers.ElapsedEventHandler(CaptureScreen);

                        //Scrolling Right
                        keybd_event(VK_RIGHT, 0, 0, 0);
                        bool flag = Utilities.ProcessMonitor(psGE, Constants.SAMPLE_FILE_RUNTIME_GOOGLEEARTH, true);
                        keybd_event(VK_RIGHT, 0, KEYEVENTF_KEYUP, 0);

                        //Scrolling Down
                        if (flag)
                        {
                            keybd_event(VK_DOWN, 0, 0, 0);
                            flag = Utilities.ProcessMonitor(psGE, Constants.SAMPLE_FILE_RUNTIME_GOOGLEEARTH, true);
                            keybd_event(VK_DOWN, 0, KEYEVENTF_KEYUP, 0);
                        }

                        Thread.Sleep(Constants.DUMMY_RUNTIME);

                        if (!psGE.HasExited)
                        {
                            psGE.Kill();
                        }
                        this.Enabled = true;

                        sbResult.AppendLine("Google Earth ran successfully on 2nd screen: " + sampleFileFullPath);
                        txtResult.AppendText("Google Earth ran successfully on 2nd screen: " + sampleFileFullPath + "\n");

                        sbResult.AppendLine("Screenshot saved successfully and the path is " + Globals.screenshotName);
                        txtResult.AppendText("Screenshot saved successfully and the path is " + Globals.screenshotName + "\n");

                        if (!psNEHE.HasExited)
                        {
                            psNEHE.Kill();
                        }
                        this.WindowState = FormWindowState.Normal;
                    }

                    TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_COMPLETED;
                    TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                    this.Refresh();

                    return Constants.VERIFICATION_STATUS_COMPLETED;

                }
                catch (Exception ex)
                {
                    sbResult.AppendLine("ERROR: Runtime Error in TC10_HDXMultiMonitor - Open different windows on multiple screens failed - " + ex.Message);
                    txtResult.AppendText("ERROR: Runtime Error in TC10_HDXMultiMonitor - Open different windows on multiple screens failed - " + ex.Message + "\n");
                    TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;

                    return Constants.VERIFICATION_STATUS_ERROR;
                }

                #endregion

            }
            else
            {
                msgUser = "You don't seem to have multi-monitor connection.  This test case will be skipped.";
                MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE10_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);

                sbResult.AppendLine("INFO: You don't seem to have multi-monitor connection.  This test case will be skipped.");
                txtResult.AppendText("INFO: You don't seem to have multi-monitor connection.  This test case will be skipped." + "\n");
                TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;
                TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                return Constants.VERIFICATION_STATUS_ERROR;
            }
        }
        #endregion

        #region - Test Case #11 - HDX 3D Pro Rich Graphics Apps Support
        public string TC11_HDX3DProRichGraphicsAppsSupport(byte TestCaseID)
        {
            string returnValue = Constants.VERIFICATION_STATUS_ERROR;
            Boolean resultGooglEarth = false; Boolean result3DXMLPlayer = false;
            Boolean resultUnigine = false;
            string msgUser = "";
            StringBuilder sbResult = new StringBuilder("");
            string sampleFileFullPath = "";

            txtResult.AppendText("\n");
            txtResult.AppendText("Running " + Constants.TESTCASE11_NAME + " Test Case - " + DateTime.Now.ToString() + "\n");

            #region - Pro Rich Graphics Apps Support

            #region - Check screen resolution

            //msgUser = "From Display Setting, set the best resolution of the endpoint." +
            //    "\n\nOnce you are done, click OK to continue.";
            //MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);

            int iHeight = 0; int iWidth = 0;
            ManagementObjectSearcher displays = new ManagementObjectSearcher("SELECT * FROM Win32_VideoController");
            foreach (ManagementObject mo in displays.Get())
            {
                if (mo["CurrentVerticalResolution"] != null) iHeight = Convert.ToInt32(mo["CurrentVerticalResolution"]);
                if (mo["CurrentHorizontalResolution"] != null) iWidth = Convert.ToInt32(mo["CurrentHorizontalResolution"]);
            }
            sbResult.AppendLine("Current Display Resolution: " + iWidth.ToString() + " x " + iHeight.ToString());
            txtResult.AppendText("Current Display Resolution: " + iWidth.ToString() + " x " + iHeight.ToString() + "\n");

            #endregion

            try
            {
                sampleFileFullPath = Globals.SampleFilesLocation;
                sampleFileFullPath = sampleFileFullPath + Constants.SAMPLE_FILE_NAME_3D_XML;

                MouseClickFilter filter = new MouseClickFilter();
                Application.AddMessageFilter(filter);

                #region  - Google Earth
                string fpsThreadDataProRichGraphics = "";
                tokenSource = new CancellationTokenSource();
                token = tokenSource.Token;
                if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes")
                {
                    Task fpsTaskForProRichGraphics = Task.Factory.StartNew(() =>
                    fpsThreadDataProRichGraphics = Utilities.CalculateFPS(TestCaseID, "3D Rich Graphics App Support - Google Earth", Constants.FPS_INTERVAL, (Constants.FPS_CALCULATION_RUNTIME) / 1000, token), token);
                }
                if (!System.IO.File.Exists(Constants.APP_EXECUTABLE_GOOGLEEARTH))
                {
                    sbResult.AppendLine("Error file missing at:" + Constants.APP_EXECUTABLE_GOOGLEEARTH);
                    txtResult.AppendText("Error file missing at:" + Constants.APP_EXECUTABLE_GOOGLEEARTH + "\n");
                }
                else
                {
                    ProcessStartInfo info = new ProcessStartInfo(Constants.APP_EXECUTABLE_GOOGLEEARTH);
                    info.WindowStyle = ProcessWindowStyle.Maximized;
                    Process ps = Process.Start(info);

                    Thread.Sleep(Constants.DUMMY_RUNTIME * 2);

                    SendKeys.Send("{F11}");
                    bool flag = Utilities.ProcessMonitor(ps, Constants.SAMPLE_FILE_RUNTIME_3DPRO_TESTS, true);

                    //Scrolling Right
                    if (flag)
                    {
                        keybd_event(VK_RIGHT, 0, 0, 0);
                        flag = Utilities.ProcessMonitor(ps, Constants.SAMPLE_FILE_RUNTIME_GOOGLEEARTH, true);
                        keybd_event(VK_RIGHT, 0, KEYEVENTF_KEYUP, 0);
                    }

                    if (flag)
                    {
                        //Scrolling Down
                        keybd_event(VK_DOWN, 0, 0, 0);
                        flag = Utilities.ProcessMonitor(ps, Constants.SAMPLE_FILE_RUNTIME_GOOGLEEARTH, true);
                        keybd_event(VK_DOWN, 0, KEYEVENTF_KEYUP, 0);
                    }
                    Thread.Sleep(Constants.DUMMY_RUNTIME);

                    if (!ps.HasExited)
                    {
                        ps.Kill();
                    }
                    if (!flag)
                    {
                        tokenSource.Cancel();
                        sbResult.AppendLine("Google Earth abruptly closed by User.");
                        txtResult.AppendText("Google Earth abruptly closed by User." + "\n");
                    }

                    if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes" && flag)
                    {
                        sbResult.AppendLine(fpsThreadDataProRichGraphics);
                        txtResult.AppendText(fpsThreadDataProRichGraphics);
                        sbResult.AppendLine("Google Earth App support test completed " + "\n");
                        txtResult.AppendText("Google Earth App support test completed " + "\n");
                        resultGooglEarth = true;
                    }
                    else
                    {
                        sbResult.AppendLine("Unable to calculate FPS using HDX Monitor.");
                        txtResult.AppendText("Unable to calculate FPS using HDX Monitor." + "\n");
                    }
                }

                #endregion

                #region - 3D-XML Player App
                string fpsThreadData3DXMLPlayer = "";
                tokenSource = new CancellationTokenSource();
                token = tokenSource.Token;
                if (!System.IO.File.Exists(sampleFileFullPath))
                {
                    sbResult.AppendLine("ERROR: 3D XML Sample File is missing: " + sampleFileFullPath);
                    txtResult.AppendText("ERROR: 3D XML Sample File is missing: " + sampleFileFullPath + "\n");
                }
                else
                {
                    if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes")
                    {
                        Task fpsTaskFor3DXMLPlayer = Task.Factory.StartNew(() =>
                        fpsThreadData3DXMLPlayer = Utilities.CalculateFPS(TestCaseID, "3D Rich Graphics App Support - 3D XML Player", Constants.FPS_INTERVAL_FOR_3D_XML_PLAYER, (Constants.FPS_CALCULATION_RUNTIME) / 1000, token), token);
                    }

                    ProcessStartInfo psInfo = new ProcessStartInfo(sampleFileFullPath);
                    psInfo.WindowStyle = ProcessWindowStyle.Maximized;
                    msgUser = "Please follow the below steps for this test case " + "\n" +
                        "1) Select a part of the Tractor 3D Object" + "\n" +
                        "2) Click on Play button" + "\n" +
                        "3) Tractor 3D Object will be available for 90seconds for you to perform the interactive opeations" + "\n" +
                        "\n\nOnce you are ready, click OK to open 3D XML Player.";
                    MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE11_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Process psI = Process.Start(psInfo);

                    bool flag = Utilities.ProcessMonitor(psI, Constants.SAMPLE_FILE_RUNTIME_3DXML + 8000);

                    if (!psI.HasExited)
                    {
                        psI.Kill();
                    }
                    if (flag == false)
                    {
                        tokenSource.Cancel();
                        sbResult.AppendLine("3D XML Player closed by User abruptly.");
                        txtResult.AppendText("3D XML Player closed by User abruptly." + "\n");
                        result3DXMLPlayer = false;
                    }
                    if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes" && flag)
                    {
                        sbResult.AppendLine(fpsThreadData3DXMLPlayer);
                        txtResult.AppendText(fpsThreadData3DXMLPlayer);
                        sbResult.AppendLine("3D XML Player App support test completed " + "\n");
                        txtResult.AppendText("3D XML Player App support test completed " + "\n");
                        result3DXMLPlayer = true;
                    }
                    else
                    {
                        sbResult.AppendLine("Unable to calculate FPS using HDX Monitor.");
                        txtResult.AppendText("Unable to calculate FPS using HDX Monitor." + "\n");
                    }
                }
                #endregion  

                #region - Unigine Tool
                string fpsThreadDataUnigine = "";
                tokenSource = new CancellationTokenSource();
                token = tokenSource.Token;
                if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes")
                {
                    Task fpsTaskUnigine = Task.Factory.StartNew(() =>
                    fpsThreadDataUnigine = Utilities.CalculateFPS(TestCaseID, "3D Rich Graphics App Support - Unigine", Constants.FPS_INTERVAL, (Constants.FPS_CALCULATION_RUNTIME) / 1000, token), token);
                }

                if (!System.IO.File.Exists(Constants.APP_EXECUTABLE_UNIGINE))
                {
                    sbResult.AppendLine("ERROR: Unigine Application is not available on this system: " + Constants.APP_EXECUTABLE_UNIGINE);
                    txtResult.AppendText("ERROR: Unigine Application is not available on this system: " + Constants.APP_EXECUTABLE_UNIGINE + "\n");
                }
                else
                {
                    msgUser = "Click the 'Run' button after Unigine is launched.\n" +
                        "Do not terminate Unigine abruptly.";
                    MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE11_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    ProcessStartInfo psiUnigine = new ProcessStartInfo();
                    psiUnigine.ErrorDialog = true;
                    psiUnigine.UseShellExecute = false;
                    psiUnigine.RedirectStandardOutput = true;
                    psiUnigine.RedirectStandardError = true;
                    psiUnigine.FileName = @"" + Constants.APP_EXECUTABLE_UNIGINE;
                    psiUnigine.WorkingDirectory = Path.GetDirectoryName(Constants.APP_EXECUTABLE_UNIGINE);

                    Process psUnigine = Process.Start(psiUnigine);
                    bool flag = Utilities.ProcessMonitorUnigine(Constants.SAMPLE_FILE_RUNTIME_UNIGINE);

                    Utilities.killUnigine();
                    if (flag == false)
                    {
                        tokenSource.Cancel();
                    }

                    if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes")
                    {
                        sbResult.AppendLine(fpsThreadDataUnigine);
                        txtResult.AppendText(fpsThreadDataUnigine);
                        sbResult.AppendLine("Unigine Application test completed " + "\n");
                        txtResult.AppendText("Unigine Application test completed " + "\n");
                        resultUnigine = true;
                    }
                    else
                    {
                        sbResult.AppendLine("Unable to calculate FPS using HDX Monitor.");
                        txtResult.AppendText("Unable to calculate FPS using HDX Monitor." + "\n");
                    }
                }

                #endregion

                Application.RemoveMessageFilter(filter);

                sbResult.AppendLine("3D Pro Rich Graphics Apps support test completed " + "\n");
                txtResult.AppendText("3D Pro Rich Graphics Apps support test completed " + "\n");

                this.Refresh();
            }

            catch (Exception ex)
            {
                returnValue = Constants.VERIFICATION_STATUS_ERROR;

                sbResult.AppendLine("ERROR: Runtime Error in TC11_HDX3DProRichGraphicsAppsSupport - " + ex.Message);
                txtResult.AppendText("ERROR: Runtime Error in TC11_HDX3DProRichGraphicsAppsSupport - " + ex.Message + "\n");
            }


            this.Refresh();

            TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();

            if (resultGooglEarth && result3DXMLPlayer && resultUnigine) returnValue = Constants.VERIFICATION_STATUS_COMPLETED;
            TestCases.oTestCases[TestCaseID - 1].Status = returnValue;

            tokenSource.Dispose();

            Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);

            return returnValue;
            #endregion
        }
        #endregion

        #region - Test Case #12 - Pixel Perfect Lossless Compression
        public string TC12_PixelPerfectLosslessCompression(byte TestCaseID)
        {
            string returnValue = Constants.VERIFICATION_STATUS_ERROR;
            Boolean resultGoogleEarth = false; Boolean result3DXMLPlayer = false; Boolean resultITKSnap = false;
            string msgUser = "";
            StringBuilder sbResult = new StringBuilder("");
            string sampleFileFullPath = "";
            txtResult.AppendText("\n");
            txtResult.AppendText("Running " + Constants.TESTCASE12_NAME + " Test Case - " + DateTime.Now.ToString() + "\n");

            #region - 3D Pro Pixel Perfect Lossless Compression

            msgUser = "Right click on the Citrix HDX 3D Pro Lossless Indicator in the Notification area and select \"Lossless Switch\"";
            MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE12_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);

            #region - Check screen resolution

            msgUser = "From Display Setting, set the best resolution of the endpoint." +
                "\n\nOnce you are done, click OK to continue.";
            //MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);

            int iHeight = 0; int iWidth = 0;
            ManagementObjectSearcher displays = new ManagementObjectSearcher("SELECT * FROM Win32_VideoController");
            foreach (ManagementObject mo in displays.Get())
            {
                if (mo["CurrentVerticalResolution"] != null) iHeight = Convert.ToInt32(mo["CurrentVerticalResolution"]);
                if (mo["CurrentHorizontalResolution"] != null) iWidth = Convert.ToInt32(mo["CurrentHorizontalResolution"]);
            }
            sbResult.AppendLine("Current Display Resolution: " + iWidth.ToString() + " x " + iHeight.ToString());
            txtResult.AppendText("Current Display Resolution: " + iWidth.ToString() + " x " + iHeight.ToString() + "\n");

            #endregion

            try
            {
                sampleFileFullPath = Globals.SampleFilesLocation;

                #region - ITK-Snap Test
                sampleFileFullPath = sampleFileFullPath + Constants.SAMPLE_FILE_NAME_ITKSNAP_IMAGE;
                if (!System.IO.File.Exists(sampleFileFullPath))
                {
                    sbResult.AppendLine("ERROR: ITK-Snap Sample File is missing: " + sampleFileFullPath);
                    txtResult.AppendText("ERROR: ITK-Snap Sample File is missing: " + sampleFileFullPath + "\n");
                }
                else
                {
                    tokenSource = new CancellationTokenSource();
                    token = tokenSource.Token;
                    string fpsTaskDataITKSnap = "";
                    if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes")
                    {
                        Task fpsTaskForITKSnap = Task.Factory.StartNew(() =>
                        fpsTaskDataITKSnap = Utilities.CalculateFPS(TestCaseID, "3D Pro Pixel Perfect Lossless Compression - ITK-SNAP", Constants.FPS_INTERVAL, (Constants.FPS_CALCULATION_RUNTIME) / 1000, token), token);
                    }

                    ProcessStartInfo procStart = new ProcessStartInfo(Constants.APP_EXECUTABLE_ITKSNAP);
                    //if (System.IO.File.Exists(Constants.APP_EXECUTABLE_ITKSNAP64))
                    //{
                    //    procStart.FileName =  Constants.APP_EXECUTABLE_ITKSNAP64 ;
                    //}
                    //else
                    //{
                    //    procStart.FileName = @"" + "\"" + Constants.APP_EXECUTABLE_ITKSNAP32 + "\"";
                    //    //procStart.FileName = Constants.APP_EXECUTABLE_ITKSNAP32 ;
                    //}
                    procStart.FileName = sampleFileFullPath;
                    procStart.WindowStyle = ProcessWindowStyle.Maximized;
                    msgUser = "Please follow the below steps for this test case: " + "\n" +
                        "1) Ensure MRI image(.gipl) file uses default application ITK-Snap" + "\n" +
                        "2) Perform interactive operations like Pan, Zoom, Rotate etc" + "\n" +
                        "3) ITK Snap will run for 2 minutes" + "\n" +
                        "\n\nOnce you are ready, click OK to open ITK Snap.";
                    MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE12_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    Process p = Process.Start(procStart);
                    this.Enabled = false;
                    this.WindowState = FormWindowState.Minimized;

                    frmTimer timerFrmITK = new frmTimer(Constants.SAMPLE_FILE_RUNTIME_ITKSNAP, 12);
                    timerFrmITK.ShowDialog();
                    //bool flag = Utilities.ProcessMonitor(p, Constants.SAMPLE_FILE_RUNTIME_ITKSNAP + 2000);

                    if (!p.HasExited)
                    {
                        p.Kill();
                    }
                    this.Enabled = true;
                    this.WindowState = FormWindowState.Normal;

                    if (Globals.isITKSnapClosed)
                    {
                        tokenSource.Cancel();
                        sbResult.AppendLine("ITK Snap closed by User abruptly.");
                        txtResult.AppendText("ITK Snap closed by User abruptly." + "\n");
                    }

                    if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes" && !Globals.isITKSnapClosed)
                    {
                        sbResult.AppendLine(fpsTaskDataITKSnap);
                        txtResult.AppendText(fpsTaskDataITKSnap);
                        sbResult.AppendLine("ITK Snap application tested successfully.");
                        txtResult.AppendText("ITK Snap application tested successfully.");
                        resultITKSnap = true;
                    }
                    else
                    {
                        sbResult.AppendLine("Unable to calculate FPS using HDX Monitor.");
                        txtResult.AppendText("Unable to calculate FPS using HDX Monitor." + "\n");
                    }
                }
                #endregion

                #region  - Google Earth
                string fpsTaskDataGoogleEarth = "";
                tokenSource = new CancellationTokenSource();
                token = tokenSource.Token;
                if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes")
                {
                    Task fpsTaskForGoogleEarth = Task.Factory.StartNew(() =>
                    fpsTaskDataGoogleEarth = Utilities.CalculateFPS(TestCaseID, "3D Pro Pixel Perfect Lossless Compression - Google Earth", Constants.FPS_INTERVAL, (Constants.FPS_CALCULATION_RUNTIME) / 1000, token), token);
                }
                if (!System.IO.File.Exists(Constants.APP_EXECUTABLE_GOOGLEEARTH))
                {
                    sbResult.AppendLine("Error: File missing at:" + Constants.APP_EXECUTABLE_GOOGLEEARTH);
                    txtResult.AppendText("Error: File missing at:" + Constants.APP_EXECUTABLE_GOOGLEEARTH + "\n");
                }
                else
                {
                    ProcessStartInfo info = new ProcessStartInfo(Constants.APP_EXECUTABLE_GOOGLEEARTH);
                    info.WindowStyle = ProcessWindowStyle.Maximized;
                    Process ps = Process.Start(info);

                    this.Enabled = false;
                    MouseClickFilter filter = new MouseClickFilter();
                    Application.AddMessageFilter(filter);

                    Thread.Sleep(Constants.DUMMY_RUNTIME * 2);
                    SendKeys.Send("{F11}");
                    bool flag = Utilities.ProcessMonitor(ps, Constants.SAMPLE_FILE_RUNTIME_3DPRO_TESTS, true);

                    //Scrolling Right
                    if (flag)
                    {
                        keybd_event(VK_RIGHT, 0, 0, 0);
                        flag = Utilities.ProcessMonitor(ps, Constants.SAMPLE_FILE_RUNTIME_GOOGLEEARTH, true);
                        keybd_event(VK_RIGHT, 0, KEYEVENTF_KEYUP, 0);
                    }

                    if (flag)
                    {
                        //Scrolling Down
                        keybd_event(VK_DOWN, 0, 0, 0);
                        flag = Utilities.ProcessMonitor(ps, Constants.SAMPLE_FILE_RUNTIME_GOOGLEEARTH, true);
                        keybd_event(VK_DOWN, 0, KEYEVENTF_KEYUP, 0);
                    }

                    if (!ps.HasExited)
                    {
                        ps.Kill();
                    }
                    if (!flag)
                    {
                        tokenSource.Cancel();
                        sbResult.AppendLine("\n" + "Google Earth abruptly closed by User." + "\n");
                        txtResult.AppendText("Google Earth abruptly closed by User." + "\n");
                    }
                    this.Enabled = true;
                    Application.RemoveMessageFilter(filter);

                    if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes" && flag)
                    {
                        sbResult.AppendLine(fpsTaskDataGoogleEarth);
                        txtResult.AppendText(fpsTaskDataGoogleEarth);
                        sbResult.AppendLine("Google Earth App support test completed " + "\n");
                        txtResult.AppendText("Google Earth App support test completed " + "\n");
                        resultGoogleEarth = true;
                    }
                    else
                    {
                        sbResult.AppendLine("Unable to calculate FPS using HDX Monitor." + "\n");
                        txtResult.AppendText("Unable to calculate FPS using HDX Monitor." + "\n");
                    }
                }

                #endregion

                #region - 3D-XML Player App
                string fpsThreadData3DXMLPlayer = "";
                tokenSource = new CancellationTokenSource();
                token = tokenSource.Token;
                sampleFileFullPath = Globals.SampleFilesLocation;
                sampleFileFullPath = sampleFileFullPath + Constants.SAMPLE_FILE_NAME_3D_XML;
                if (!System.IO.File.Exists(sampleFileFullPath))
                {
                    sbResult.AppendLine("ERROR: 3D XML Sample File is missing: " + sampleFileFullPath);
                    txtResult.AppendText("ERROR: 3D XML Sample File is missing: " + sampleFileFullPath + "\n");
                }
                else
                {
                    if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes")
                    {
                        Task fpsTaskFor3DXMLPlayer = Task.Factory.StartNew(() =>
                        fpsThreadData3DXMLPlayer = Utilities.CalculateFPS(TestCaseID, "3D Pro Pixel Perfect Lossless Compression - 3D XML Player", Constants.FPS_INTERVAL_FOR_3D_XML_PLAYER, (Constants.FPS_CALCULATION_RUNTIME) / 1000, token), token);
                    }

                    ProcessStartInfo psInfo = new ProcessStartInfo(sampleFileFullPath);
                    psInfo.WindowStyle = ProcessWindowStyle.Maximized;
                    msgUser = "Please follow the below steps for this test case " + "\n" +
                        "1) Select a part of the Tractor 3D Object" + "\n" +
                        "2) Click on Play button" + "\n" +
                        "3) Tractor 3D Object will be available for 2 minutes for you to perform the interactive opeations" + "\n" +
                        "\n\nOnce you are ready, click OK to open 3D XML Player.";
                    MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE12_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    Process psI = Process.Start(psInfo);

                    bool flag = Utilities.ProcessMonitor(psI, Constants.SAMPLE_FILE_RUNTIME_3DXML + 8000);

                    if (!psI.HasExited)
                    {
                        psI.Kill();
                    }
                    if (flag == false)
                    {
                        tokenSource.Cancel();
                        sbResult.AppendLine("Test case terminated by User" + "\n");
                        txtResult.AppendText("Test case terminated by User" + "\n");
                    }
                    if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes" && flag)
                    {
                        sbResult.AppendLine(fpsThreadData3DXMLPlayer);
                        txtResult.AppendText(fpsThreadData3DXMLPlayer);
                        sbResult.AppendLine("3D XML Player App support test completed " + "\n");
                        txtResult.AppendText("3D XML Player App support test completed " + "\n");
                        result3DXMLPlayer = true;
                    }
                    else
                    {
                        sbResult.AppendLine("Unable to calculate FPS using HDX Monitor." + "\n");
                        txtResult.AppendText("Unable to calculate FPS using HDX Monitor." + "\n");
                    }
                }
                #endregion

                sbResult.AppendLine(Constants.TESTCASE12_NAME + " completed" + "\n");
                txtResult.AppendText(Constants.TESTCASE12_NAME + " completed" + "\n");
                this.Refresh();
            }
            catch (Exception ex)
            {
                this.Enabled = true;
                this.WindowState = FormWindowState.Normal;
                returnValue = Constants.VERIFICATION_STATUS_ERROR;
                sbResult.AppendLine("ERROR: Runtime Error in TC12_PixelPerfectLosslessCompression - " + ex.Message + "\n");
                txtResult.AppendText("ERROR: Runtime Error in TC12_PixelPerfectLosslessCompression - " + ex.Message + "\n");
            }
            this.Refresh();

            TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();

            if (resultGoogleEarth && result3DXMLPlayer && resultITKSnap) returnValue = Constants.VERIFICATION_STATUS_COMPLETED;
            TestCases.oTestCases[TestCaseID - 1].Status = returnValue;
            tokenSource.Dispose();
            Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);

            return returnValue;
            #endregion
        }
        #endregion

        #region - Test Case #13 - HDX P2P Video Conferencing on Skype for Business with HDX RealTime Optimization Pack
        public string TC13_HDXP2PVideoConferencingOnSkypeForBusiness(byte TestCaseID)
        {
            
                
            string RTOPTestcaseStatus = "";
            StringBuilder sbResult = new StringBuilder("");
            txtResult.AppendText("\n");
            txtResult.AppendText("Running " + Constants.TESTCASE13_NAME + " Test Case - " + DateTime.Now.ToString() + "\n");

            this.Enabled = false;
            if (Constants.isLynPresent)
            {
                if (Constants.IsEncoder == true)
                {
                    try
                    {
                        Globals.RTOP = "RTOP";
                        ProcessStartInfo psStartInfo = new ProcessStartInfo();
                        psStartInfo.FileName = @"" + "\"" + Constants.SOFTWARE_NAME_LYNC + "\"";
                        Process ps = Process.Start(psStartInfo);
                        Thread.Sleep(Constants.DUMMY_RUNTIME * 2);
                        //SendKeys.Send("%");
                        //SendKeys.Send("E");
                        //SendKeys.Send("F");
                        //SendKeys.Send("R");
                        //SendKeys.Send("{TAB}"); SendKeys.Send("{TAB}"); SendKeys.Send("{TAB}"); SendKeys.Send("{TAB}");
                        //SendKeys.Send("0");
                        //SendKeys.Send("~");
                        frmQsnBanner = new frmQuestionBanner(this, 13, ps);
                        frmQsnBanner.ShowDialog();


                        if (Globals.IsVideoRecorded == true)
                        {
                            UploadLoadingIcon icon = new UploadLoadingIcon();
                            icon.ShowDialog();
                        }

                        if (Globals.isRTOPCompleted2 == true)
                        {
                            sbResult.AppendLine("HDX RTOP with Skype For Business completed");
                            txtResult.AppendText("HDX P2P Video Conferencing on Skype for Business With HDX RealTime Optimization Pack completed" + "\n");
                            TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_COMPLETED;
                            TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                            TestCases.oTestCases[TestCaseID - 1].DownloadLink = Constants.VideoDownloadLink;
                            txtResult.AppendText(Constants.VideoDownloadLink);
                            StreamWriter fileLogHDXRTOP = new StreamWriter(Constants.LOG_FILE_NAME, true);
                            fileLogHDXRTOP.WriteLine("URL - " + Constants.VideoDownloadLink);
                            fileLogHDXRTOP.Close();
                            this.Enabled = true;
                            this.Refresh();
                            RTOPTestcaseStatus = Constants.VERIFICATION_STATUS_COMPLETED;
                        }
                        else
                        {
                            sbResult.AppendLine("HDX RTOP with Skype For Business closed  abruptly.");
                            txtResult.AppendText("HDX RTOP with Skype For Business closed  abruptly. \n");
                            this.Enabled = true;
                            this.Refresh();
                            TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;
                            TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                            RTOPTestcaseStatus = Constants.VERIFICATION_STATUS_ERROR;
                        }
                    }
                    catch(Exception ex)
                    {
                        RTOPTestcaseStatus = Constants.VERIFICATION_STATUS_ERROR;
                        sbResult.AppendLine("ERROR: Runtime Error in TC13_HDX P2P Video Conferencing On Skype For Business- " + ex.Message);
                        txtResult.AppendText("ERROR: Runtime Error in TC13_HDX P2P Video Conferencing On Skype For Business- " + ex.Message + "\n");

                    }
                }
                else
                {
                    MessageBox.Show("This testcase cannot be executed because all the required software(s) are not installed. Please install all the mandatory software(s) mentioned in the prerequisite.", Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    sbResult.AppendLine("ERROR: Required software(s) are not installed on this VDA");
                    txtResult.AppendText("ERROR: Required software(s) are not installed on this VDA" + "\n");
                    this.Enabled = true;
                    this.Refresh();
                    RTOPTestcaseStatus = Constants.VERIFICATION_STATUS_ERROR;
                }
                return RTOPTestcaseStatus;
            }
            else
            {
                TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;
                TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                sbResult.AppendLine("HDX P2P Video Conferencing on Skype for Business With HDX RealTime Optimization Pack Error: Skype for Business not available");
                txtResult.AppendText("HDX P2P Video Conferencing on Skype for Business With HDX RealTime Optimization Pack Error: Skype for Business not available" + "\n");
                this.Enabled = true;
                this.Refresh();
                return Constants.VERIFICATION_STATUS_ERROR;
            }
        }

        #endregion

        #region - Test Case # 14 - Citrix SD-WAN HDX Optimization 
        public string TC14_CloudBridge(byte TestCaseID)
        {
            StringBuilder sbResult = new StringBuilder("");
            DialogResult msgResult = new DialogResult();

            txtResult.AppendText("\n");
            txtResult.AppendText("Running " + Constants.TESTCASE14_NAME + " Test Case - " + DateTime.Now.ToString() + "\n");

            msgUser = "Please perform the Citrix SD-WAN Testing manually by following the instructions in the verification form on Citrix Ready Verification Platform." + "\n" + "\n" +
                "Once you are done, click OK to continue.";
            msgResult = MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE14_NAME, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (msgResult == DialogResult.OK)
            {
                sbResult.AppendLine("Citrix SD-WAN HDX Optimization completed");
                txtResult.AppendText("Citrix SD-WAN HDX Optimization completed" + "\n");

                TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_COMPLETED;
                TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                this.Refresh();

                return Constants.VERIFICATION_STATUS_COMPLETED;
            }
            else
            {
                sbResult.AppendLine("\n" + "Citrix SD-WAN HDX Optimization skipped");
                txtResult.AppendText("Citrix SD-WAN HDX Optimization skipped" + "\n");

                TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_SKIPPED;
                TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                this.Refresh();

                return Constants.VERIFICATION_STATUS_SKIPPED;
            }
        }
        #endregion

        #region - Test Case #15 - Citrix Gateway and Citrix ADM (formerly NetScaler MAS) verification
        public string TC15_NetScaler(byte TestCaseID)
        {
            StringBuilder sbResult = new StringBuilder("");
            DialogResult msgResult = new DialogResult();

            txtResult.AppendText("\n");
            txtResult.AppendText("Running " + Constants.TESTCASE15_NAME + " Test Case - " + DateTime.Now.ToString() + "\n");

            msgUser = "Please perform the Citrix Gateway and Citrix ADM (formerly NetScaler MAS) verification by following the steps from the Verification Platform";
            msgResult = MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE15_NAME, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (msgResult == DialogResult.OK)
            {
                sbResult.AppendLine("Citrix Gateway and Citrix ADM (formerly NetScaler MAS) verification completed");
                txtResult.AppendText("Citrix Gateway and Citrix ADM (formerly NetScaler MAS) verification completed" + "\n");

                TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_COMPLETED;
                TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                this.Refresh();

                return Constants.VERIFICATION_STATUS_COMPLETED;
            }
            else
            {
                sbResult.AppendLine("Citrix Gateway and Citrix ADM (formerly NetScaler MAS) verification completed");
                txtResult.AppendText("Citrix Gateway and Citrix ADM (formerly NetScaler MAS) verification completed" + "\n");

                TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_SKIPPED;
                TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                this.Refresh();

                return Constants.VERIFICATION_STATUS_SKIPPED;
            }
        }
        #endregion





        #endregion

        #region - HDX Premium Test Cases

        #region - Test Case #16 - H265 decoding for graphics
        public string TC16_H265DecodingForGraphics(byte TestCaseID)
        {
            StringBuilder sbResult = new StringBuilder("");
            string tc16TestcaseStatus = "";
            txtResult.AppendText("\n");
            txtResult.AppendText("Running " + Constants.TESTCASE16_NAME + " Test Case - " + DateTime.Now.ToString() + "\n");
            sbResult.AppendLine("H265 Decoding for Graphics completed");
            txtResult.AppendText("H265 Decoding for Graphics completed" + "\n");

            TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_COMPLETED;
            TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
            tc16TestcaseStatus   = Constants.VERIFICATION_STATUS_COMPLETED;
            return tc16TestcaseStatus;
        }

        #endregion

        #region - Test Case #17 - HDX Real-Time Audio (VoIP)
        public string TC17_HDXRealTimeAudioVoIP(byte TestCaseID)
        {
            StringBuilder sbResult = new StringBuilder("");
            txtResult.AppendText("\n");
            txtResult.AppendText("Running " + Constants.TESTCASE17_NAME + " Test Case - " + DateTime.Now.ToString() + "\n");
            try
            {
                string audacityTestcaseStatus = "";
                if (!System.IO.File.Exists(Constants.APP_EXECUTABLE_AUDACITY))
                {
                    sbResult.AppendLine("ERROR: Audacity is not installed on this VDA");
                    txtResult.AppendText("ERROR: Audacity is not installed on this VDA" + "\n");
                    TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;
                    audacityTestcaseStatus = Constants.VERIFICATION_STATUS_ERROR;
                }
                else
                {
                    if (Constants.IsEncoder == true)
                    {
                        Globals.VOIP = "VOIP";
                        //msgUser = "1) Audacity will be launched once Ok button is clicked.\n" +
                        //          "2) Please do not terminate the application in between or open other windows.\n" +
                        //          "3) Please follow the instructions mentioned in HDX Real-Time Audio (VoIP) test case";
                        //MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE10_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);

                        ProcessStartInfo psStartInfo = new ProcessStartInfo();
                        psStartInfo.FileName = @"" + "\"" + Constants.APP_EXECUTABLE_AUDACITY + "\"";

                        Process ps = Process.Start(psStartInfo);

                        Thread.Sleep(Constants.DUMMY_RUNTIME * 2);

                        //SendKeys.Send("%");
                        //SendKeys.Send("E");
                        //SendKeys.Send("F");
                        //SendKeys.Send("R");
                        //SendKeys.Send("{TAB}"); SendKeys.Send("{TAB}"); SendKeys.Send("{TAB}"); SendKeys.Send("{TAB}");
                        //SendKeys.Send("0");
                        //SendKeys.Send("~");

                        frmQsnBanner = new frmQuestionBanner(this, 9, ps);
                        frmQsnBanner.ShowDialog();

                        try
                        {
                            if (Globals.IsVideoRecorded == true)
                            {
                                UploadLoadingIcon icon = new UploadLoadingIcon();
                                icon.ShowDialog();
                            }
                        }
                        catch (Exception ex)
                        {
                            string msgUser = "Unable to establish a connection with Citrix Ready ShareFile server." + "\n" + "\n" +
                                           "This could be caused by a loss of network connectivity or inadequate permission to connect to the Citrix Ready ShareFile." + "\n" + "\n" +
                                           "If the problem persists, please reach out to us at CitrixReady@citrix.com";
                            MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                        if (Globals.isAudacityTestCaseCompleted == true)
                        {



                            sbResult.AppendLine("HDX Real-Time Audio (VoIP) completed");
                            txtResult.AppendText("HDX Real-Time Audio (VoIP) completed" + "\n");
                            TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_COMPLETED;
                            TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                            TestCases.oTestCases[TestCaseID - 1].DownloadLink = Constants.VideoDownloadLink;
                            txtResult.AppendText(Constants.VideoDownloadLink);
                            StreamWriter fileLogVoIP = new StreamWriter(Constants.LOG_FILE_NAME, true);
                            fileLogVoIP.WriteLine("URL - " + Constants.VideoDownloadLink);
                            fileLogVoIP.Close();
                            this.Enabled = true;
                            this.Refresh();
                            audacityTestcaseStatus = Constants.VERIFICATION_STATUS_COMPLETED;
                        }
                        else
                        {
                            sbResult.AppendLine("Audacity closed  abruptly.");
                            txtResult.AppendText("Audacity closed  abruptly. \n");
                            TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;
                            TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                            audacityTestcaseStatus = Constants.VERIFICATION_STATUS_ERROR;
                        }
                    }
                    else
                    {
                        MessageBox.Show("This testcase cannot be executed because all the required software(s) are not installed. Please install all the mandatory software(s) mentioned in the prerequisite.", Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        sbResult.AppendLine("ERROR: Required software(s) are not installed on this VDA");
                        txtResult.AppendText("ERROR: Required software(s) are not installed on this VDA" + "\n");
                        this.Enabled = true;
                        this.Refresh();
                        audacityTestcaseStatus = Constants.VERIFICATION_STATUS_ERROR;
                    }


                }
                return audacityTestcaseStatus;

            }
            catch (Exception ex)
            {
                sbResult.AppendLine("ERROR: Runtime Error in TC17_HDXRealTimeAudioVoIP - Audacity Recording failed - " + ex.Message);
                txtResult.AppendText("ERROR: Runtime Error in TC17_HDXRealTimeAudioVoIP - Audacity Recording failed - " + ex.Message + "\n");
                TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;
                return Constants.VERIFICATION_STATUS_ERROR;
            }

        }
        #endregion

        #region - Test Case #18 - HDX Media Stream Flash Redirection
        public string TC18_HDXMediaStreamFlashRedirection(byte TestCaseID)
        {
            StringBuilder sbResult = new StringBuilder("");
            int cpuUsage = 0;
            bool flashSite = false;
            bool youtube = false;
           

            txtResult.AppendText("\n");
            txtResult.AppendText("Running " + Constants.TESTCASE18_NAME + " Test Case - " + DateTime.Now.ToString() + "\n");

            #region - Run 720P Flash website
            try
            {
                string fpsTaskDataIEYouTubeWebsite = "";
                tokenSource = new CancellationTokenSource();
                token = tokenSource.Token;
                StreamWriter fileFPSLogIEYouTubeWebsiteStart = new StreamWriter(Constants.LOG_FILE_NAME, true);
                fileFPSLogIEYouTubeWebsiteStart.WriteLine("START - HDX Media Stream Flash Redirection  - Run 720P Flash website - " + DateTime.Now.ToString());
                fileFPSLogIEYouTubeWebsiteStart.Close();
                if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes")
                {
                    Task fpsTaskForIEYouTubeWebsite = Task.Factory.StartNew(() =>
                    fpsTaskDataIEYouTubeWebsite = Utilities.CalculateFPS(TestCaseID, "HDX Media Stream Flash Redirection  - Run 720P Flash website - ", Constants.FPS_INTERVAL, (Constants.FPS_CALCULATION_RUNTIME) / 1000, token), token);
                }

                ProcessStartInfo psStartInfo = new ProcessStartInfo();
                psStartInfo.WindowStyle = ProcessWindowStyle.Maximized;
                psStartInfo.FileName = @"" + "\"" + Constants.APP_EXECUTABLE_INTERNETEXPLORER + "\"";
                psStartInfo.Arguments = @"" + "\"" + Constants.SAMPLE_FILE_NAME_FLASH_URL + "\"";
                Process ps = Process.Start(psStartInfo);

                bool flag = Utilities.ProcessMonitor(ps, Constants.SAMPLE_FILE_RUNTIME_YOUTUBE + 40000);

                PerformanceCounter cpuCount = new PerformanceCounter();
                cpuCount.CategoryName = "Processor";
                cpuCount.CounterName = "% Processor Time";
                cpuCount.InstanceName = "_Total";
                cpuCount.NextValue();
                Thread.Sleep(100);
                cpuUsage = Convert.ToInt32(cpuCount.NextValue());
                sbResult.AppendLine("HDX Media Stream Flash Redirection Test - Run 720p Flash website - CPU Usage: " + cpuUsage + " %");
                txtResult.AppendText("HDX Media Stream Flash Redirection Test - Run 720p Flash website - CPU Usage: " + cpuUsage + " %" + "\n");

                if (flag == false)
                {
                    tokenSource.Cancel();
                    sbResult.AppendLine("Internet Explorer closed by User abruptly.");
                    txtResult.AppendText("Internet Explorer closed by User abruptly." + "\n");
                }

                if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes" && flag)
                {
                    sbResult.AppendLine(fpsTaskDataIEYouTubeWebsite);
                    txtResult.AppendText(fpsTaskDataIEYouTubeWebsite);
                    sbResult.AppendLine("HDX Media Stream Flash Redirection Test - Run 720P Flash website - ran successfully from: " + Constants.SAMPLE_FILE_NAME_YOUTUBE + "\n");
                    txtResult.AppendText("HDX Media Stream Flash Redirection Test - Run 720P Flash website - ran successfully from: " + Constants.SAMPLE_FILE_NAME_YOUTUBE + "\n");
                    string eventLogged = Utilities.EventLogReader("flash");
                    if (eventLogged != "")
                    {
                        if (Constants.eventFlag == 1)
                        {
                            sbResult.AppendLine("Exception with event viewer logs: \n" + eventLogged + "\n");
                            txtResult.AppendText("Exception with event viewer logs: \n" + eventLogged + "\n");
                        }
                        else
                        {
                            sbResult.AppendLine(eventLogged + "\n");
                            txtResult.AppendText(eventLogged + "\n");
                        }
                    }
                    else
                    {
                        sbResult.AppendLine("Event logs are not present, please check the event logger service status and try again." + "\n");
                        txtResult.AppendText("Event logs are not present, please check the event logger service status and try again." + "\n");
                    }

                    flashSite = true;
                }
                else
                {
                    sbResult.AppendLine("Unable to calculate FPS using HDX Monitor.");
                    txtResult.AppendText("Unable to calculate FPS using HDX Monitor." + "\n");
                }

                StreamWriter fileFPSLogIEYouTubeEnd = new StreamWriter(Constants.LOG_FILE_NAME, true);
                fileFPSLogIEYouTubeEnd.WriteLine("END - HDX Media Stream Flash RedirectionTest - Run 720P Flash website - " + DateTime.Now.ToString());
                fileFPSLogIEYouTubeEnd.Close();

            }
            catch (Exception ex)
            {
                msgUser = "ERROR: Runtime Error in TC11_HDX Media Stream Flash Redirection - Run 720p Flash website - " + ex.Message;

                sbResult.AppendLine(msgUser + ex.Message + "\n");
                txtResult.AppendText(msgUser + ex.Message + "\n");
                TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;
            }

            #endregion

            #region - Run 720P HD Flash Video
            try
            {
                string fpsTaskDataIEYouTubeWebsite = "";
                tokenSource = new CancellationTokenSource();
                token = tokenSource.Token;
                StreamWriter fileFPSLogIEYouTubeVideoStart = new StreamWriter(Constants.LOG_FILE_NAME, true);
                fileFPSLogIEYouTubeVideoStart.WriteLine("START - HDX Media Stream Flash Redirection - Run 720P Flash video - " + DateTime.Now.ToString());
                fileFPSLogIEYouTubeVideoStart.Close();
                Process psForTestCase10;
                int ieVersion = Utilities.GetInternetExplorerVersion();
                this.WindowState = FormWindowState.Minimized;
                if (ieVersion == 10 || ieVersion == 11)
                {
                    ProcessStartInfo psInfo = new ProcessStartInfo();
                    psInfo.WindowStyle = ProcessWindowStyle.Maximized;
                    psInfo.FileName = @"" + "\"" + Constants.APP_EXECUTABLE_INTERNETEXPLORER + "\"";
                    psInfo.Arguments = @"" + "\"" + Constants.SAMPLE_FILE_NAME_YOUTUBE_URL_TRAILER + "\"";
                    psForTestCase10 = Process.Start(psInfo);

                    Thread.Sleep(Constants.DUMMY_RUNTIME * 2);
                    SendKeys.Send("{F12}");
                    Thread.Sleep(Constants.DUMMY_RUNTIME * 2);
                    SendKeys.Send("^(7)");
                    SendKeys.Send("^(8)");
                    Thread.Sleep(Constants.DUMMY_RUNTIME);
                    SendKeys.Send("{TAB}");
                    SendKeys.Send("{9}");
                    #region - commented
                    //bool ieFlag = Utilities.ProcessMonitor(psForTestCase10, Constants.FPSRUNTIMEFORTC10);
                    //frmCalculateFPS formCalculateFps = new frmCalculateFPS(this, 4, psForTestCase10);
                    //formCalculateFps.ShowDialog();
                    //if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes" && Globals.isUserStartedFPS)
                    //{
                    //    Task fpsTaskForIEYouTubeWebsite = Task.Factory.StartNew(() =>
                    //    fpsTaskDataIEYouTubeWebsite = Utilities.CalculateFPS(TestCaseID, "HDX Media Stream Flash Redirection - Run 720P Flash video - ", Constants.FPS_INTERVAL, Constants.FPSRUNTIMEFORTC10 / 1000, token), token);
                    //}
                    #endregion
                    if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes")
                    {
                        Task fpsTaskForIEYouTubeWebsite = Task.Factory.StartNew(() =>
                        fpsTaskDataIEYouTubeWebsite = Utilities.CalculateFPS(TestCaseID, "HDX Media Stream Flash Redirection- Run 720P Flash video - ", Constants.FPS_INTERVAL, (Constants.FPS_CALCULATION_RUNTIME) / 1000, token), token);
                    }
                    Constants.FPSRUNTIMEFORTC10 = Constants.SAMPLE_FILE_RUNTIME_YOUTUBE;
                }
                else
                {
                    ProcessStartInfo psStartInfo = new ProcessStartInfo();
                    psStartInfo.WindowStyle = ProcessWindowStyle.Maximized;
                    psStartInfo.FileName = @"" + "\"" + Constants.APP_EXECUTABLE_INTERNETEXPLORER + "\"";
                    psStartInfo.Arguments = @"" + "\"" + Constants.SAMPLE_FILE_NAME_YOUTUBE_URL_TRAILER + "\"";
                    psForTestCase10 = Process.Start(psStartInfo);

                    if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes")
                    {
                        Task fpsTaskForIEYouTubeWebsite = Task.Factory.StartNew(() =>
                        fpsTaskDataIEYouTubeWebsite = Utilities.CalculateFPS(TestCaseID, "HDX Media Stream Flash Redirection - Run 720P Flash video - ", Constants.FPS_INTERVAL, (Constants.FPS_CALCULATION_RUNTIME) / 1000, token), token);
                    }
                    Constants.FPSRUNTIMEFORTC10 = Constants.SAMPLE_FILE_RUNTIME_YOUTUBE;
                }

                bool flag = Utilities.ProcessMonitor(psForTestCase10, Constants.FPSRUNTIMEFORTC10);
                this.WindowState = FormWindowState.Normal;
                if (flag == false)
                {
                    tokenSource.Cancel();
                    sbResult.AppendLine("Internet Explorer closed by User or not started FPS Calculation.");
                    txtResult.AppendText("Internet Explorer closed by User or not started FPS Calculation." + "\n");
                }

                if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes" && flag)
                {
                    sbResult.AppendLine(fpsTaskDataIEYouTubeWebsite);
                    txtResult.AppendText(fpsTaskDataIEYouTubeWebsite);
                    sbResult.AppendLine("HDX Media Stream Flash Redirection - Run 720P Flash video - ran successfully from: " + Constants.SAMPLE_FILE_NAME_YOUTUBE + "\n");
                    txtResult.AppendText("HDX Media Stream Flash Redirection  - Run 720P Flash video - ran successfully from: " + Constants.SAMPLE_FILE_NAME_YOUTUBE + "\n");
                    string eventLogged = Utilities.EventLogReader("flash");
                    if (eventLogged != "")
                    {
                        if (Constants.eventFlag == 1)
                        {
                            sbResult.AppendLine("Exception with event viewer logs: \n" + eventLogged + "\n");
                            txtResult.AppendText("Exception with event viewer logs: \n" + eventLogged + "\n");
                        }
                        else
                        {
                            sbResult.AppendLine(eventLogged + "\n");
                            txtResult.AppendText(eventLogged + "\n");
                        }
                    }
                    else
                    {
                        sbResult.AppendLine("Event logs are not present, please check the event logger service status and try again." + "\n");
                        txtResult.AppendText("Event logs are not present, please check the event logger service status and try again." + "\n");
                    }
                    youtube = true;
                }
                else
                {
                    sbResult.AppendLine("Unable to calculate FPS using HDX Monitor.");
                    txtResult.AppendText("Unable to calculate FPS using HDX Monitor." + "\n");
                }

                StreamWriter fileFPSLogIEYouTubeEnd = new StreamWriter(Constants.LOG_FILE_NAME, true);
                fileFPSLogIEYouTubeEnd.WriteLine("END - HDX Media Stream Flash Redirection  - Run 720P Flash video - " + DateTime.Now.ToString());
                fileFPSLogIEYouTubeEnd.Close();
            }
            catch (Exception ex)
            {
                msgUser = "ERROR: Runtime Error in TC18_HDX Media Stream Flash Redirection - Run a 720p HD Flash video - " + ex.Message;
                MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);

                sbResult.AppendLine(msgUser + ex.Message);
                txtResult.AppendText(msgUser + ex.Message + "\n");
                TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;
                return Constants.VERIFICATION_STATUS_ERROR;
            }
            #endregion

           

            if (flashSite && youtube )
            {
                TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_COMPLETED;
                TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                this.Refresh();

                return Constants.VERIFICATION_STATUS_COMPLETED;
            }
            else
            {
                TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;
                TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                this.Refresh();

                return Constants.VERIFICATION_STATUS_ERROR;
            }
        }
        #endregion

        #region - Test Case #19 - HTML5 Video Redirection
        public string TC19_HTML5VideoRedirection(byte TestCaseID)
        {
            StringBuilder sbResult = new StringBuilder("");
            int cpuUsage = 0;
            bool html5 = false;

            txtResult.AppendText("\n");
            txtResult.AppendText("Running " + Constants.TESTCASE19_NAME + " Test Case - " + DateTime.Now.ToString() + "\n");


            #region - Run HTML5 redirection
            try
            {
                PerformanceCounter cpuCounter = new PerformanceCounter();
                int cpuUsed = 1;

                msgUser = "Please follow below steps for this test case:" + "\n" + "\n" +
                    "1. Verify that the player controls switch to citrix custom controls" + "\n" + "\n" +
                    "2. Verify that the controls: Play, Pause, Seek, Audio are functioning properly." + "\n" + "\n" +
                    "3. Make sure that player controls don't fade away as the tool takes screenshots during the testcase" + "\n";
                MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE19_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);

                string fpsTaskDataIEHTML5 = "";
                tokenSource = new CancellationTokenSource();
                token = tokenSource.Token;
                StreamWriter fpsTaskDataIEHTML5Start = new StreamWriter(Constants.LOG_FILE_NAME, true);
                fpsTaskDataIEHTML5Start.WriteLine("START - HTML5 Video Redirection - Run HTML5 Flash website - " + DateTime.Now.ToString());
                fpsTaskDataIEHTML5Start.Close();
                if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes")
                {
                    Task fpsTaskForIEHTML5 = Task.Factory.StartNew(() =>
                    fpsTaskDataIEHTML5 = Utilities.CalculateFPS(TestCaseID, "HTML5 Video Redirection - Run HTML5 Flash website - ", Constants.FPS_INTERVAL, (Constants.FPS_CALCULATION_RUNTIME) / 1000, token), token);
                }

                ProcessStartInfo psStartInfo = new ProcessStartInfo();
                psStartInfo.WindowStyle = ProcessWindowStyle.Maximized;

                psStartInfo.FileName = @"" + "\"" + Constants.APP_EXECUTABLE_INTERNETEXPLORER + "\"";
                psStartInfo.Arguments = @"" + "\"" + Constants.SAMPLE_FILE_NAME_HTML5 + "\"";
                Process ps = Process.Start(psStartInfo);

                Task screencaptureTask = Task.Factory.StartNew(() =>
                {
                    scrnCaptureTimer.Interval = 10000;
                    scrnCaptureTimer.AutoReset = false;
                    scrnCaptureTimer.Start();
                    scrnCaptureTimer.Elapsed += new System.Timers.ElapsedEventHandler(CaptureScreen);

                    cpuCounter.CategoryName = "Processor";
                    cpuCounter.CounterName = "% Processor Time";
                    cpuCounter.InstanceName = "_Total";
                    cpuCounter.NextValue();
                    Thread.Sleep(100);
                    cpuUsed = Convert.ToInt32(cpuCounter.NextValue());

                });

                bool flag = Utilities.ProcessMonitor(ps, Constants.SAMPLE_FILE_RUNTIME_HTML5);

                sbResult.AppendLine("HTML5 Video Redirection Test - Run 720p Flash website - CPU Usage: " + cpuUsed + " %");
                txtResult.AppendText("HTML5 Video Redirection Test - Run 720p Flash website - CPU Usage: " + cpuUsed + " %" + "\n");

                if (flag == false)
                {
                    tokenSource.Cancel();
                    sbResult.AppendLine("Internet Explorer closed by User abruptly.");
                    txtResult.AppendText("Internet Explorer closed by User abruptly." + "\n");
                }

                if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes" && flag)
                {
                    sbResult.AppendLine(fpsTaskDataIEHTML5);
                    txtResult.AppendText(fpsTaskDataIEHTML5);
                    sbResult.AppendLine("HTML5 Video Redirection - Run HTML5 Flash website from: " + Constants.SAMPLE_FILE_NAME_HTML5 + "\n");
                    txtResult.AppendText("HTML5 Video Redirection - Run HTML5 Flash website from: " + Constants.SAMPLE_FILE_NAME_HTML5 + "\n");
                    string eventLogged = Utilities.EventLogReader("flash");
                    if (eventLogged != "")
                    {
                        if (Constants.eventFlag == 1)
                        {
                            sbResult.AppendLine("Exception with event viewer logs: \n" + eventLogged + "\n");
                            txtResult.AppendText("Exception with event viewer logs: \n" + eventLogged + "\n");
                        }
                        else
                        {
                            sbResult.AppendLine(eventLogged + "\n");
                            txtResult.AppendText(eventLogged + "\n");
                        }
                    }
                    else
                    {
                        sbResult.AppendLine("Event logs are not present, please check the event logger service status and try again." + "\n");
                        txtResult.AppendText("Event logs are not present, please check the event logger service status and try again." + "\n");
                    }
                    html5 = true;
                }
                else
                {
                    sbResult.AppendLine("Unable to calculate FPS using HDX Monitor.");
                    txtResult.AppendText("Unable to calculate FPS using HDX Monitor." + "\n");
                }

                StreamWriter fileFPSLogIEYouTubeEnd = new StreamWriter(Constants.LOG_FILE_NAME, true);
                fileFPSLogIEYouTubeEnd.WriteLine("END - HTML5 Video Redirection - Run 720P Flash website - " + DateTime.Now.ToString());
                fileFPSLogIEYouTubeEnd.Close();

            }
            catch (Exception ex)
            {
                msgUser = "ERROR: Runtime Error in TC19_HTML5 Video Redirection - Run 720p Flash website - " + ex.Message;

                sbResult.AppendLine(msgUser + ex.Message + "\n");
                txtResult.AppendText(msgUser + ex.Message + "\n");
                TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;
            }

            #endregion

            if (html5)
            {
                TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_COMPLETED;
                TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                this.Refresh();

                return Constants.VERIFICATION_STATUS_COMPLETED;
            }
            else
            {
                TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;
                TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                this.Refresh();

                return Constants.VERIFICATION_STATUS_ERROR;
            }
        }
        #endregion

        #region - Test Case #20 - HDX Media Stream Windows Media Redirection
        public string TC20_HDXMediaStreamWindowsMediaRedirection(byte TestCaseID)
        {
            StringBuilder sbResult = new StringBuilder("");

            txtResult.AppendText("\n");
            txtResult.AppendText("Running " + Constants.TESTCASE20_NAME + " Test Case - " + DateTime.Now.ToString() + "\n" + "\n");

            RunVideoForFPS(TestCaseID);

            #region - Run different files of different formats with Windows Media Player
            try
            {
                bool flag1 = System.IO.File.Exists(Globals.SampleFilesLocation + Constants.SAMPLE_FILE_NAME_1);
                bool flag2 = System.IO.File.Exists(Globals.SampleFilesLocation + Constants.SAMPLE_FILE_NAME_2);
                bool flag3 = System.IO.File.Exists(Globals.SampleFilesLocation + Constants.SAMPLE_FILE_NAME_3);
                bool flag4 = System.IO.File.Exists(Globals.SampleFilesLocation + Constants.SAMPLE_FILE_NAME_4);
                bool flag5 = System.IO.File.Exists(Globals.SampleFilesLocation + Constants.SAMPLE_FILE_NAME_5);
                if (((flag1 && flag2) && (flag3 && flag4)) && flag5)
                {
                    RunWithWindowsMediaPlayer(Globals.SampleFilesLocation + Constants.SAMPLE_FILE_NAME_1);
                    RunWithWindowsMediaPlayer(Globals.SampleFilesLocation + Constants.SAMPLE_FILE_NAME_2);
                    RunWithWindowsMediaPlayer(Globals.SampleFilesLocation + Constants.SAMPLE_FILE_NAME_3);
                    string eventLogged = Utilities.EventLogReader("rave");
                    if (eventLogged != "")
                    {
                        if (Constants.eventFlag == 1)
                        {
                            sbResult.AppendLine("Exception with event viewer logs: \n" + eventLogged + "\n");
                            txtResult.AppendText("Exception with event viewer logs: \n" + eventLogged + "\n");
                        }
                        else
                        {
                            sbResult.AppendLine(eventLogged + "\n");
                            txtResult.AppendText(eventLogged + "\n");
                        }
                    }
                    else
                    {
                        sbResult.AppendLine("Event logs are not present, please check the event logger service status and try again." + "\n");
                        txtResult.AppendText("Event logs are not present, please check the event logger service status and try again." + "\n");
                    }
                    RunWithWindowsMediaPlayer(Globals.SampleFilesLocation + Constants.SAMPLE_FILE_NAME_4);
                    RunWithWindowsMediaPlayer(Globals.SampleFilesLocation + Constants.SAMPLE_FILE_NAME_5);
                    string eventLogged2 = Utilities.EventLogReader("rave");
                    if (eventLogged2 != "")
                    {
                        if (Constants.eventFlag == 1)
                        {
                            sbResult.AppendLine("Exception with event viewer logs: \n" + eventLogged2 + "\n");
                            txtResult.AppendText("Exception with event viewer logs: \n" + eventLogged2 + "\n");
                        }
                        else
                        {
                            sbResult.AppendLine(eventLogged2 + "\n");
                            txtResult.AppendText(eventLogged2 + "\n");
                        }
                    }
                    else
                    {
                        sbResult.AppendLine("Event logs are not present, please check the event logger service status and try again." + "\n");
                        txtResult.AppendText("Event logs are not present, please check the event logger service status and try again." + "\n");
                    }

                    msgUser = @"Before running the next multimedia videos test, Please make the below registry changes in these locations:" + "\n" +
                    @"1) For 32 bit: HKEY_LOCAL_MACHINE\SOFTWARE\Citrix\HDXMediastream" + "\n" +
                    @"2) For 64 bit: HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Citrix\HDXMediastream" + "\n\n" +
                    "\t" + "TranscodingWatermark = (DWORD) 1" + "\n" + "\t" + "ForceTranscodingPolicy = (DWORD) 1" + "\n" +
                    "\t" + "ForceTranscoding = (DWORD) 1";
                    MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE12_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    RunWithWindowsMediaPlayer(Globals.SampleFilesLocation + Constants.SAMPLE_FILE_NAME_1);
                    RunWithWindowsMediaPlayer(Globals.SampleFilesLocation + Constants.SAMPLE_FILE_NAME_2);
                    RunWithWindowsMediaPlayer(Globals.SampleFilesLocation + Constants.SAMPLE_FILE_NAME_3);
                    string eventLogged3 = Utilities.EventLogReader("rave");
                    if (eventLogged3 != "")
                    {
                        if (Constants.eventFlag == 1)
                        {
                            sbResult.AppendLine("Exception with event viewer logs: \n" + eventLogged3 + "\n");
                            txtResult.AppendText("Exception with event viewer logs: \n" + eventLogged3 + "\n");
                        }
                        else
                        {
                            sbResult.AppendLine(eventLogged3 + "\n");
                            txtResult.AppendText(eventLogged3 + "\n");
                        }
                    }
                    else
                    {
                        sbResult.AppendLine("Event logs are not present, please check the event logger service status and try again." + "\n");
                        txtResult.AppendText("Event logs are not present, please check the event logger service status and try again." + "\n");
                    }
                    RunWithWindowsMediaPlayer(Globals.SampleFilesLocation + Constants.SAMPLE_FILE_NAME_4);
                    RunWithWindowsMediaPlayer(Globals.SampleFilesLocation + Constants.SAMPLE_FILE_NAME_5);
                    string eventLogged4 = Utilities.EventLogReader("rave");
                    if (eventLogged4 != "")
                    {
                        if (Constants.eventFlag == 1)
                        {
                            sbResult.AppendLine("Exception with event viewer logs: \n" + eventLogged4 + "\n");
                            txtResult.AppendText("Exception with event viewer logs: \n" + eventLogged4 + "\n");
                        }
                        else
                        {
                            sbResult.AppendLine(eventLogged4 + "\n");
                            txtResult.AppendText(eventLogged4 + "\n");
                        }
                    }
                    else
                    {
                        sbResult.AppendLine("Event logs are not present, please check the event logger service status and try again." + "\n");
                        txtResult.AppendText("Event logs are not present, please check the event logger service status and try again." + "\n");
                    }

                    sbResult.AppendLine("HDX Media Stream Windows Media Redirection - completed" + "\n");
                    txtResult.AppendText("HDX Media Stream Windows Media Redirection - completed" + "\n");

                    TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_COMPLETED;
                    TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                    this.Refresh();

                    return Constants.VERIFICATION_STATUS_COMPLETED;
                }
                else
                {
                    sbResult.AppendLine("ERROR: Windows Media Player Sample File is missing: " + Globals.SampleFilesLocation);
                    txtResult.AppendText("ERROR: Windows Media Player Sample File is missing: " + Globals.SampleFilesLocation + "\n");
                    TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;
                    TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                    this.Refresh();

                    return Constants.VERIFICATION_STATUS_ERROR;

                }
            }

            catch (Exception ex)
            {
                msgUser = "ERROR: Runtime Error in TC20_HDXMediaStreamWindowsMediaRedirection - HDX Media Stream Windows Media Redirection - " + ex.Message;
                sbResult.AppendLine(msgUser + ex.Message);
                txtResult.AppendText(msgUser + ex.Message + "\n");
                TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;
                return Constants.VERIFICATION_STATUS_ERROR;
            }
            #endregion

        }

        #region - FPS video play
        public void RunVideoForFPS(byte TestCaseID)
        {
            StringBuilder sbResult = new StringBuilder("");
            string sampleFileFullPath = "";
            tokenSource = new CancellationTokenSource();
            token = tokenSource.Token;

            bool isHDXMonitoringToolAvailable = SysRequirements.oSysRequirements[Constants.TC_HDX_MONITOR_TOOL - 1].IsInstalled;
            if (!isHDXMonitoringToolAvailable) sbResult.AppendLine("HDX Monitor Tool is not available");

            Thread.Sleep(Constants.TC_WAIT_TIME_IN_MS);
            StreamWriter fileFPSLogWindowsMediaPlayer = new StreamWriter(Constants.LOG_FILE_NAME, true);
            fileFPSLogWindowsMediaPlayer.WriteLine("START - Windows Media Player Video Clip - " + DateTime.Now.ToString());
            fileFPSLogWindowsMediaPlayer.Close();

            try
            {
                string fpsTaskDataWindowsMediaPlayer = "";

                sampleFileFullPath = Globals.SampleFilesLocation;
                sampleFileFullPath = sampleFileFullPath + Constants.SAMPLE_FILE_NAME_MOVIE_MP4;
                if (!System.IO.File.Exists(sampleFileFullPath))
                {
                    sbResult.AppendLine("ERROR: Windows Media Player MP4 Movie Sample File is missing: " + sampleFileFullPath);
                    txtResult.AppendText("ERROR: Windows Media Player MP4 Movie Sample File is missing: " + sampleFileFullPath + "\n");
                }
                else
                {
                    if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes")
                    {
                        Task fpsTaskForWindowsMediaPlayer = Task.Factory.StartNew(() =>
                        fpsTaskDataWindowsMediaPlayer = Utilities.CalculateFPS(TestCaseID, "Windows Media Player Video Clip", Constants.FPS_INTERVAL, (Constants.FPS_CALCULATION_RUNTIME) / 1000, token), token);
                    }

                    ProcessStartInfo psStartInfo = new ProcessStartInfo();
                    psStartInfo.FileName = @"" + "\"" + Constants.APP_EXECUTABLE_WINDOWSMEDIAPLAYER + "\"";
                    psStartInfo.Arguments = @" " + "\"" + sampleFileFullPath + "\" /fullscreen";
                    psStartInfo.WindowStyle = ProcessWindowStyle.Maximized;
                    Process ps = Process.Start(psStartInfo);

                    bool flag = Utilities.ProcessMonitor(ps, Constants.SAMPLE_FILE_RUNTIME_MP4);

                    if (!ps.HasExited)
                    {
                        ps.Kill();
                    }
                    if (flag == false)
                    {
                        tokenSource.Cancel();
                        sbResult.AppendLine("Media Player terminated by User abruptly.");
                        txtResult.AppendText("Media Player terminated by User abruptly." + "\n");
                    }
                    if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes" && flag)
                    {
                        sbResult.AppendLine(fpsTaskDataWindowsMediaPlayer);
                        txtResult.AppendText(fpsTaskDataWindowsMediaPlayer);
                        sbResult.AppendLine("Windows Media Player Movie Sample File ran successfully from: " + sampleFileFullPath);
                        txtResult.AppendText("Windows Media Player Movie Sample File ran successfully from: " + sampleFileFullPath + "\n");
                    }
                    else
                    {
                        sbResult.AppendLine("Unable to calculate FPS using HDX Monitor.");
                        txtResult.AppendText("Unable to calculate FPS using HDX Monitor." + "\n");
                    }
                }

                StreamWriter fileFPSLogWindowsMediaPlayerEnd = new StreamWriter(Constants.LOG_FILE_NAME, true);
                fileFPSLogWindowsMediaPlayerEnd.WriteLine("END - Windows Media Player Video Clip - " + DateTime.Now.ToString());
                fileFPSLogWindowsMediaPlayerEnd.Close();
            }
            catch (Exception ex)
            {
                sbResult.AppendLine("ERROR: Runtime Error in TC20_VidPlaybackPlayer - Video Playback - Server Rendered Windows Media Test Failed - " + ex.Message);
                txtResult.AppendText("ERROR: Runtime Error in TC20_VidPlaybackPlayer - Video Playback - Server Rendered Windows Media Test Failed - " + ex.Message + "\n");
            }
        }
        #endregion

        #region - Playing multiple video files
        public void RunWithWindowsMediaPlayer(string sampleFileFullPath)
        {
            StringBuilder sbResult = new StringBuilder("");

            if (!System.IO.File.Exists(sampleFileFullPath))
            {
                sbResult.AppendLine("ERROR: Windows Media Player Sample File is missing: " + sampleFileFullPath);
                txtResult.AppendText("ERROR: Windows Media Player Sample File is missing: " + sampleFileFullPath + "\n");
            }
            else
            {
                ProcessStartInfo psStartInfo = new ProcessStartInfo();
                psStartInfo.FileName = @"" + "\"" + Constants.APP_EXECUTABLE_WINDOWSMEDIAPLAYER + "\"";
                psStartInfo.Arguments = @" " + "\"" + sampleFileFullPath + "\" /fullscreen";
                psStartInfo.WindowStyle = ProcessWindowStyle.Maximized;
                Process ps = Process.Start(psStartInfo);
                Thread.Sleep(Constants.SAMPLE_FILE_RUNTIME_MULTIMEDIA_VARIOUS);
                sbResult.AppendLine("Windows Media Player Movie Sample File ran successfully from: " + sampleFileFullPath);
                txtResult.AppendText("Windows Media Player Movie Sample File ran successfully from: " + sampleFileFullPath + "\n");
                if (!ps.HasExited)
                {
                    ps.Kill();
                }
            }
        }
        #endregion
        #endregion

        #region - Test Case #21 - HDX RealTime Webcam Compression
        public string TC21_HDXRealTimeWebcamCompression(byte TestCaseID)
        {
            StringBuilder sbResult = new StringBuilder("");

            txtResult.AppendText("\n");
            txtResult.AppendText("Running " + Constants.TESTCASE21_NAME + " Test Case - " + DateTime.Now.ToString() + "\n");

            msgUser = "1) Please ensure the Webcam is connected and is working correctly" + "\n" +
                "2) Ensure that you have logged in to Skype For Business" +
                "\n\nOnce you are done, click OK to continue.";
            MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE21_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);

            this.Enabled = false;
            if (Constants.isLynPresent)
            {
                Process.Start(Constants.SOFTWARE_NAME_LYNC);
                Thread.Sleep(2000);
                Process ps = Process.Start(Constants.SOFTWARE_NAME_LYNC);
                bool flag = Utilities.ProcessMonitor(ps, Constants.DUMMY_RUNTIME * 8, true);
                if (!ps.HasExited)
                {
                    IntPtr mainWindow = ps.MainWindowHandle;
                    IntPtr newPos = new IntPtr(-1);
                    SetWindowPos(mainWindow, new IntPtr(0), 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE | SWP_SHOWWINDOW);
                }
                Thread.Sleep(Constants.DUMMY_RUNTIME);
                SendKeys.SendWait("%");
                SendKeys.SendWait("%(T)");
                Thread.Sleep(Constants.DUMMY_RUNTIME);
                SendKeys.Send("C");
                Thread.Sleep(Constants.DUMMY_RUNTIME / 2);

                scrnCaptureTimer.Interval = 1000;
                scrnCaptureTimer.AutoReset = false;
                scrnCaptureTimer.Start();
                scrnCaptureTimer.Elapsed += new System.Timers.ElapsedEventHandler(CaptureScreen);

                Thread.Sleep(Constants.DUMMY_RUNTIME);
                SendKeys.Send("~");
                Thread.Sleep(Constants.DUMMY_RUNTIME);

                Process[] psSkype = Process.GetProcessesByName("lync");
                if (psSkype != null)
                {
                    ShowWindow(psSkype[0].MainWindowHandle, SW_FORCEMINIMIZE);
                }

                sbResult.AppendLine("HDX RealTime Webcam Compression is successfully completed. Screenshot capture is available at: " + Globals.screenshotName);
                txtResult.AppendText("HDX RealTime Webcam Compression is successfully completed. Screenshot capture is available at: " + Globals.screenshotName + "\n");

                TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_COMPLETED;
                TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();

                this.Enabled = true;
                this.Refresh();
                return Constants.VERIFICATION_STATUS_COMPLETED;
            }
            else
            {
                sbResult.AppendLine("HDX RealTime Webcam Compression Error: Skype for Business not available.");
                txtResult.AppendText("HDX RealTime Webcam Compression Error: Skype for Business not available." + "\n");

                TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;
                TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();

                this.Enabled = true;
                this.Refresh();
                return Constants.VERIFICATION_STATUS_ERROR;
            }
        }
        #endregion

        #region - Test Case #22 - Browser Content Redirection

        public string TC22_BrowserContentRedirection(byte TestCaseID)
        {
            StringBuilder sbResult = new StringBuilder("");
            string tc16TestcaseStatus = "";
            txtResult.AppendText("\n");
            txtResult.AppendText("Running " + Constants.TESTCASE22_NAME + " Test Case - " + DateTime.Now.ToString() + "\n");
            sbResult.AppendLine("Browser Content Redirection");
            txtResult.AppendText("Browser Content Redirection" + "\n");

            TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_COMPLETED;
            TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
            tc16TestcaseStatus = Constants.VERIFICATION_STATUS_COMPLETED;
            return tc16TestcaseStatus;
        }
        #endregion

        #region - Test Case #23 - HDX PIV Smartcard On-Premises Support
        public string TC23_HDXPIVSmartcardSupport(byte TestCaseID)
        {
            StringBuilder sbResult = new StringBuilder("");
            try
            {
                TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;

                txtResult.AppendText("\n");
                txtResult.AppendText("Running " + Constants.TESTCASE23_NAME + " Test Case - " + DateTime.Now.ToString() + "\n");

                msgUser = "1) Please insert PIV Smart Card." + "\n" +
                    "2) Ensure the smart card is configured for login." + "\n" +
                    "3) Without closing the automation tool and logging off the VDA session, disconnect the session." + "\n" +
                    "4) Connect back to the VDA session." + "\n" +
                    "5) Once connected back to the session, Click OK." + "\n" +
                    "\n\nOnce you are done, click OK to continue.";
                MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE23_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);

                if (!Utilities.CheckWindowsService(Constants.APP_SERVICE_SMARTCARD))
                {
                    sbResult.AppendLine("Smart Card Service is not running");
                    txtResult.AppendText("Smart Card Service is not running" + "\n");
                }

                ProcessStartInfo psStartInfo = new ProcessStartInfo();
                psStartInfo.FileName = @"" + "\"" + Constants.APP_EXECUTABLE_SMARTCARD + "\"";
                psStartInfo.Arguments = @" /SCINFO ";
                psStartInfo.UseShellExecute = false;
                psStartInfo.RedirectStandardOutput = true;
                psStartInfo.CreateNoWindow = true;

                Process ps = Process.Start(psStartInfo);
                string psOutput = "";

                while (!ps.StandardOutput.EndOfStream)
                {
                    psOutput = psOutput + ps.StandardOutput.ReadLine() + "\n";
                    sbResult.AppendLine(ps.StandardOutput.ReadLine());
                    txtResult.AppendText(ps.StandardOutput.ReadLine() + "\n");
                }

                if ((psOutput.IndexOf("failed", StringComparison.OrdinalIgnoreCase) >= 0) ||
                    (psOutput.IndexOf("not running", StringComparison.OrdinalIgnoreCase) >= 0))
                {
                    TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;
                    sbResult.AppendLine("HDX PIV Smartcard On-Premises Support Test failed");
                    txtResult.AppendText("HDX PIV Smartcard On-Premises Support Test failed" + "\n");
                }
                else
                {
                    TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_COMPLETED;
                    sbResult.AppendLine("HDX PIV Smartcard On-Premises Support completed");
                    txtResult.AppendText("HDX PIV Smartcard On-Premises Support completed" + "\n");
                }

                TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
                this.Refresh();

                return TestCases.oTestCases[TestCaseID - 1].Status;
            }

            catch (Exception ex)
            {
                sbResult.AppendLine("ERROR: Runtime Error in TC23_HDX PIV Smartcard On-Premises Support - " + ex.Message);
                txtResult.AppendText("ERROR: Runtime Error in TC23_HDX PIV Smartcard On-Premises Support - " + ex.Message + "\n");
                TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;
                return TestCases.oTestCases[TestCaseID - 1].Status;
            }
        }
        #endregion

        #region - Test Case #9 - HDX Multi Monitor Support
        //public string TC8_HDXMultiMonitor(byte TestCaseID)
        //{
        //    StringBuilder sbResult = new StringBuilder("");
        //    string sampleFileFullPathVLC = "";
        //    string sampleFileFullPathPPT = "";
        //    string msgUser = "";
        //    int cntScreen = 0;
        //    bool resultNP = false;
        //    bool resultMW = false;
        //    bool isHDXMonitoringToolAvailable = SysRequirements.oSysRequirements[Constants.TC_HDX_MONITOR_TOOL - 1].IsInstalled;
        //    if (!isHDXMonitoringToolAvailable) sbResult.AppendLine("HDX Monitor Tool is not available");

        //    txtResult.AppendText("\n");
        //    txtResult.AppendText("Running " + Constants.TESTCASE09_NAME + " Test Case - " + DateTime.Now.ToString() + "\n");

        //    Screen[] scr = Screen.AllScreens;
        //    cntScreen = scr.Count();
        //    if (!(cntScreen > 1))
        //    {
        //        msgUser = "Please ensure you have two monitors connected in Extended mode." + "\n" + "\n" +
        //               "Once you are done, click OK to continue.";
        //        MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION + " - " + Constants.TESTCASE09_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
        //    }
        //    if (cntScreen > 1)
        //    {

        //        string fpsTaskDataNotepad = "";

        //        #region - Moving Notepad
        //        tokenSource = new CancellationTokenSource();
        //        token = tokenSource.Token;
        //        if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes")
        //        {
        //            Task fpsTaskForNotepad = Task.Factory.StartNew(() =>
        //            fpsTaskDataNotepad = Utilities.CalculateFPS(TestCaseID, "Moving notepad across screens", Constants.FPS_INTERVAL, (Constants.FPS_CALCULATION_RUNTIME) / 1000, token), token);
        //        }

        //        ProcessStartInfo psStartInfo = new ProcessStartInfo();
        //        psStartInfo.WindowStyle = ProcessWindowStyle.Normal;
        //        psStartInfo.FileName = @"" + "\"" + Constants.APP_EXECUTABLE_NOTEPAD + "\"";
        //        Process ps = Process.Start(psStartInfo);
        //        Thread.Sleep(Constants.SAMPLE_FILE_RUNTIME_NOTEPAD);

        //        IntPtr handle = ps.MainWindowHandle;
        //        Screen[] screens = Screen.AllScreens;
        //        int iCounter = 1;
        //        int iScreenNumber = 0;
        //        scrnCaptureTimer.Interval = 1000;
        //        scrnCaptureTimer.AutoReset = false;
        //        scrnCaptureTimer.Start();
        //        scrnCaptureTimer.Elapsed += new System.Timers.ElapsedEventHandler(CaptureScreen);

        //        while (iScreenNumber <= screens.Length)
        //        {
        //            if (iScreenNumber == 0)
        //            {
        //                MoveWindow(handle, screens[iScreenNumber].Bounds.Width / 2, 0, screens[iScreenNumber].Bounds.Width, screens[iScreenNumber].Bounds.Height, false);
        //                this.Refresh();
        //                Thread.Sleep(1000);
        //                iScreenNumber++;
        //                MoveWindow(handle, screens[iScreenNumber].Bounds.X, screens[iScreenNumber].Bounds.Y, screens[iScreenNumber].Bounds.Width, screens[iScreenNumber].Bounds.Height, false);
        //                this.Refresh();
        //                Thread.Sleep(1000);
        //            }
        //            else
        //            {
        //                MoveWindow(handle, screens[iScreenNumber].Bounds.Width / 2, 0, screens[iScreenNumber].Bounds.Width, screens[iScreenNumber].Bounds.Height, false);
        //                this.Refresh();
        //                Thread.Sleep(1000);
        //                iScreenNumber--;
        //                MoveWindow(handle, screens[iScreenNumber].Bounds.X, screens[iScreenNumber].Bounds.Y, screens[iScreenNumber].Bounds.Width, screens[iScreenNumber].Bounds.Height, false);
        //                this.Refresh();
        //                Thread.Sleep(1000);
        //            }
        //            iCounter++;
        //            if (iCounter == 20)
        //            {
        //                break;
        //            }
        //        }
        //        sbResult.AppendLine("Screenshot saved successfully and the path is " + Globals.screenshotName);
        //        txtResult.AppendText("Screenshot saved successfully and the path is " + Globals.screenshotName + "\n");

        //        if (!ps.HasExited)
        //        {
        //            ps.Kill();
        //        }
        //        if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes")
        //        {
        //            sbResult.AppendLine(fpsTaskDataNotepad);
        //            txtResult.AppendText(fpsTaskDataNotepad);
        //            sbResult.AppendLine("Scenario of Moving app across screens is completed.");
        //            txtResult.AppendText("Scenario of Moving app across screens is completed." + "\n");
        //            resultNP = true;
        //        }
        //        else
        //        {
        //            sbResult.AppendLine("HDX Monitor Tool has not been installed properly. FPS is not available.");
        //            txtResult.AppendText("HDX Monitor Tool has not been installed properly. FPS is not available." + "\n");
        //        }
        //        this.Refresh();

        //        #endregion

        //        #region - Opening multiple apps on different screens
        //        sampleFileFullPathVLC = Globals.SampleFilesLocation;
        //        sampleFileFullPathVLC = sampleFileFullPathVLC + Constants.SAMPLE_FILE_NAME_MOVIE_MP4;

        //        sampleFileFullPathPPT = Globals.SampleFilesLocation;
        //        sampleFileFullPathPPT = sampleFileFullPathPPT + Constants.SAMPLE_FILE_NAME_POWERPOINT;

        //        if (!System.IO.File.Exists(sampleFileFullPathVLC) && !System.IO.File.Exists(sampleFileFullPathPPT))
        //        {
        //            sbResult.AppendLine("ERROR: Video (720p resolution) sample files is missing: " + sampleFileFullPathVLC + "\n" + sampleFileFullPathPPT);
        //            txtResult.AppendText("ERROR: Video (720p resolution) sample files is missing: " + sampleFileFullPathVLC + "\n" + sampleFileFullPathPPT + "\n");
        //        }
        //        else
        //        {
        //            Screen[] sc = Screen.AllScreens;
        //            string fpsTaskDataMultipleApp = "";
        //            tokenSource = new CancellationTokenSource();
        //            token = tokenSource.Token;
        //            if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes")
        //            {
        //                Task fpsTaskForMultipleApp = Task.Factory.StartNew(() =>
        //                fpsTaskDataMultipleApp = Utilities.CalculateFPS(TestCaseID, "Opening multiple apps across screens", Constants.FPS_INTERVAL, (Constants.FPS_CALCULATION_RUNTIME) / 1000, token), token);
        //            }

        //            ProcessStartInfo psStartInfoVLC = new ProcessStartInfo();
        //            psStartInfoVLC.FileName = @"" + "\"" + Constants.APP_EXECUTABLE_VLCPLAYER + "\"";
        //            psStartInfoVLC.Arguments = @" -f " + "\"" + sampleFileFullPathVLC + "\"";
        //            psStartInfoVLC.WindowStyle = ProcessWindowStyle.Maximized;
        //            Process psVLC = Process.Start(psStartInfoVLC);
        //            Thread.Sleep(500);

        //            ProcessStartInfo psStartInfoPPT = new ProcessStartInfo();
        //            psStartInfoPPT.FileName = @"" + "\"" + Constants.APP_EXECUTABLE_POWERPOINT + "\"";
        //            psStartInfoPPT.Arguments = @"/S " + "\"" + sampleFileFullPathPPT + "\"";
        //            Process psPPT = Process.Start(psStartInfoPPT);
        //            psPPT.WaitForInputIdle();

        //            Thread.Sleep(Constants.SAMPLE_FILE_RUNTIME_NOTEPAD);

        //            IntPtr handlePPT = psPPT.MainWindowHandle;
        //            MoveWindow(handlePPT, sc[1].Bounds.X, sc[1].Bounds.Y, sc[1].Bounds.Width, sc[1].Bounds.Height, false);
        //            scrnCaptureTimer.Interval = 15000;
        //            scrnCaptureTimer.AutoReset = false;
        //            scrnCaptureTimer.Start();
        //            scrnCaptureTimer.Elapsed += new System.Timers.ElapsedEventHandler(CaptureScreen);

        //            bool flag = Utilities.ProcessMonitor(psPPT, psVLC, Constants.SAMPLE_FILE_RUNTIME_MULTIWINDOW);

        //            sbResult.AppendLine("Screenshot saved successfully and the path is " + Globals.screenshotName);
        //            txtResult.AppendText("Screenshot saved successfully and the path is " + Globals.screenshotName + "\n");

        //            sbResult.AppendLine("Video (720p resolution) sample file ran successfully on 1st screen: " + sampleFileFullPathVLC);
        //            txtResult.AppendText("Video (720p resolution) sample file ran successfully on 1st screen: " + sampleFileFullPathVLC + "\n");

        //            sbResult.AppendLine("PowerPoint sample file ran successfully on 2nd screen: " + sampleFileFullPathPPT);
        //            txtResult.AppendText("PowerPoint sample file ran successfully on 2nd screen: " + sampleFileFullPathPPT + "\n");
        //            if (flag == false)
        //            {
        //                sbResult.AppendLine("MultiMonitor Apps terminated by User.");
        //                txtResult.AppendText("MultiMonitor Apps terminated by User." + "\n");
        //            }
        //            if (SysInformations.oSysInformations[Constants.SOFTWARE_NUM7_HDX_MONITOR].ConfigValue.ToString() == "Yes" && flag)
        //            {
        //                sbResult.AppendLine(fpsTaskDataMultipleApp);
        //                txtResult.AppendText(fpsTaskDataMultipleApp);
        //                sbResult.AppendLine("HDX MultiMonitor Support Verification completed sucessfully");
        //                txtResult.AppendText("HDX MultiMonitor Support Verification completed sucessfully" + "\n");
        //                resultMW = true;
        //            }
        //            else
        //            {
        //                sbResult.AppendLine("HDX Monitor Tool has not been installed properly. FPS is not available.");
        //                txtResult.AppendText("HDX Monitor Tool has not been installed properly. FPS is not available." + "\n");
        //            }
        //        }
        //        #endregion

        //        if (resultNP && resultMW)
        //        {
        //            TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_COMPLETED;
        //            TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
        //            this.Refresh();
        //            return Constants.VERIFICATION_STATUS_COMPLETED;
        //        }
        //        else
        //        {
        //            sbResult.AppendLine("ERROR: TC9_HDXMultiMonitor Failed and FPS Unavailable");
        //            txtResult.AppendText("ERROR: TC9_HDXMultiMonitor Failed and FPS Unavailable");
        //            TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;

        //            return Constants.VERIFICATION_STATUS_ERROR;
        //        }
        //    }
        //    else
        //    {
        //        msgUser = "You don't seem to have multi-monitor connection.  This test case will be skipped.";
        //        MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);
        //        sbResult.AppendLine("INFO: You don't seem to have multi-monitor connection.  This test case will be skipped.");
        //        txtResult.AppendText("INFO: You don't seem to have multi-monitor connection.  This test case will be skipped." + "\n");
        //        TestCases.oTestCases[TestCaseID - 1].Status = Constants.VERIFICATION_STATUS_ERROR;
        //        TestCases.oTestCases[TestCaseID - 1].Result = sbResult.ToString();
        //        return Constants.VERIFICATION_STATUS_ERROR;
        //    }
        //}


        #endregion

        

       

        

        

      

       
        #endregion

       

        #region - Change Status Column Color
        public void UpdateStatusColor(byte TestCaseNumber)
        {
            string strStatusLabelControlID = "lblStatus" + Convert.ToString(TestCaseNumber);
            Label lblStatusControl = this.Controls.Find(strStatusLabelControlID, true).FirstOrDefault() as Label;

            if (lblStatusControl != null)
            {
                if (lblStatusControl.Text == Constants.VERIFICATION_STATUS_IN_PROGRESS)
                {
                    lblStatusControl.ForeColor = System.Drawing.ColorTranslator.FromHtml(Constants.HTML_COLOR_NORMAL);
                }
                if (lblStatusControl.Text == Constants.VERIFICATION_STATUS_SKIPPED)
                {
                    lblStatusControl.ForeColor = System.Drawing.ColorTranslator.FromHtml(Constants.HTML_COLOR_NORMAL);
                }
                if (lblStatusControl.Text == Constants.VERIFICATION_STATUS_ERROR)
                {
                    lblStatusControl.ForeColor = System.Drawing.ColorTranslator.FromHtml(Constants.HTML_COLOR_RED);
                }
                if (lblStatusControl.Text == Constants.VERIFICATION_STATUS_COMPLETED)
                {
                    lblStatusControl.ForeColor = System.Drawing.ColorTranslator.FromHtml(Constants.HTML_COLOR_GREEN);
                }
            }
        }
        #endregion

        #endregion

        #region - Policy links click events
        private void lblHDXReadyPoliciesInfo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(Constants.HDXReadyPolicyLink);
        }

        private void lblHDXPremiumPoliciesInfo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(Constants.HDXPremiumPolicyLink);
        }

        private void lblHDX3DProPoliciesInfo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(Constants.HDX3DProPolicyLink);
        }



        #endregion





        #region - Custom Tasks
        public static async Task<ShareFileClient> PasswordAuthentication(SampleUser user, string clientId, string clientSecret)
        {
            try
            {
                // Initialize ShareFileClient.
                var configuration = Configuration.Default();
                configuration.Logger = new DefaultLoggingProvider();

                var sfClient = new ShareFileClient("https://secure.sf-api.com/sf/v3/", configuration);
                var oauthService = new OAuthService(sfClient, clientId, clientSecret);

                // Perform a password grant request.  Will give us an OAuthToken
                var oauthToken = await oauthService.PasswordGrantAsync(user.Username, user.Password, user.Subdomain, user.ControlPlane);

                // Add credentials and update sfClient with new BaseUri
                sfClient.AddOAuthCredentials(oauthToken);
                sfClient.BaseUri = oauthToken.GetUri();

                return sfClient;
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public static async Task StartSession(ShareFileClient sfClient)
        {
            var session = await sfClient.Sessions.Login().Expand("Principal").ExecuteAsync();
            Console.WriteLine("Authenticated as " + session.Principal.Email);
        }

        public static async Task<Folder> CreateFolder(ShareFileClient sfClient, Uri parentFolder, string folderToCreate)
        {
            // Create instance of the new folder we want to create.  Only a few properties 
            // on folder can be defined, others will be ignored.
            var newFolder = new Folder
            {
                Name = folderToCreate,
                Description = "Created by: PartnerProductVerificationTool"
            };
            return await sfClient.Items.CreateFolder(parentFolder, newFolder, overwrite: true).ExecuteAsync();
        }

        public static async Task<string> UploadVideo()
        {
            try
            {

                Folder folderName = new Folder();
                folderName = await CreateFolder(sfClient, Constants.folderToUploadUri, DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss"));
                var msgUser = await Upload(sfClient, folderName, Constants.VideoFileName);
                return msgUser.ToString();
            }
            catch (Exception ex)
            {
                string msgUser = "Unable to establish a connection with Citrix Ready ShareFile server." + "\n" + "\n" +
                               "This could be caused by a loss of network connectivity or inadequate permission to connect to the Citrix Ready ShareFile." + "\n" + "\n" +
                               "If the problem persists, please reach out to us at CitrixReady@citrix.com";
                MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }


        }
        public static async Task<UploadResponse> Upload(ShareFileClient sfClient, Folder destinationFolder, string FilePath)
        {
            if (System.IO.File.Exists(FilePath))
            {

                var file = System.IO.File.Open(FilePath, System.IO.FileMode.Open);
                try
                {
                    var uploadRequest = new UploadSpecificationRequest
                    {
                        FileName = System.IO.Path.GetFileName(FilePath),
                        FileSize = file.Length,
                        //Details = companyName + "\r\n" + Name + "\r\n" + "\r\n" + email,
                        Parent = destinationFolder.url

                    };

                    bytesofOriginalFile = file.Length;
                    var uploader = sfClient.GetAsyncFileUploader(uploadRequest, new PlatformFileStream(file, file.Length, "samplefile"));
                    var uploadResponse = await uploader.UploadAsync();
                    Share linkItem = new Share();
                    if (uploadResponse != null)
                    {
                        foreach (var item in uploadResponse)
                        {
                            var itemUri = sfClient.Items.GetAlias(item.Id);
                            var uploadedFile = await sfClient.Items.Get(itemUri).ExecuteAsync();
                            var share = new Share
                            {
                                Items = new List<Item> { uploadedFile }
                            };
                            linkItem = await sfClient.Shares.Create(share).ExecuteAsync();
                        }
                    }
                    Constants.VideoDownloadLink = linkItem.Uri.ToString();
                    Globals.isRTOPCompleted1 = true;
                    Globals.isRTOPCompleted2 = true;
                    Globals.isAudacityTestCaseCompleted = true;
                    Globals.isUpload = true;

                    return uploadResponse;
                }
                catch (Exception ex)
                {
                    file.Close();
                    Console.Write(ex.Message);

                    return null;
                }
                finally
                {
                    file.Close();
                }
            }
            else
            {
                Console.Write("ERROR");
                return null;
            }
        }
        #endregion



        //powershell script

        public static string AddDoubleQuotes(string value)
        {
            return "\"" + value + "\"";
        }
        public static string GetUniqueSiteName()
        {
            string siteName = "Site";
            var uniqueKey = Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(0, 6);
            return siteName + uniqueKey;

        }
        public static string CreatePolicyScript(string templateName, string policyName)
        {
            string uniqueSiteName = GetUniqueSiteName();
            var script =
  @"Add-PSSnapin Citrix.Common.GroupPolicy" + Environment.NewLine +
  @"New-PSDrive " + uniqueSiteName + " –PSProvider CitrixGroupPolicy –Root \\ -Controller localhost" + Environment.NewLine +
  @"cd " + uniqueSiteName + ":\\" + Environment.NewLine +
  @"copy-item Templates:\" + templateName + " " + uniqueSiteName + ":\\User\\" + policyName + Environment.NewLine +
  @"copy-item Templates:\" + templateName + " " + uniqueSiteName + ":\\Computer\\" + policyName + Environment.NewLine;

            return script;
        }
        public static string CheckCitrixPolicyExist(string policyName)
        {

            string uniqueSiteName = GetUniqueSiteName();
            var script =
  @"Add-PSSnapin Citrix.Common.GroupPolicy" + Environment.NewLine +
  @"New-PSDrive " + uniqueSiteName + " –PSProvider CitrixGroupPolicy –Root \\ -Controller localhost" + Environment.NewLine +
  @"cd " + uniqueSiteName + ":\\User\\" + policyName + Environment.NewLine +
  @"cd " + uniqueSiteName + ":\\Computer\\" + policyName + Environment.NewLine;
            return script;
        }

        public static string CheckCitrixPolicyStatus(string policyName)
        {
            string uniqueSiteName = GetUniqueSiteName();
            var script =
 @"Add-PSSnapin Citrix.Common.GroupPolicy" + Environment.NewLine +
  @"New-PSDrive " + uniqueSiteName + " –PSProvider CitrixGroupPolicy –Root \\ -Controller localhost" + Environment.NewLine +
  @"cd " + uniqueSiteName + ":\\" + Environment.NewLine +
  @"cd " + uniqueSiteName + ":\\User\\" + policyName + Environment.NewLine +
  @"Get-ItemProperty . –Name Enabled " + Environment.NewLine +
  @"cd " + uniqueSiteName + ":\\Computer\\" + policyName + Environment.NewLine +
  @"Get-ItemProperty . –Name Enabled " ;
            return script;
        }

        public static string GetCitrixPolicyListScript()
        {
            string uniqueSiteName = GetUniqueSiteName();
            var script =
  @"Add-PSSnapin Citrix.Common.GroupPolicy" + Environment.NewLine +
  @"New-PSDrive " + uniqueSiteName + " –PSProvider CitrixGroupPolicy –Root \\ -Controller localhost" + Environment.NewLine +
  @"cd " + uniqueSiteName + ":\\User" + Environment.NewLine +
  @"dir" + Environment.NewLine +
  @"cd " + uniqueSiteName + ":\\Computer" + Environment.NewLine +
  @"dir" + Environment.NewLine;
            return script;
        }
        public static string GetPolicyStatusChangeScript(string policyName, string statusName)
        {
            string uniqueSiteName = GetUniqueSiteName();
            var script =
  @"Add-PSSnapin Citrix.Common.GroupPolicy" + Environment.NewLine +
  @"New-PSDrive " + uniqueSiteName + " –PSProvider CitrixGroupPolicy –Root \\ -Controller localhost" + Environment.NewLine +
  @"cd " + uniqueSiteName + ":\\" + Environment.NewLine +
  @"cd " + uniqueSiteName + ":\\User\\" + policyName + Environment.NewLine +
  @"Set-ItemProperty . –Name Enabled –Value " + statusName + Environment.NewLine +
  @"cd " + uniqueSiteName + ":\\Computer\\" + policyName + Environment.NewLine +
  @"Set-ItemProperty . –Name Enabled –Value " + statusName;
            return script;

        }
        public static string GetDisableAllPolicyScript()
        {
            string uniqueSiteName = GetUniqueSiteName();
            var script =
  @"Add-PSSnapin Citrix.Common.GroupPolicy" + Environment.NewLine +
  @"New-PSDrive " + uniqueSiteName + " –PSProvider CitrixGroupPolicy –Root \\ -Controller localhost" + Environment.NewLine +
  @"cd " + uniqueSiteName + ":\\" + Environment.NewLine +
  @"cd " + uniqueSiteName + ":\\User" + Environment.NewLine +
  @"Set-ItemProperty . –Name Enabled –Value  false"  + Environment.NewLine +
  @"cd " + uniqueSiteName + ":\\Computer"+ Environment.NewLine +
  @"Set-ItemProperty . –Name Enabled –Value  false" ;
            return script;

        }

        public static string EnablePowerShellPermission(string ipAddress)
        {
            System.Collections.ObjectModel.Collection<PSObject> results = new System.Collections.ObjectModel.Collection<PSObject>();
            string ip = AddDoubleQuotes(ipAddress);
            string script = "winrm s winrm/config/client '@{TrustedHosts=" + ip + "}'"; // whatever you want to execute

            // step 1 entered
            //StreamWriter fileLogHDXRTOP = new StreamWriter(Constants.LOG_FILE_NAME, true);
            //fileLogHDXRTOP.WriteLine("Step 1 entered");
            //fileLogHDXRTOP.Close();

            //string script1 = @"Clear-Item -Path WSMan:\localhost\Client\TrustedHosts –Force";

            using (Runspace runspace = RunspaceFactory.CreateRunspace())
            {
                try
                {
                    // StreamWriter fileLogHDXRTOP1 = new StreamWriter(Constants.LOG_FILE_NAME, true);
                    runspace.Open();
                    using (Pipeline pipeline = runspace.CreatePipeline())
                    {
                        // log

                        // fileLogHDXRTOP1.WriteLine("Step 2 pipeline entered");


                        pipeline.Commands.AddScript(script);
                        results = pipeline.Invoke();
                        // fileLogHDXRTOP1.WriteLine("Step 3  script invoked");
                    }

                    //fileLogHDXRTOP1.Close();
                }
                finally
                {
                    runspace.Close();
                }
            }

            // convert the script result into a single string
            StringBuilder stringBuilder = new StringBuilder();
            foreach (PSObject obj in results)
            {
                stringBuilder.AppendLine(obj.ToString());
            }
            return stringBuilder.ToString();


        }
        public static string ExecuteScriptHandledPolicyExist(string psScript, WSManConnectionInfo connectionInfo)
        {
            string response = "";
            System.Collections.ObjectModel.Collection<PSObject> results = new System.Collections.ObjectModel.Collection<PSObject>();
            try
            {
                using (Runspace runspace = RunspaceFactory.CreateRunspace(connectionInfo))
                {
                    try
                    {
                        runspace.Open();

                        using (Pipeline pipeline = runspace.CreatePipeline())
                        {

                            pipeline.Commands.AddScript(psScript);
                            results = pipeline.Invoke();
                            if (pipeline.Error.Count > 0)
                            {
                                response = "PolicyNotExist";
                            }
                            else
                            {
                                response = "PolicyFound";
                            }
                        }
                    }
                    finally
                    {
                        runspace.Close();
                    }
                }

                // Not Required to Read

                // convert the script result into a single string
                //StringBuilder stringBuilder = new StringBuilder();
                //foreach (PSObject obj in results)
                //{
                //    stringBuilder.AppendLine(obj.ToString());
                //}

                // response = stringBuilder.ToString();
            }
            catch (Exception ex)
            {
                response = "";
                MessageBox.Show(ex.Message + "dtk" + ex.StackTrace);
            }

            return response;

        }
        public static string ExecuteScript(string psScript, WSManConnectionInfo connectionInfo)
        {

            System.Collections.ObjectModel.Collection<PSObject> results = new System.Collections.ObjectModel.Collection<PSObject>();
            try
            {
                using (Runspace runspace = RunspaceFactory.CreateRunspace(connectionInfo))
                {
                    try
                    {
                        runspace.Open();
                        using (Pipeline pipeline = runspace.CreatePipeline())
                        {

                            pipeline.Commands.AddScript(psScript);
                            results = pipeline.Invoke();
                        }
                    }
                    finally
                    {
                        runspace.Close();
                    }
                }

                // convert the script result into a single string
                StringBuilder stringBuilder = new StringBuilder();
                foreach (PSObject obj in results)
                {
                    stringBuilder.AppendLine(obj.ToString());
                }
                return stringBuilder.ToString();
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        public static string PowerShellScriptCall(string serverIpAddress, string username, string password, string callmethod, string policytitle, string status = "")
        {
            string result = "";

            try
            {
                EnablePowerShellPermission(Constants.CitrixStudioIP);
                string domainName = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName;

                //string username = "rt";
                string fullName = domainName + "\\" + username; 

                var str = password;

                var sc = new SecureString();
                foreach (char c in str) sc.AppendChar(c);


                WSManConnectionInfo connectionInfo = new WSManConnectionInfo(false, Constants.CitrixStudioIP, 5985, "/wsman", "http://schemas.microsoft.com/powershell/Microsoft.PowerShell", new System.Management.Automation.PSCredential(fullName, sc));
                //string Hdx_Ready_PolicyName = "HDX_Ready";
                //string Hdx_Premium_PolicyName = "HDX_Premium";
                //string Hdx_Pro_PolicyName = "HDX_3D_Pro";

                string Hdx_Ready_PolicyName = "Standard";
                string Hdx_Premium_PolicyName = "Premium";
                string Hdx_Pro_PolicyName = "Standard_contd";
                //string Hdx_Ready_TemplateName = AddDoubleQuotes("HDX Ready");
                //string Hdx_Premium_TemplateName = AddDoubleQuotes("HDX Premium");
                //string Hdx_Pro_TemplateName = AddDoubleQuotes("HDX 3D Pro");
                string Hdx_Ready_TemplateName = AddDoubleQuotes("Standard");
                string Hdx_Premium_TemplateName = AddDoubleQuotes("Premium");
                string Hdx_Pro_TemplateName = AddDoubleQuotes("Standard contd");

                string policyname = "";
                string templatename = "";
                switch (policytitle)
                {
                    case "1":
                        policyname = Hdx_Ready_PolicyName;
                        templatename = Hdx_Ready_TemplateName;
                        break;

                    case "2":
                        policyname = Hdx_Premium_PolicyName;
                        templatename = Hdx_Premium_TemplateName;
                        break;

                    case "3":
                        policyname = Hdx_Pro_PolicyName;
                        templatename = Hdx_Pro_TemplateName;
                        break;

                }


                //var checkPolicyExist()
                var getPolicyExistResponse = "";
                if (callmethod == "Create")
                {
                    getPolicyExistResponse = ExecuteScriptHandledPolicyExist(CheckCitrixPolicyExist(policyname), connectionInfo);
                    if (getPolicyExistResponse == "PolicyNotExist")
                    {
                        var createPolicyResponse = ExecuteScript(CreatePolicyScript(templatename, policyname), connectionInfo);
                        if (!string.IsNullOrEmpty(createPolicyResponse))
                        {
                            var getPolicyResponse = ExecuteScript(GetCitrixPolicyListScript(), connectionInfo);
                            if (getPolicyResponse.Contains("Name=" + policyname))
                            {
                                result = "Policy :" + policyname + "  Already Exist in Studio";
                            }
                        }
                    }
                    else
                    {
                        result = "Policy :" + policyname + "  Already Exist in Studio";
                    }
                }
                else if (callmethod == "StatusChange")
                {
                    // follow like above method

                    getPolicyExistResponse = ExecuteScriptHandledPolicyExist(CheckCitrixPolicyExist(policyname), connectionInfo);
                    if (getPolicyExistResponse == "PolicyFound")
                    {
                        getPolicyExistResponse = ExecuteScript(GetPolicyStatusChangeScript(policyname, status), connectionInfo);




                    }


                }
                else if (callmethod == "StatusCheck")
                {
                    getPolicyExistResponse = ExecuteScriptHandledPolicyExist(CheckCitrixPolicyExist(policyname), connectionInfo);
                    if (getPolicyExistResponse == "PolicyFound")
                    {
                        if (policytitle.Equals("1"))
                        {
                            String tempResult = ExecuteScript(CheckCitrixPolicyStatus(policyname), connectionInfo);
                            if (tempResult.Contains("Enabled=True"))
                            {
                                result = "Policy :" + policyname + "  Enabled";
                            }
                            else
                            {
                                result = "Policy :" + policyname + "  Disabled";
                            }
                        }
                    }
                }
                else if (callmethod == "DisableAll")
                {
                    if (policytitle.Equals("1"))
                    {
                        var getPolicyResponse = ExecuteScript(GetCitrixPolicyListScript(), connectionInfo);
                        List<String> final = getPolicies(getPolicyResponse.ToString());
                        foreach (String policy in final)
                        {
                            getPolicyExistResponse = ExecuteScript(GetPolicyStatusChangeScript(policy, status), connectionInfo);
                            System.Threading.Thread.Sleep(5000);

                        }
                    }
                    else
                    {
                    }

                    //var s1 = ExecuteScript(GetPolicyStatusChangeScript ("HDX_Premium","True"), connectionInfo);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }

        private static List<string> getPolicies(string input)
        {
            String[] temp = input.Split('@');
            List<String> temp2 = new List<string>();
            foreach (String s in temp)
            {
                if (s.Contains("Name"))
                {

                    String x = s.Split(';')[0];
                    temp2.Add(x.Substring(x.IndexOf('=') + 1).Trim());
                }
            }
            List<String> final = temp2.Distinct().ToList();
            return final;
        }

        private void tabTestCases_SelectedIndexChanged(object sender, EventArgs e)
        {
            Constants.CurrentPolicyStatusChanged = false;

            //switch (tabTestCases.SelectedIndex.ToString())
            //{
            //    case "0":
            //        pictureBox1.Visible = true;
            //        DisableItem();
            //        await Task.Factory.StartNew(() => Constants.tempLoadingFlag = HDXReady());
            //        Constants.HDXReadyTab = true;
            //        pictureBox1.Visible = false;
            //        EnableItem();
            //        break;

            //    case "1":
            //        pictureBox1.Visible = true;
            //        DisableItem();
            //        await Task.Factory.StartNew(() => Constants.tempLoadingFlag = HDXPremium());
            //        Constants.HDXPremiumTab = true;
            //        pictureBox1.Visible = false;
            //        EnableItem();
            //        break;

            //    case "2":
            //        pictureBox1.Visible = true;
            //        DisableItem();
            //        await Task.Factory.StartNew(() => Constants.tempLoadingFlag = HDX3d());
            //        Constants.HDXPremiumTab = true;
            //        Constants.HDX3DTab = true;
            //        pictureBox1.Visible = false;
            //        EnableItem();
            //        break;
            //}
        }

        public string HDXReady()
        {
            string result = String.Empty;
            if (Constants.HDXReadyPolicyStatus == false)
            {
                var HDXReadyresponse = PowerShellScriptCall(Constants.CitrixStudioIP, Constants.CitrixStudioUserName, Constants.CitrixStudioPassword, "Create", "1");

                if (HDXReadyresponse.Contains("Already Exist in Studio"))
                {
                    var HDXReadyCurrentResponse = PowerShellScriptCall(Constants.CitrixStudioIP, Constants.CitrixStudioUserName, Constants.CitrixStudioPassword, "StatusCheck", "1");
                    if (HDXReadyCurrentResponse.Contains("Disabled"))
                    {
                        var HDXReadydisableResponse1 = PowerShellScriptCall(Constants.CitrixStudioIP, Constants.CitrixStudioUserName, Constants.CitrixStudioPassword, "DisableAll", "1", "false");
                        System.Threading.Thread.Sleep(5000);
                        //var HDXReadydisableResponse2 = PowerShellScriptCall(Constants.CitrixStudioIP, Constants.CitrixStudioUserName, Constants.CitrixStudioPassword, "StatusChange", "3", "false");
                        //System.Threading.Thread.Sleep(5000);
                        var HDXReadyResponseEnabled = PowerShellScriptCall(Constants.CitrixStudioIP, Constants.CitrixStudioUserName, Constants.CitrixStudioPassword, "StatusChange", "1", "true");
                        System.Threading.Thread.Sleep(5000);
                        msgUser = "Please logoff and login again into the VDA for the policies to take effect!" + "\n" +
                             "Click OK after logging back in.";
                        MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                       
                    }
                }

                // MessageBox.Show("Success");
                // Constants.HDXReadyPolicyStatus = true;               
            }
            return result = "success";
        }


        public string HDXPremium()
        {
            string result = String.Empty;
            if (Constants.HDXPremiumPolicyStatus == false)
            {
                string HDXPremiumresponse = PowerShellScriptCall(Constants.CitrixStudioIP, Constants.CitrixStudioUserName, Constants.CitrixStudioPassword, "Create", "2");

                if (HDXPremiumresponse.Contains("Already Exist in Studio"))
                {
                    var HDXPremiumresponseEnabled = PowerShellScriptCall(Constants.CitrixStudioIP, Constants.CitrixStudioUserName, Constants.CitrixStudioPassword, "StatusChange", "2", "true");
                    System.Threading.Thread.Sleep(5000);
                    var HDXPremiumdisableResponse1 = PowerShellScriptCall(Constants.CitrixStudioIP, Constants.CitrixStudioUserName, Constants.CitrixStudioPassword, "StatusChange", "1", "false");
                    System.Threading.Thread.Sleep(5000);
                    var HDXPremiumdisableResponse2 = PowerShellScriptCall(Constants.CitrixStudioIP, Constants.CitrixStudioUserName, Constants.CitrixStudioPassword, "StatusChange", "3", "false");

                }
                //MessageBox.Show("Success");
                // Constants.HDXPremiumPolicyStatus = true;
            }
            return result = "success";
        }

        public string HDX3d()
        {
            var result = String.Empty; ;
            if (Constants.HDX3DPolicyStatus == false)
            {
                var HDX3Dresponse = PowerShellScriptCall(Constants.CitrixStudioIP, Constants.CitrixStudioUserName, Constants.CitrixStudioPassword, "Create", "3");

                if (HDX3Dresponse.Contains("Already Exist in Studio"))
                {
                    var HDX3DresponseEnabled = PowerShellScriptCall(Constants.CitrixStudioIP, Constants.CitrixStudioUserName, Constants.CitrixStudioPassword, "StatusChange", "3", "true");
                    System.Threading.Thread.Sleep(5000);
                    var HDX3DdisableResponse1 = PowerShellScriptCall(Constants.CitrixStudioIP, Constants.CitrixStudioUserName, Constants.CitrixStudioPassword, "StatusChange", "1", "false");
                    System.Threading.Thread.Sleep(5000);
                    var HDX3DdisableResponse2 = PowerShellScriptCall(Constants.CitrixStudioIP, Constants.CitrixStudioUserName, Constants.CitrixStudioPassword, "StatusChange", "2", "false");

                }
                //  MessageBox.Show("Sucess");
                //  Constants.HDX3DPolicyStatus = true;
            }
            return result = "success";
        }

        public void EnableItem()
        {
            tabTestCases.Enabled = true;
            txtResult.Enabled = true;
            btnBack.Enabled = true;
            btnValidate.Enabled = true;
            btnViewReport.Enabled = true;
            btnExit.Enabled = true;
            btnSampleFilesLocation.Enabled = true;
        }
        public void DisableItem()
        {
            tabTestCases.Enabled = false;
            txtResult.Enabled = false;
            btnBack.Enabled = false;
            btnValidate.Enabled = false;
            btnViewReport.Enabled = false;
            btnExit.Enabled = false;
            btnSampleFilesLocation.Enabled = false;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void tlpHDXPremium_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lblTestCase13_Click(object sender, EventArgs e)
        {

        }

        private void pnlTestCase133_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
