﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using ThinClient.Verification.Common;

namespace ThinClientVerification.UserInterface
{
    public partial class frmTimer : Form
    {
        int timerDecrement;
        int runtime;
        int _testcaseId;
        public frmTimer(int timeStamp, int testcaseId)
        {
            Globals.isITKSnapClosed = false;
            runtime = timeStamp;
            _testcaseId = testcaseId;
            timerDecrement = runtime / 1000;
            InitializeComponent();
        }

        int screenWidth;
        int screenHeight;
        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        private void frmTimer_Load(object sender, EventArgs e)
        {
            this.BackColor = Color.Aqua;
            this.TransparencyKey = Color.Aqua;

            Screen screenResolution = Screen.PrimaryScreen;
            screenWidth = (screenResolution.WorkingArea.Width);
            screenHeight = (screenResolution.WorkingArea.Height);

            this.Location = new Point(screenWidth - 200, 20);
            Globals.isUserStartedFPS = false;
            btnTimeLeft.Text = "Time Left";

            timer.Interval = 1000;
            timer.Tick += new EventHandler(Timer_Tick);
            timer.Start();
        }
        private void Timer_Tick(object sender, EventArgs e)
        {
            try
            {
                bool chkProcess = false;
                btnTimeLeft.Text = "Time Left (" + timerDecrement.ToString() + ")";
                if (_testcaseId == 12)
                {
                    Process[] ps = Process.GetProcessesByName("ITK-SNAP");
                    if (ps.Length == 0)
                    {
                        chkProcess = true;
                        Globals.isITKSnapClosed = true;
                    }
                }
                if (timerDecrement == 0 || chkProcess)
                {
                    timer.Stop();
                    timer.Dispose();
                    timerDecrement = runtime / 1000;
                    this.Close();
                }
                timerDecrement--;
            }
            catch
            {
                timer.Stop();
                timer.Dispose();
                timerDecrement = runtime / 1000;
                this.Close();
            }
        }
    }
}
