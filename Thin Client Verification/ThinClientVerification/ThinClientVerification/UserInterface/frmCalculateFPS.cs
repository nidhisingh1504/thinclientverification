﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ThinClient.Verification.Common;
using ThinClient.Verification.Model;
using ThinClient.Verification.UserInterface;

namespace ThinClientVerification.UserInterface
{
    public partial class frmCalculateFPS : Form
    {
        Form _frm;
        int _testcaseid;
        Process _ps;
        int timerDecrement;
        int runtime = Constants.SAMPLE_FILE_RUNTIME_YOUTUBE;       

        public frmCalculateFPS(frmVerification frmVerify, int testcaseId, Process ps)
        {
            _frm = frmVerify;
            _testcaseid = testcaseId;
            _ps = ps;
            timerDecrement = runtime / 1000;
            InitializeComponent();            
        }


        int screenWidth;
        int screenHeight;
        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        private void frmCalculateFPS_Load(object sender, EventArgs e)
        {
            this.BackColor = Color.Aqua;
            this.TransparencyKey = Color.Aqua;

            Screen screenResolution = Screen.PrimaryScreen;
            screenWidth = (screenResolution.WorkingArea.Width);
            screenHeight = (screenResolution.WorkingArea.Height);

            this.Location = new Point(screenWidth - 200, 20);
            Globals.isUserStartedFPS = false;
            btnCalculateFPS.Text = "Calculate FPS";

            timer.Interval = 1000;
            timer.Tick += new EventHandler(Timer_Tick);
            timer.Start();

        }
        private void Timer_Tick(object sender, EventArgs e)
        {
            try
            {
                btnCalculateFPS.Text = "Calculate FPS (" + timerDecrement.ToString() + ")";
                Process[] isIeExisted = Process.GetProcessesByName(_ps.ProcessName);
                if (timerDecrement == 0 || isIeExisted.Length < 1)
                {
                    timer.Stop();
                    Utilities.ProcessMonitor(_ps, 2000);
                    this.Close();
                }
                timerDecrement--;
            }
            catch
            {
                timer.Stop();
                this.Close();
            }
        }

        private void btnCalculateFPS_Click(object sender, EventArgs e)
        {
            Globals.isUserStartedFPS = true;
            Constants.FPSRUNTIMEFORTC10 = timerDecrement * 1000;
            timer.Stop();
            this.Hide();
        }
    }
}
