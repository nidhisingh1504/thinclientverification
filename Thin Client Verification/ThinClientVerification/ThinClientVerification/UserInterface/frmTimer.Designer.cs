﻿namespace ThinClientVerification.UserInterface
{
    partial class frmTimer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTimeLeft = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnTimeLeft
            // 
            this.btnTimeLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.btnTimeLeft.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTimeLeft.Font = new System.Drawing.Font("Calibri", 12F);
            this.btnTimeLeft.ForeColor = System.Drawing.Color.White;
            this.btnTimeLeft.Location = new System.Drawing.Point(3, 11);
            this.btnTimeLeft.Name = "btnTimeLeft";
            this.btnTimeLeft.Size = new System.Drawing.Size(167, 39);
            this.btnTimeLeft.TabIndex = 0;
            this.btnTimeLeft.Text = "Time Left";
            this.btnTimeLeft.UseVisualStyleBackColor = false;
            // 
            // frmTimer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(175, 63);
            this.ControlBox = false;
            this.Controls.Add(this.btnTimeLeft);
            this.Font = new System.Drawing.Font("Calibri", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmTimer";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmFloatingWindow";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frmTimer_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnTimeLeft;
    }
}