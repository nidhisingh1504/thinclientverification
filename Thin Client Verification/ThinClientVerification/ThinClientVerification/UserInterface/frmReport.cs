﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.IO;
using System.Security.Cryptography;
using iTextSharp.text;
using iTextSharp.text.pdf;
using ThinClient.Verification.Common;
using ThinClient.Verification.Model;
using System.Runtime.InteropServices;

namespace ThinClient.Verification.UserInterface
{
    public partial class frmReport : Form
    {
        public string DetailedLog { get; set; }
        public string msgUser = "";

        public frmReport(string parameter)
        {
            InitializeComponent();
            DetailedLog = parameter;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmReport_Load(object sender, EventArgs e)
        {
            GenerateReport();
        }

        public void GenerateReport()
        {
            txtReport.Clear();

            ShowSystemInformation();
            ShowTestCaseResults();
        }

        private void btnSaveReport_Click(object sender, EventArgs e)
        {
            try
            {
                if (folderBrowserDialogReports.ShowDialog() == DialogResult.OK)
                {
                    Globals.ReportOutputLocation = folderBrowserDialogReports.SelectedPath;
                    if (!Globals.ReportOutputLocation.EndsWith(@"\"))
                    {
                        Globals.ReportOutputLocation = Globals.ReportOutputLocation + @"\";
                    }

                    string strReportFileName = @"" + Globals.ReportOutputLocation + "Report-" + String.Format("{0:dd-MM-yyyy-hhmmss}", DateTime.Now) + ".pdf";
                    string imagesPath = @"" + System.IO.Directory.GetCurrentDirectory() + "\\Screenshots\\";

                    var pdfDoc = new Document();

                    PdfWriter.GetInstance(pdfDoc, new FileStream(strReportFileName, FileMode.Create));
                    pdfDoc.Open();

                    pdfDoc.Add(new Paragraph(txtReport.Text));

                    if (Directory.Exists(imagesPath))
                    {
                        string[] imagefiles = Directory.GetFiles(imagesPath);
                        foreach (string imagefile in imagefiles)
                        {
                            pdfDoc.NewPage();
                            iTextSharp.text.Image pdfImage = iTextSharp.text.Image.GetInstance(imagefile);
                            pdfImage.Alignment = Element.ALIGN_CENTER;
                            pdfImage.ScaleToFit(pdfDoc.PageSize.Width - 10, pdfDoc.PageSize.Height - 10);
                            pdfDoc.Add(pdfImage);
                        }
                    }

                    pdfDoc.Close();

                    msgUser = "Report Saved Successfully at below location:" + "\n" + "\n" + strReportFileName;
                    MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    msgUser = "Saved Report operation cancelled";
                    MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                msgUser = "System Error occured while saving report - " + ex.Message.ToString();
                MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExportReport_Click(object sender, EventArgs e)
        {
            try
            {
                if (folderBrowserDialogReports.ShowDialog() == DialogResult.OK)
                {
                    Globals.ReportOutputLocation = folderBrowserDialogReports.SelectedPath;
                    if (!Globals.ReportOutputLocation.EndsWith(@"\"))
                    {
                        Globals.ReportOutputLocation = Globals.ReportOutputLocation + @"\";
                    }

                    string strReportFileName = @"" + Globals.ReportOutputLocation + "ThinClient-Verification-Report-" + String.Format("{0:dd-MM-yyyy}", DateTime.Now) + ".pdf";

                    bool isFileExists = File.Exists(strReportFileName);
                    bool allowExported = true;
                    if (isFileExists)
                    {
                        msgUser = "The file already exists. Do you want to overwrite it? \n";
                        DialogResult result = MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                        if (result == DialogResult.Yes)
                        {
                            File.Delete(strReportFileName);
                            allowExported = true;
                        }
                        else
                        {
                            allowExported = false;
                        }
                    }
                    if (allowExported)
                    {
                        string imagesPath = @"" + System.IO.Directory.GetCurrentDirectory() + "\\Screenshots\\";

                        FileStream fs = new FileStream(strReportFileName, FileMode.Create, FileAccess.Write, FileShare.None);
                        Document reportDoc = new Document(PageSize.A4);
                        PdfWriter writer = PdfWriter.GetInstance(reportDoc, fs);
                        writer.SetEncryption(PdfWriter.STRENGTH128BITS, Constants.REPORT_EXPORT_PASSWORD, Constants.REPORT_EXPORT_PASSWORD, PdfWriter.AllowPrinting);

                        reportDoc.Open();
                        reportDoc.Add(new Paragraph(txtReport.Text));
                        reportDoc.Add(new Paragraph("DETAILED LOG SECTION:"));
                        reportDoc.Add(new Paragraph(DetailedLog.ToString()));

                        if (Directory.Exists(imagesPath))
                        {
                            string[] imagefiles = Directory.GetFiles(imagesPath);
                            foreach (string imagefile in imagefiles)
                            {
                                reportDoc.NewPage();
                                iTextSharp.text.Image pdfImage = iTextSharp.text.Image.GetInstance(imagefile);
                                pdfImage.Alignment = Element.ALIGN_CENTER;
                                pdfImage.ScaleToFit(reportDoc.PageSize.Width - 10, reportDoc.PageSize.Height - 10);
                                reportDoc.Add(pdfImage);
                            }
                        }

                        reportDoc.Close();

                        /*
                        if (!reportDoc.IsOpen())
                        {
                            string mykey = "012345678901234567890123";
                            GCHandle gch = GCHandle.Alloc(mykey, GCHandleType.Pinned);
                            EncryptData(strReportFileName, mykey);
                            ZeroMemory(gch.AddrOfPinnedObject(), mykey.Length * 2);
                            gch.Free();
                        }
                        */

                        msgUser = "Report Exported Successfully at below location:" + "\n" + "\n" + strReportFileName;
                        MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    msgUser = "Export Report operation cancelled";
                    MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                msgUser = "System Error occured while exporting report - " + ex.Message.ToString();
                MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        #region - Show System Information
        public void ShowSystemInformation()
        {
            txtReport.AppendText("SYSTEM INFORMATION" + " \n");
            txtReport.AppendText("\n");
            foreach (var oSysInfo in SysInformations.oSysInformations)
            {
                if (oSysInfo.ID != 3) //Do not show seperate line for Service Pack
                {
                    txtReport.AppendText(oSysInfo.ConfigLabel + oSysInfo.ConfigValue + " \n");
                }
            }
            txtReport.AppendText("\n");

            txtReport.AppendText("HDX Standard - PREREQUISITE VALIDATION" + " \n");
            foreach (var oSysReq in SysRequirements.oSysRequirements)
            {
                if (oSysReq.Category == Constants.CATEGORY_NAME_HDX_READY)
                {
                    txtReport.AppendText("- " + oSysReq.DisplayName + ": " + oSysReq.Status + " \n");
                }
            }

            txtReport.AppendText("HDX PREMIUM - PREREQUISITE VALIDATION" + " \n");
            foreach (var oSysReq in SysRequirements.oSysRequirements)
            {
                if (oSysReq.Category == Constants.CATEGORY_NAME_HDX_PREMIUM)
                {
                    txtReport.AppendText("- " + oSysReq.DisplayName + ": " + oSysReq.Status + " \n");
                }
            }

            //txtReport.AppendText("HDX 3D PRO - PREREQUISITE VALIDATION" + " \n");
            //foreach (var oSysReq in SysRequirements.oSysRequirements)
            //{
            //    if (oSysReq.Category == Constants.CATEGORY_NAME_HDX_3DPRO)
            //    {
            //        txtReport.AppendText("- " + oSysReq.DisplayName + ": " + oSysReq.Status + " \n");
            //    }
            //}
        }
        #endregion

        #region - Show Test Case Results
        public void ShowTestCaseResults()
        {
            //Common.Utilities.LoadTestCaseDetails();
            txtReport.AppendText("\n");
            txtReport.AppendText("TEST CASE RESULT" + " \n");
            txtReport.AppendText("\n");
            foreach (var oTestCase in TestCases.oTestCases)
            {
                txtReport.AppendText(oTestCase.ID.ToString() + " - " + oTestCase.ScenarioTitle + " (Status: " + oTestCase.Status.ToString() + ")" + " \n");
                txtReport.AppendText(oTestCase.Result.ToString() + " \n");
                txtReport.AppendText(oTestCase.DownloadLink.ToString() + " \n");
                txtReport.AppendText("\n");
            }
        }
        #endregion

    }
}
