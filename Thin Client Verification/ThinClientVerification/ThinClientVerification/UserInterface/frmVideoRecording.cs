﻿using Microsoft.Expression.Encoder.ScreenCapture;
using System;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;

using System.IO;
using Microsoft.Expression.Encoder.Devices;
using Microsoft.Expression.Encoder.Profiles;
using System.Collections.ObjectModel;
using ThinClient.Verification.Common;
using ThinClient.Verification.UserInterface;
using System.Linq;

namespace ThinClientVerification.UserInterface
{
    public partial class frmVideoRecording : Form
    {
        public static int width;
        public static int height;
        public static int btnPauseCount = 0;
        public static string filePath = "";
        public static string fileName = "";
        public static string videoName = "";
        public static ScreenCaptureJob vid;
        public static string pauseFlag = "pause";
        public static string msgUser = "";

        public static System.Timers.Timer stopWatch;
        public static TimeSpan elapsedTime = TimeSpan.Zero;
        public bool isTimerRunning = false;
        public static Stopwatch clock;

        public static frmVerification mainFrmCtrl;

        public static frmQuestionBanner bannerCtrl;

        public frmVideoRecording(frmQuestionBanner frmBanner)
        {
            InitializeComponent();
            this.FormClosing += frmVideoRecording_FormClosing;

            bannerCtrl = frmBanner;


            clock = new Stopwatch();
            stopWatch = new System.Timers.Timer();
            stopWatch.Interval = 1000;
            stopWatch.Elapsed += new System.Timers.ElapsedEventHandler(StopWatch_Elapsed);
        }
        private void StopWatch_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                elapsedTime = clock.Elapsed;
                elapsedTime = new TimeSpan(elapsedTime.Hours, elapsedTime.Minutes, elapsedTime.Seconds);
                this.lblTimer.BeginInvoke((MethodInvoker)delegate () { this.lblTimer.Text = elapsedTime.ToString(); ; });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void frmVideoRecording_Load(object sender, EventArgs e)
        {
            width = 0;
            height = 0;
            Screen[] screens = Screen.AllScreens;
            foreach (Screen scr in screens)
            {
                width += scr.Bounds.Width;
            }
            height = screens.ToList().Max(x => x.Bounds.Height);

            toolTip1.Active = true;
            Constants.isRecorderOpen = true;
            btnStop.Enabled = false;
            btnPlay.Enabled = false;
            btnPause.Enabled = false;
            Screen myScreen = Screen.PrimaryScreen;
            int mywidth = (myScreen.Bounds.Width);
            int myheight = (myScreen.Bounds.Height);
            this.Location = new Point(mywidth - this.Width - 20, bannerCtrl.Height + 60);
            this.BringToFront();
            lblWatermark.Text = DateTime.Now.ToString("dd-MMM-yyyy");
            this.Focus();
            toolTip1.SetToolTip(btnRecord, "Start Recording");
            this.Refresh();

            Globals.isRTOPCompleted1 = false;
            Globals.isRTOPCompleted2 = false;
            Globals.isAudacityTestCaseCompleted = false;
            Globals.IsVideoRecorded = false;
        }


        public void RecordVideo()
        {
            try
            {
                filePath = Constants.FolderToSaveVideo;
                vid = new ScreenCaptureJob();

                if (Globals.RTOP == "RTOP")
                {
                    videoName = "HDX RTOP" + DateTime.Now.ToString("dd-MMM-yyyy_HH-mm-ss") + ".mkv";

                    if (!filePath.EndsWith(@"\"))
                    {
                        fileName = filePath + @"\" + "HDX RTOP" + "-" + DateTime.Now.ToString("dd-MMM-yyyy_HH-mm-ss") + ".mkv";
                    }
                    else
                    {
                        fileName = filePath + "HDX RTOP" + "-" + DateTime.Now.ToString("dd-MMM-yyyy_HH-mm-ss") + ".mkv";
                    }

                }

                if (Globals.VOIP == "VOIP")
                {
                    videoName = "HDX VoIP" + DateTime.Now.ToString("dd-MMM-yyyy_HH-mm-ss") + ".mkv";
                    if (!filePath.EndsWith(@"\")) { fileName = filePath + @"\" + "HDX VoIP" + "-" + DateTime.Now.ToString("dd-MMM-yyyy_HH-mm-ss") + ".mkv"; }
                    else { fileName = filePath + "HDX VoIP" + "-" + DateTime.Now.ToString("dd-MMM-yyyy_HH-mm-ss") + ".mkv"; }

                }
                //fileName = filePath + videoName;
                vid.OutputScreenCaptureFileName = fileName;
                Constants.VideoFileName = fileName;
                

                Collection<EncoderDevice> devices = EncoderDevices.FindDevices(EncoderDeviceType.Audio);

                //Iterates through all audio devices and selects the internal audio
                //foreach (EncoderDevice device in devices)
                //{
                //    if (!device.Name.ToLower().Contains("speaker") & !device.Name.ToLower().Contains("earphone"))
                //    { vid.AddAudioDeviceSource(device); }
                //}

                vid.CaptureRectangle = new Rectangle(0, 0, width - width % 8, height - height % 8);                
                vid.ScreenCaptureVideoProfile = new ScreenCaptureVideoProfile();
                vid.ShowFlashingBoundary = false;
                vid.ScreenCaptureVideoProfile.Quality = 40;
                vid.ScreenCaptureVideoProfile.FrameRate = 25;
                vid.CaptureMouseCursor = true;
                vid.ScreenCaptureVideoProfile.Force16Pixels = true;

                // Starts capture
                vid.Start();
                Globals.IsVideoRecorded = true;
                if (isTimerRunning == false)
                {
                    stopWatch.Start();
                    clock.Start();
                    isTimerRunning = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("A runtime exception occured: " + ex.Message + "Please try again");
            }
        }


        private void frmVideoRecording_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult msgResult;
            if (e.CloseReason == CloseReason.UserClosing)
            {

                bannerCtrl.WindowState = FormWindowState.Normal;

                if (Globals.recordingMessage)
                {
                    if (Globals.IsVideoRecorded == true)
                    {
                        vid.Stop();
                        msgUser = "Your media file ‘" + videoName + "’ has been successfully processed and saved at the below location: " + filePath;
                        MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Constants.isRecorderOpen = false;
                        Globals.recordingMessage = false;
                   
                    }
                    else
                    {
                        msgUser = "You have not recorded the execution of this test case. Please initiate/start the recording.";
                        MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    msgUser = "Are you sure you want to stop the execution of the test case?";
                    msgResult = MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (msgResult == DialogResult.Yes)
                    {
                        if (Globals.IsVideoRecorded == true)
                        {
                            vid.Stop();
                            msgUser = "Your media file ‘" + videoName + "’ has been successfully processed and saved at the below location: " + filePath;
                            MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);
                          
                        }
                        else
                        {
                            msgUser = "You have not recorded the execution of this test case. Please initiate/start the recording.";
                            MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        Constants.isRecorderOpen = false;
                        Globals.recordingMessage = true;
                        Form f = Application.OpenForms["frmQuestionBanner"];
                        if (f != null)
                            f.Close();
                        Globals.counter = 0;
                    }
                    else
                    {
                        e.Cancel = true;
                        this.Focus();

                    }
                }

                Constants.isRecorderOpen = false;
            }

        }


        private void btnRecord_Click(object sender, EventArgs e)
        {
            btnRecord.Enabled = false;
            btnPause.Enabled = true;
            toolTip1.SetToolTip(btnPause, "Pause");
            btnStop.Enabled = true;
            toolTip1.SetToolTip(btnStop, "Stop Recording");
            btnPlay.Enabled = false;
            RecordVideo();
            bannerCtrl.BringToFront();
            bannerCtrl.TopMost = true;
            bannerCtrl.Focus();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            string msgUser = "Are you sure you want to stop record?";
            DialogResult result = MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (result == DialogResult.OK)
            {
                btnStop.Enabled = false;
                btnPause.Enabled = false;
                btnRecord.Enabled = true;
                btnPlay.Enabled = true;
                vid.Stop();

                stopWatch.Stop();
                clock.Stop();
                clock.Reset();
                isTimerRunning = false;
                elapsedTime = TimeSpan.Zero;

                bannerCtrl.WindowState = FormWindowState.Normal;
                //bannerCtrl.Close();
                //this.Close();

                msgUser = "Your media file ‘" + videoName + "’ has been successfully processed and saved at the below location: " + filePath;
                MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }




           

        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            btnRecord.Enabled = false;
            btnStop.Enabled = true;
            btnPlay.Enabled = false;

            if (pauseFlag == "pause")
            {
                vid.Pause();
                if (isTimerRunning == true)
                {
                    stopWatch.Stop();
                    clock.Stop();
                    isTimerRunning = false;
                }
                pauseFlag = "resume";
                btnPause.Image = Properties.Resources.niEX5xpKT;
                toolTip1.SetToolTip(btnPause, "Resume Recording");
            }
            else
            {
                vid.Resume();
                if (isTimerRunning == false)
                {
                    stopWatch.Start();
                    clock.Start();
                    isTimerRunning = true;
                }
                pauseFlag = "pause";
                btnPause.Image = Properties.Resources.player_pause;
                toolTip1.SetToolTip(btnPause, "Pause Recording");
            }
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            btnStop.Enabled = false;
            btnPause.Enabled = false;
            btnRecord.Enabled = true;

            msgUser = "Media file " + fileName + "is not available in the destination folder.";
            if (File.Exists(fileName))
            {
                ProcessStartInfo psStartInfo = new ProcessStartInfo();
                psStartInfo.FileName = @"" + "\"" + Constants.APP_EXECUTABLE_WINDOWSMEDIAPLAYER + "\"";
                psStartInfo.Arguments = @" " + "\"" + fileName + "\" /fullscreen";
                psStartInfo.WindowStyle = ProcessWindowStyle.Maximized;
                Process ps = Process.Start(psStartInfo);
            }
            else { MessageBox.Show(msgUser, Constants.MSGBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information); }
        }
    }


}
