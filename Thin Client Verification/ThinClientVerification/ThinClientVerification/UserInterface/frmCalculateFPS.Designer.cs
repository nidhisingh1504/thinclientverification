﻿namespace ThinClientVerification.UserInterface
{
    partial class frmCalculateFPS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCalculateFPS = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCalculateFPS
            // 
            this.btnCalculateFPS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(121)))), ((int)(((byte)(184)))));
            this.btnCalculateFPS.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCalculateFPS.Font = new System.Drawing.Font("Calibri", 12F);
            this.btnCalculateFPS.ForeColor = System.Drawing.Color.White;
            this.btnCalculateFPS.Location = new System.Drawing.Point(3, 11);
            this.btnCalculateFPS.Name = "btnCalculateFPS";
            this.btnCalculateFPS.Size = new System.Drawing.Size(167, 39);
            this.btnCalculateFPS.TabIndex = 0;
            this.btnCalculateFPS.Text = "Calculate FPS";
            this.btnCalculateFPS.UseVisualStyleBackColor = false;
            this.btnCalculateFPS.Click += new System.EventHandler(this.btnCalculateFPS_Click);
            // 
            // frmCalculateFPS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(175, 63);
            this.ControlBox = false;
            this.Controls.Add(this.btnCalculateFPS);
            this.Font = new System.Drawing.Font("Calibri", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmCalculateFPS";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmFloatingWindow";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frmCalculateFPS_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCalculateFPS;
    }
}